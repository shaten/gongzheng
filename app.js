const util = require('utils/util.js')
//app.js
App({
  onLaunch: function () {
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        console.log('onCheckForUpdate====', res)
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          console.log('res.hasUpdate====')
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？历史转让人记录已更新,原历史记录清空,不影响正常办理流程',
              success: function (res) {
                console.log('success====', res)
                // util.removeUserData('identity_token')
                // res: {errMsg: "showModal: ok", cancel: false, confirm: true}
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
            })
          })
        }
      })
    }



    var that = this
    // wx.clearStorageSync()

    // 展示本地存储能力
    //var logs = wx.getStorageSync('logs') || []
    //logs.unshift(Date.now())
    //wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        util.request('V2/getSign',{code:res.code},
          function(res){
            util.removeUserData('help_transfer_info_apply_nos')//清空本地applyno
            var session_key = util.getUserdata('session_key')
            session_key = res.data.session_key
            util.setUserdata('session_key',session_key)
            var identity_token = util.getUserdata('identity_token')
            console.log('切换帐号新token,getSin的旧token:' + identity_token)
            console.log('切换帐号新token,getSin的旧token:' + identity_token.length)
            console.log(typeof(identity_token))
            console.log('getSin的最新token:' + res.data.identity_token)
            // identity_token = res.data.identity_token
            if (typeof(identity_token) == 'object') {
              console.log('进来')
              identity_token = res.data.identity_token
            }
            util.setUserdata('identity_token', identity_token)

            wx.getSystemInfo({
              success(res) {
                var rtn = res
                console.log(rtn)
                wx.request({
                  url: util.reqUrl('V2/getSystemInfo'),
                  header: util.headers(),
                  method: 'post',
                  data: { 'identity_token': util.getUserdata('identity_token'),'res':JSON.stringify(rtn)},
                  success(res) {
                    console.log('用户信息')
                    console.log(res)
                    if(res.data.code == '-10001') {
                    }
                  }
                })
              }
            })

            if (that.userInfoReadyCallback) {
              that.userInfoReadyCallback(res)
            }
          }
        )
      }
    })

    util.request(
      'V2/myAllInfo',
      { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          that.globalData.userInfo = res.data
          that.globalData.member = res.data.member
        }
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        // wx.hideToast();
      })

    

    // 获取用户信息
    // wx.getSetting({
    //   success: res => {
    //     console.log(res)
    //     if (res.authSetting['scope.userInfo']) {
    //       // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
    //       wx.getUserInfo({
    //         success: res => {
    //           console.log('dsb')
    //           console.log(res)
    //           // // 可以将 res 发送给后台解码出 unionId
    //           that.globalData.userInfo = res.userInfo
    //         }
    //       })
    //     }
    //   }
    // })
    
  },
  onShow: function (options) {

    console.log("wx.onShow监听页面显示");
    console.log(options);

    if (options) {
      console.log("认证凭证,32位凭证，提交申请时提供:");
      if (options.referrerInfo.extraData)
      {
        console.log(options.referrerInfo.extraData.verifyCode);  //认证凭证,32位凭证，提交申请时提供
        console.log("结果码为:");
        console.log(options.referrerInfo.extraData.verifyResult);//结果码，除“0”以外，其余都是错误代码
        if (options.referrerInfo.extraData.verifyResult == 0) {
          console.log("认证成功");
          // if (options.path != 'pages/share_certification/share_certification') {
            //本人认证
            var myindex = util.getUserArr('help_transfer_info_GetToken_index');
          
            var transferSign = util.getUserArr('transferSign')
            console.log(transferSign)
            console.log(myindex)
            transferSign[myindex - 1].verifycode = options.referrerInfo.extraData.verifyCode;
            console.log("认证凭证,32位凭证，提交申请时提供:" + transferSign[myindex - 1].verifycode);
            console.log(transferSign)
            util.setUserdata('transferSign', transferSign);
            util.setUserdata('transferIndex', myindex);
            util.setUserdata('help_transfer_info_GetToken_index', -1);

            // var myindex = util.getUserArr('transfer_info_GetToken_index');
            // var transfer_info = util.getUserArr('transfer_info');
            // console.log(transfer_info)
            // //    transfer_info[myindex-1].id = myindex;
            // transfer_info[myindex - 1].verifycode = options.referrerInfo.extraData.verifyCode;
            // console.log("认证凭证,32位凭证，提交申请时提供:" + transfer_info[myindex - 1].verifycode);

            // util.setUserdata('transfer_info', transfer_info);
            // util.setUserdata('transfer_info_GetToken_index', -1);
            
          // }

          // else{
          //   //代人认证
          //   var help_myindex = util.getUserArr('help_transfer_info_GetToken_index');
          //   var help_transfer_info = util.getUserArr('share_transfer_info');
          //   console.log(help_transfer_info)
          //   console.log(options.referrerInfo.extraData.verifyCode)
          //   console.log(help_transfer_info[help_myindex - 1].verifycode)
          //   //    transfer_info[myindex-1].id = myindex;
          //   help_transfer_info[help_myindex - 1].verifycode = options.referrerInfo.extraData.verifyCode;
          //   console.log("代人认证凭证,32位凭证，提交申请时提供:" + help_transfer_info[help_myindex - 1].verifycode);

          //   util.setUserdata('share_transfer_info', help_transfer_info);
          //   util.setUserdata('help_transfer_info_GetToken_index', -1);
          // }
          
        } else {
          // wx.showToast({
          //   title: options.referrerInfo.extraData.verifyResult,
          //   icon: 'none',
          //   duration: 1000
          // });
        }
      }
    
    } else {
      console.log("wx.onShow监听页面没有数据传输显示");
    }
 
  },
  globalData: {
    userInfo: [],
    member: []
  }

})