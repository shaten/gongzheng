// pages/transfer_info_personal/transfer_info_personal.js
const util = require('../../utils/util.js')
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    genderType: ["点击选择性别","男", "女"],
    genderTypeIndex: 0,
    personal_list : [],
    isButtonClick: false,
    // isidentityClick: false,
    cWidth: 0,
    cHeight: 0
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  go_select: function(e){
    wx.navigateTo({
      url: '../help_transfer_info_select/help_transfer_info_select?type=1&identity_type=1',
    })
  },
  genderChange: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].personal_gender = e.detail.value
    console.log(transfer_info);
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info)
  },
  choose_identity_card_front_files: function (e) {
      var that = this
      var test_img = ''
      var index = e.currentTarget.dataset.index
      if(this.data.personal_list[index].identity_card_front_path) return;
      wx.chooseImage({
          count: 1,
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (photo) {
            console.log(photo)
            console.log(photo.tempFilePaths[0])
            console.log(photo.tempFiles[0].size/1024/1024)
            var image_size = 5 * 1024 * 1024
            if (photo.tempFiles[0].size > image_size) {
              wx.showToast({
                title: '上传图片不能超过5M',
                icon: 'none',
                duration: 2000
              })
              return false
            }
            wx.getImageInfo({
              src: photo.tempFilePaths[0], 
              success: function (res) {
                console.log(res);
                //---------利用canvas压缩图片--------------
                var ratio = 2;
                var canvasWidth = res.width //图片原始长宽
                var canvasHeight = res.height
                while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
                  canvasWidth = Math.trunc(res.width / ratio)
                  canvasHeight = Math.trunc(res.height / ratio)
                  ratio++;
                }
                that.setData({
                  cWidth: canvasWidth,
                  cHeight: canvasHeight
                })

                //----------绘制图形并取出图片路径--------------
                var ctx = wx.createCanvasContext('identity_card_front_files')
                ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
                ctx.draw(false, setTimeout(function () {
                  wx.canvasToTempFilePath({
                    canvasId: 'identity_card_front_files',
                    destWidth: canvasWidth,
                    destHeight: canvasHeight,
                    success: function (result) {
                      console.log(result.tempFilePath)//最终图片路径
                      
                      //上传图片
                      wx.showLoading({
                        title: '图片上传中',
                        icon: 'loading',
                        mask: true
                      })
                      wx.uploadFile({
                        url: util.reqUrl('V2/uploadIdentityFront'),
                        header: util.headers(),
                        formData: { 'identity_token': util.getUserdata('identity_token') },
                        filePath: result.tempFilePath,
                        name: 'identity_card_front_' + index,
                        success(res) {
                          wx.hideLoading()
                          console.log(res)
                          var rtn = JSON.parse(res.data)
                          console.log(rtn)
                          if (rtn.code < 0) {
                            wx.showToast({
                              title: rtn.err,
                              icon: 'none',
                              duration: 2000
                            })
                            return false
                          }
                          var transfer_info = util.getUserArr('help_transfer_info')
                          transfer_info[index].identity_card_front_vc = rtn.verify_code

                          transfer_info[index].identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                          if (rtn.personal_name) {
                            transfer_info[index].personal_name = rtn.personal_name
                          }

                          // if (rtn.identity_address) {
                          //   transfer_info[index].identity_address = rtn.identity_address
                          // }

                          if (rtn.identity_code) {
                            transfer_info[index].identity_code = rtn.identity_code
                          }

                          if (rtn.personal_gender) {
                            transfer_info[index].personal_gender = rtn.personal_gender
                          }

                          if (rtn.personal_birth_date) {
                            transfer_info[index].personal_birth_date = rtn.personal_birth_date
                          }

                          console.log(transfer_info)

                          that.setData({
                            personal_list: transfer_info
                          })

                          util.setUserdata('help_transfer_info', transfer_info)
                        }
                      })
                      
                    },
                    fail: function (res) {
                      console.log(res.errMsg)
                    }
                  })
                },500))
              }
            })
          }
      })
  },
  choose_identity_card_back_files: function (e) {
      var that = this
      var index = e.currentTarget.dataset.index
      if(this.data.personal_list[index].identity_card_back_path) return;
      wx.chooseImage({
          count: 1,
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (photo) {
            var image_size = 5 * 1024 * 1024
            if (photo.tempFiles[0].size > image_size) {
              wx.showToast({
                title: '上传图片不能超过5M',
                icon: 'none',
                duration: 2000
              })
              return false
            }
            console.log(photo.tempFilePaths[0])
            wx.getImageInfo({
              src: photo.tempFilePaths[0],
              success: function (res) {
                console.log(res);
                //---------利用canvas压缩图片--------------
                var ratio = 2;
                var canvasWidth = res.width //图片原始长宽
                var canvasHeight = res.height
                while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
                  canvasWidth = Math.trunc(res.width / ratio)
                  canvasHeight = Math.trunc(res.height / ratio)
                  ratio++;
                }
                that.setData({
                  cWidth: canvasWidth,
                  cHeight: canvasHeight
                })

                //----------绘制图形并取出图片路径--------------
                var ctx = wx.createCanvasContext('identity_card_back_files')
                ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
                ctx.draw(false, setTimeout(function () {
                  wx.canvasToTempFilePath({
                    canvasId: 'identity_card_back_files',
                    destWidth: canvasWidth,
                    destHeight: canvasHeight,
                    success: function (result) {
                      console.log(result.tempFilePath)//最终图片路径
                      //上传图片
                      wx.showLoading({
                        title: '图片上传中',
                        icon: 'loading',
                        mask: true
                      })
                      wx.uploadFile({
                        url: util.reqUrl('V2/uploadIdentityBack'),
                        header: util.headers(),
                        formData: { 'identity_token': util.getUserdata('identity_token') },
                        filePath: result.tempFilePath,
                        name: 'identity_card_back_' + index,
                        success(res) {
                          wx.hideLoading()
                          console.log(res)
                          var rtn = JSON.parse(res.data)
                          if (rtn.code < 0) {
                            wx.showToast({
                              title: rtn.err,
                              icon: 'none',
                              duration: 2000
                            })
                            return false
                          }
                          var transfer_info = util.getUserArr('help_transfer_info')
                          transfer_info[index].identity_card_back_vc = rtn.verify_code
                          transfer_info[index].identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                          that.setData({
                            personal_list: transfer_info
                          })

                          util.setUserdata('help_transfer_info', transfer_info)
                        }
                      })

                    },
                    fail: function (res) {
                      console.log(res.errMsg)
                    }
                  })
                }, 500))
              }
            })
          }
      })
  },
  choose_signature_files(e){
    var that = this
    var index = e.currentTarget.dataset.index
    if(this.data.personal_list[index].signature_path) return;
    wx.navigateTo({
      url: '../help_signature/help_signature?page=help_transfer_info_personal&type=1&index='+index
    })
  },
  save: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''

    
    
    // for(var i = 0; i < pageData.length; i++) {
      if(pageData[index].transfer_type == 1) {

        if (!pageData[index].identity_card_front_path) {
          wx.showToast({
            title: '请上传身份证正面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (!pageData[index].identity_card_back_path) {
          wx.showToast({
            title: '请上传身份证背面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].personal_name == '') {
          wx.showToast({
            title: '请输入姓名',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        transfer_remarkp += '转让人:' + pageData[index].personal_name+';'

        if (pageData[index].signature_vc == '') {
          wx.showToast({
            title: '请输入身份证',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].identity_code == '') {
          wx.showToast({
            title: '请输入身份证',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        transfer_remarkp += '身份证:' + pageData[index].identity_code + ';'

        if (pageData[index].personal_gender == 0) {
          wx.showToast({
            title: '请选择性别',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].personal_birth_date == '') {
          wx.showToast({
            title: '请选择出生日期',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        transfer_remarkp += '出生日期:' + pageData[index].personal_birth_date + ';'
        // transfer_remarkp += '地址:' + pageData[index].identity_address + ';'
        
        pageData[index].isShow = true
        pageData[index].transfer_remarkp = transfer_remarkp
      }
    // }
    

    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    console.log(index);
    console.log(pageData[index].personal_birth_date)

    transfer_info[index].identity_address = pageData[index].identity_address
    transfer_info[index].identity_card_back_path = pageData[index].identity_card_back_path
    transfer_info[index].identity_card_back_vc = pageData[index].identity_card_back_vc
    transfer_info[index].identity_card_front_path = pageData[index].identity_card_front_path
    transfer_info[index].identity_card_front_vc = pageData[index].identity_card_front_vc
    transfer_info[index].identity_code = pageData[index].identity_code
    transfer_info[index].personal_birth_date = pageData[index].personal_birth_date
    transfer_info[index].personal_gender = pageData[index].personal_gender
    transfer_info[index].personal_name = pageData[index].personal_name
    transfer_info[index].transfer_type = 1
    // transfer_info[index].signature_vc = pageData[index].signature_vc
    transfer_info[index].isShow = pageData[index].isShow

    
    that.setData({
      personal_list: pageData
    })

    console.log(transfer_info);
    wx.showToast({
      title: '保存成功',
      icon: 'none',
      duration: 2000
    })
    wx.reLaunch({
      url: '../help_get_notarization/help_get_notarization',
    })
    return false

    wx.showModal({
      title: '提示',
      content: '是否确认保存',
      cancelText: "返回",
      confirmText: "继续添加",
      success: function(sm) {
        util.setUserdata('help_transfer_info', transfer_info)
        if (sm.cancel) {
          wx.navigateTo({
            url: '../help_transfer_info/help_transfer_info',
          })
        }
      }
    })
    
  },
  add: function () {
    var transfer_info = util.getUserArr('help_transfer_info')
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 1) {
        var transfer_remarkp = ''
        var num = 0
        if (transfer_info[i].identity_card_front_path) {
          console.log('kk' + 1)
          num += 1
        }else{
          wx.showToast({
            title: '请上传身份证正面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].identity_card_back_path) {
          console.log('kk' + 2)
          num += 1
        }else{
          wx.showToast({
            title: '请上传身份证背面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].personal_name) {
          console.log('kk' + 3)
          transfer_remarkp += '转让人:' + transfer_info[i].personal_name + ';'
          num += 1
        }else{
          wx.showToast({
            title: '请填写转让人姓名',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].personal_gender) {
          console.log('kk' + 4)
          num += 1
        }else{
          wx.showToast({
            title: '请选择转让让人性别',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].personal_birth_date) {
          console.log('kk' + 5)
          num += 1
        }else{
          wx.showToast({
            title: '请选择出生日期',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].identity_code) {
          console.log('kk' + 7)
          transfer_remarkp += '身份证号:' + transfer_info[i].identity_code + ';'
          num += 1
        }else{
          wx.showToast({
            title: '请填写身份证号码',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        // if (transfer_info[i].identity_address) {
        //   console.log('kk' + 6)
        //   transfer_remarkp += '地址:' + transfer_info[i].identity_address + ';'
        //   num += 1
        // }else{
        //   wx.showToast({
        //     title: '请填写转让人地址',
        //     icon: 'none',
        //     duration: 2000
        //   })
        //   return false
        // }


        console.log(num)
        //后面改7
        if (num >= 7) {
          console.log('ok')
          transfer_info[i].isShow = true
          transfer_info[i].transfer_remarkp = transfer_remarkp
        }
      }
    }

    transfer_info.push({ 'transfer_type': 1, 'ot': false, isShow: false, transfer_remarkp: ''})
    this.setData({
      personal_list: transfer_info
    })
    console.log(transfer_info);
   
    var index = transfer_info.length - 1;
    console.log(index);
    util.setUserdata('help_transfer_info',transfer_info);

    var that = this
   // var index = e.currentTarget.dataset.index
    if (this.data.personal_list[index].identity_card_front_path) return;

  },
  change_isShow: function(e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''
    var transfer_info = util.getUserArr('help_transfer_info')

    console.log(transfer_info[index].isShow);
    transfer_info[index].isShow = !transfer_info[index].isShow
    pageData[index].personal_name ? transfer_remarkp += '转让人:' + pageData[index].personal_name + ';' : ''
    pageData[index].identity_code ? transfer_remarkp += '身份证:' + pageData[index].identity_code + ';' : ''
    pageData[index].identity_address ? transfer_remarkp += '地址:' + pageData[index].identity_address + ';' : ''
    
    transfer_info[index].transfer_remarkp = transfer_remarkp
    that.setData({
      personal_list: transfer_info
    })
    util.setUserdata('help_transfer_info', transfer_info)
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function(sm) {
        if(sm.confirm){
          transfer_info.splice(index, 1)
          that.setData({
            personal_list: transfer_info
          })
          // util.setUserdata('transfer_info_apply_nos', "")
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })
    
    
  },
  clear_input_index:function(e){

    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')

    transfer_info[index] = Array();
    transfer_info[index].transfer_type=1;
    transfer_info[index].ot = false; 
    console.log(transfer_info);
    return false;
    this.setData({
      personal_list: transfer_info
    })
    util.setUserdata('help_transfer_info', transfer_info)

  },
  preview_identity_card_front_files: function(e){
    var index = e.currentTarget.dataset.index
    if(this.data.personal_list[index].identity_card_front_path){
      wx.previewImage({
        urls: this.data.personal_list[index].identity_card_front_path
      })
    }
  },
  preview_identity_card_back_files: function(e){
    var index = e.currentTarget.dataset.index;
    if(this.data.personal_list[index].identity_card_back_path){
      wx.previewImage({
        urls: this.data.personal_list[index].identity_card_back_path
      })
    }
  },
  preview_signature_files: function(e){1
    var index = e.currentTarget.dataset.index;
    if(this.data.personal_list[index].signature_path){
      wx.previewImage({
        urls: [this.data.personal_list[index].signature_path]
      })
    }
  },
  touchStart: function(e){
    this.touchTimeStamp = e.timeStamp
  },
  touchEnd: function (e) {
    var diff = e.timeStamp - this.touchTimeStamp
    var tag = e.currentTarget.dataset.tag
    if (tag == 'identity_card_front_block'){
      diff >= 500 ? this.delete_identity_card_front_files(e) : this.preview_identity_card_front_files(e)
    } 
    if (tag == 'identity_card_back_block') {
      diff >= 500 ? this.delete_identity_card_back_files(e) : this.preview_identity_card_back_files(e)
    } 
    if (tag == 'signature_block') {
      diff >= 500 ? this.delete_signature_files(e) : this.preview_signature_files(e)
    } 
  },
  delete_signature_files: function (e) {
    var index = e.currentTarget.dataset.index
    var that = this
    if (this.data.personal_list[index].signature_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].signature_path = '';
            transfer_info[index].signature_vc = '';
            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  delete_identity_card_front_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    if(this.data.personal_list[index].identity_card_front_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].identity_card_front_path = '';
            transfer_info[index].identity_card_front_vc = '';
            util.setUserdata('help_transfer_info',transfer_info)
            that.reload()
          } 
        }
      })
    }
  },
  delete_identity_card_back_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    if(this.data.personal_list[index].identity_card_back_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].identity_card_back_path = '';
            transfer_info[index].identity_card_back_vc = '';
            util.setUserdata('help_transfer_info',transfer_info)
            that.reload()
          } 
        }
      })
    }
  },
  personal_name_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].personal_name = e.detail.value
    console.log(transfer_info);
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  identity_code_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].identity_code = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  personal_birth_date_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].personal_birth_date = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  identity_address_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].identity_address = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  personal_address_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].personal_address = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  personal_phone_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].personal_phone = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  clear_input: function(e){
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    var pageList = that.data.personal_list

    console.log(transfer_info);
    console.log(that.data.personal_list);
    
    wx.showModal({
      title: '提示',
      content: '确定要清空吗？',
      success: function(sm) {
        if(sm.confirm) {
          for (var i = 0; i < pageList.length; i++) {
            if (i == index && pageList[i].transfer_type == 1) {
              pageList[i].identity_card_back_path = '';
              pageList[i].identity_card_back_vc = '';
              pageList[i].identity_card_front_path = '';
              pageList[i].identity_card_front_vc = '';
              pageList[i].identity_address = '';
              pageList[i].identity_code = '';
              pageList[i].personal_birth_date = '';
              pageList[i].personal_address = '';
              pageList[i].personal_gender = 0;
              pageList[i].personal_name = '';
              pageList[i].personal_phone = '';
            }
          }
          that.setData({
            personal_list: pageList
          });

          for (var i = 0; i < transfer_info.length; i++) {
            if (i == index && transfer_info[i].transfer_type == 1) {
              transfer_info[i].identity_card_back_path = '';
              transfer_info[i].identity_card_back_vc = '';
              transfer_info[i].identity_card_front_path = '';
              transfer_info[i].identity_card_front_vc = '';
              transfer_info[i].identity_address = '';
              transfer_info[i].identity_code = '';
              transfer_info[i].personal_birth_date = '';
              transfer_info[i].personal_address = '';
              transfer_info[i].personal_gender = 0;
              transfer_info[i].personal_name = '';
              transfer_info[i].personal_phone = '';
            }
          }
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })
  },
  identify_check(e){
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('transfer_info')
    var name = transfer_info[index].personal_name
    var identity_code = transfer_info[index].identity_code

    if (e.currentTarget.dataset.text == '已通过') {
      return false
    }

    var msg = "";
    if (!name || name == ""){
      wx.showToast({
        title: '姓名不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }
     
    if (!identity_code || identity_code == "")
    {
      wx.showToast({
        title: '身份证号码不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    } 
    console.log("第" + index+"个转让人");
    util.setUserdata('transfer_info_GetToken_index', index+1);
    //return wx.showToast("身份证号码不能为空!");
 //   util.request('V3/identifyCheck',
    wx.showLoading({
      title: '加载中',
    });

    var apply_no = "";
    if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
      apply_no = util.getUserArr('transfer_info_apply_nos');
    }
    console.log('transfer_info_apply_nos==')
    console.log(util.getUserArr('transfer_info_apply_nos'))

    

    util.request('V3/GetToken',
      {
        'name': name, 
        'identity_code': identity_code,
        'apply_no': apply_no
      },
      function(res){
        
        wx.hideLoading();
        console.log("第" + util.getUserArr('transfer_info_GetToken_index') +"个获取GetToken结果");
        console.log(res)
        if(res.data.code == 0){
          console.log(res.data.access_token)
          console.log(res.data.apply_no)

      
          var transfer_info = util.getUserArr('transfer_info')
          if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
            console.log("transfer_info_apply_nos已存在值")
            console.log(util.getUserArr('transfer_info_apply_nos'))
            // transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = util.getUserArr('transfer_info_apply_nos');//apply_no赋值
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = res.data.apply_no;
          } else {
            console.log("给transfer_info_apply_nos赋值")
            util.setUserdata('transfer_info_apply_nos', res.data.apply_no);
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = res.data.apply_no; //apply_no赋值
          }
          
          util.setUserdata('transfer_info', transfer_info)
          /*  that.setData({
            personal_list: transfer_info
          });
          util.setUserdata('transfer_info', transfer_info)
        */
         /*
         //旧的认证
          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: 'pages/idverification/idverification?certoken=' + res.data.token,//pages/idverification/idverification?certoken=
            envVersion: 'trial',
            success(res) {
              // 打开成功
              console.log("open other app ok!")
            }
          })
          */
          
          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: '/pages/idverification/idverification',
            envVersion: 'release',
            extraData: {
              'token': res.data.access_token, //身份凭证accessToken
              'applyNo': res.data.apply_no,//订单号applyNo
              'name': name, //当事人姓名
              'idno': identity_code //当事人身份证号码
            },
            success(res) {
              console.log("调用外部小程序回调数据");
              console.log(res);
            },
            fail(res){
              // that.setData({
              //   isButtonClick: false
              // })
            },
            complete(res) {
              // that.setData({
              //   isButtonClick: false
              // })
            }
          });

        }else
        {
          // that.setData({
          //   isButtonClick: true
          // })
          // wx.showToast({
          //   title: res.data.msg,
          //   icon: 'none',
          //   duration: 1000
          // });
        }
    });

/*
    wx.onShow(function (options){
      console.log("wx.onShow监听页面显示");
      console.log(options);

      if (options) {
        options.referrerInfo.extraData.verifyCode; //结果码，除“0”以外，其余都是错误代码
        options.referrerInfo.extraData.verifyResult;//认证凭证,32位凭证，提交申请时提供
        if (options.referrerInfo.extraData.verifyCode == 0) {
          var myindex = util.getUserArr('transfer_info_GetToken_index');
          var transfer_info = util.getUserArr('transfer_info')
          transfer_info[myindex].id = myindex
          this.setData({
            personal_list: transfer_info
          });
          util.setUserdata('transfer_info', transfer_info);
          util.setUserdata('transfer_info_GetToken_index', -1);
        } else {
          wx.showToast({
            title: options.referrerInfo.extraData.verifyResult,
            icon: 'none',
            duration: 1000
          });
        }
      } else {
        console.log("wx.onShow监听页面没有数据传输显示");
      }
    });
   */ 

  },
   
  notice(msg){
    if(msg != ''){
      wx.showModal({
          content: msg,
          showCancel: false,
          success: function (res) {
              if (res.confirm) {
              }
          }
      })
      return
    }
  },
  reload: function(){
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 1) {
        var transfer_remarkp = ''
        var num = 0
        if (transfer_info[i].identity_card_front_path) {
          num += 1
        }
        if (transfer_info[i].identity_card_back_path) {
          num += 1
        }
        if (transfer_info[i].personal_name) {
          transfer_remarkp += '转让人:' + transfer_info[i].personal_name + ';'
          num += 1
        }
        if (transfer_info[i].personal_gender) {
          num += 1
        }
        if (transfer_info[i].personal_birth_date) {
          num += 1
        }
        if (transfer_info[i].identity_code) {
          transfer_remarkp += '身份证号:' + transfer_info[i].identity_code + ';'
          num += 1
        }
        if (transfer_info[i].identity_address) {
          transfer_remarkp += '地址:' + transfer_info[i].identity_address + ';'
          num += 1
        }

        console.log(num)
        //后面改7
        if (num >= 7) {
          console.log('ok')
          transfer_info[i].isShow = true
          
          transfer_info[i].transfer_remarkp = transfer_remarkp
        }
      }
      
    }
    this.setData({
      personal_list : transfer_info
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("监听页面加载");
    console.log(options);
    this.reload()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   *  app.onShow:function(options) {
    options.referrerInfo.extraData.verifyCode; //结果码，除“0”以外，其余都是错误代码
    options.referrerInfo.extraData.verifyResult;//认证凭证,32位凭证，提交申请时提供
  },
   */
  onShow: function () {

  
    
    this.reload()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  touchTimeStamp: 0
})