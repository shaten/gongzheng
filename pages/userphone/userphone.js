// pages/userphone/userphone.js
const util = require('../../utils/util.js')
var interval = null //倒计时函数

Page({

  /**
   * 页面的初始数据
   */
  data: {
    time: '获取验证码', //倒计时 
    currentTime: 60,
    tempPhone: ''
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var member = util.getUserdata('member')
    console.log(member);
    this.setData({
      member: member
    })
  },
  phonenew_input:function(e){
    var phonenew = e.detail.value;
    this.setData({
      tempPhone: e.detail.value
    })

    util.setUserdata('phonenew', phonenew);

    var member = util.getUserdata('member')
    member.phonenew = phonenew;
   
    util.setUserdata('member', member);

    console.log(util.getUserdata('phonenew'));
  },
  phonecode_input:function(e){
    var phonecode = e.detail.value;

    util.setUserdata('phonecode', phonecode);

    var member = util.getUserdata('member')
    member.phonecode = phonecode;

    util.setUserdata('member', member);

    console.log(util.getUserdata('phonecode'));
  },
  checkPhoneNum: function (phoneNumber) {
    let str = /^1[34578]\d{9}$/
    if (str.test(phoneNumber)) {
      return true
    } else {
      wx.showToast({

        title: '手机号不正确',
        icon: 'none'
      })
      return false
    }
  },
  bindGetNotarizationTap:function(){
    var that = this;
    var member = util.getUserdata('member');

    if (member.phonenew == '' || !member.phonenew || that.checkPhoneNum(member.phonenew) == false) {
      wx.showToast({
        title: '请输入正确手机号码！',
        icon: 'none',
        duration: 2000
      });
      return false;
    }

    if (member.phonecode == '' || !member.phonecode) {
      wx.showToast({
        title: '请输入验证码！',
        icon: 'none',
        duration: 2000
      });
      return false;
    }

    util.request('V2/changephone', { 'phonecode': member.phonecode, 'phone': member.phonenew},
      function (res) {
        console.log(res);
        if (res.data.code == 0) {
          //  console.log(res);
          wx.showToast({
            title: '操作成功！',
            icon: 'none',
            duration: 2000
          });
          wx.navigateTo({
            url: '../my_notarization/my_notarization',
          })
          // wx.navigateBack({ changed: true });//返回上一页  
          /*
          wx.navigateTo({
            url: '../my_notarization/my_notarization',
          }) */
        } else if (res.data.code == -10003) {
          wx.showToast({
            title: '验证码错误！',
            icon: 'none',
            duration: 2000
          });
        } else {
          wx.showToast({
            title: '操作失败，请重试！',
            icon: 'none',
            duration: 2000
          });
        }
      })
  },
  getCode: function (options) {
    var that = this;
    var currentTime = that.data.currentTime
    interval = setInterval(function () {
      currentTime--;
      that.setData({
        time: ''+currentTime + 's'
      })
      if (currentTime <= 0) {
        clearInterval(interval)
        that.setData({
          time: '重新获取',
          currentTime: 60,
          disabled: false
        })
      }
    }, 1000)
  },
  getVerificationCode() {
    var that = this;
    var phoneNumber = that.data.tempPhone;

    if (phoneNumber && (!(/^[1][34578]\d{9}$/).test(phoneNumber) || !(/^[1-9]\d*$/).test(phoneNumber) || phoneNumber.length !== 11)){
      wx.showToast({
        title: '手机号码不符合规范',
        icon: 'none',
        duration: 2000
      })
      return false
    }
    
    util.request('V2/senmsg', {  'phone': util.getUserdata('phonenew') },
      function (res) {
        console.log(res);
        if (res.data.code == 0) {
         
          that.getCode();
       //   var that = this
          that.setData({
            disabled: true
          })

        } else if (res.data.code == -10003) {
          wx.showToast({
            title: '验证码错误！',
            icon: 'none',
            duration: 2000
          });
        } else {
          wx.showToast({
            title: '获取验证码失败，请重试！',
            icon: 'none',
            duration: 2000
          });
        }
      })

   /* this.getCode();
    var that = this
    that.setData({
      disabled: true
    }) */
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})