// pages/receive_info/receive_info.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  unique_receive_info: function() {
    var receive_info = util.getUserArr('help_receive_info')
    console.log('受让人去重')
    var one = 1
    console.log(receive_info)
    for (var i = 0; i < receive_info.length; i++) {
      if (receive_info[i].receive_type == 1){
        var receive_personal_flag = 0
        if (!receive_info[i].personal_name || receive_info[i].personal_name == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_card_front_vc || receive_info[i].identity_card_front_vc == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_card_back_vc || receive_info[i].identity_card_back_vc == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_code || receive_info[i].identity_code == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_gender || (receive_info[i].personal_gender != 1 && receive_info[i].personal_gender != 2)) {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_birth_date || receive_info[i].personal_birth_date == '') {
          receive_personal_flag += one
        }
        if (!util.checkDate(receive_info[i].personal_birth_date)) {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_phone || receive_info[i].personal_phone == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_address || receive_info[i].personal_address == '') {
          receive_personal_flag += one
        }

        console.log(receive_personal_flag)
        if (receive_personal_flag == 9) {
          receive_info.splice(i, 1)
        }

      } else if (receive_info[i].receive_type == 2) {
        var receive_company_flag = 0
        if (!receive_info[i].company_name || receive_info[i].company_name == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].business_license_vc || receive_info[i].business_license_vc == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].social_credit_code || receive_info[i].social_credit_code == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_personal_name || receive_info[i].company_personal_name == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_phone || receive_info[i].company_phone == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_address || receive_info[i].company_address == '') {
          receive_company_flag += one
        }

        console.log(receive_company_flag)
        if (receive_company_flag == 6) {
          receive_info.splice(i, 1)
        }
      } else if (receive_info[i].receive_type == 3) {
        var receive_overseas_flag = 0
        if (!receive_info[i].company_name || receive_info[i].company_name == '') {
          receive_overseas_flag += one
        }
        if (!receive_info[i].business_license_vc || receive_info[i].business_license_vc == '') {
          receive_overseas_flag += one
        }
        if (!receive_info[i].social_credit_code || receive_info[i].social_credit_code == '') {
          receive_overseas_flag += one
        }
        if (!receive_info[i].company_personal_name || receive_info[i].company_personal_name == '') {
          receive_overseas_flag += one
        }
        if (!receive_info[i].company_phone || receive_info[i].company_phone == '') {
          receive_overseas_flag += one
        }
        if (!receive_info[i].company_address || receive_info[i].company_address == '') {
          receive_overseas_flag += one
        }

        console.log(receive_overseas_flag)
        if (receive_overseas_flag == 6) {
          receive_info.splice(i, 1)
        }
      }
    }

    util.setUserdata('help_receive_info', receive_info)
  },
  help_receive_info_personal: function(){
    wx.navigateTo({
      url: '../help_receive_info_personal/help_receive_info_personal',
    })
  },
  help_receive_info_company: function(){
    wx.navigateTo({
      url: '../help_receive_info_company/help_receive_info_company',
    })
  },
  help_receive_info_overseas: function(){
    wx.navigateTo({
      url: '../help_receive_info_overseas/help_receive_info_overseas',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.unique_receive_info()
    this.reload();
  },
  reload: function () {
    var receive_info = util.getUserArr('help_receive_info')
    console.log(receive_info)
    var receive_remark = ''
    var receive_remarkq = ''
    var receive_remarkjw = ''
    for (var j = 0; j < receive_info.length; j++) {

      if (receive_info.length > 0 && receive_info[j]) {
        var receive = receive_info[j];
        if (receive.receive_type==1)
        {
          var receive_remarktmp="";
          if ((receive.personal_name && receive.personal_name != '') || (receive.company_name && receive.company_name != '')) {
            receive_remarktmp += "受让人：" + (receive.personal_name || receive.company_name || '') + '； ';
          }
          if (receive.receive_type == 1) {
            if (receive.identity_code && receive.identity_code != '')
              receive_remarktmp += "身份证号：" + (receive.identity_code || '') + '； ';
            // if (receive.identity_address && receive.identity_address != '')
            //   receive_remarktmp += "地址：" + (receive.identity_address || '') + '； ';
          }
          if (receive.receive_type == 2) {
            // if (receive.social_credit_code && receive.social_credit_code != '')
              // receive_remarktmp += "社会信用代码：" + (receive.social_credit_code || '') + '； ';
            // if (receive.license_address && receive.license_address != '')
            //   receive_remarktmp += "地址：" + (receive.license_address || '') + '； ';
          }
          if (receive_remarktmp)
          {
            receive_remark = receive_remark+ receive_remarktmp+ '\n';
          }  
         
        } else if (receive.receive_type == 2){
          var receive_remarkqtmp="";
          if ((receive.personal_name && receive.personal_name != '') || (receive.company_name && receive.company_name != '')) {
            receive_remarkqtmp += "受让人：" + (receive.personal_name || receive.company_name || '') + '； ';
          }
          if (receive.receive_type == 1) {
            if (receive.identity_code && receive.identity_code != '')
              receive_remarkqtmp += "身份证号：" + (receive.identity_code || '') + '； ';
            // if (receive.identity_address && receive.identity_address != '')
            //   receive_remarkqtmp += "地址：" + (receive.identity_address || '') + '； ';
          }
          if (receive.receive_type == 2) {
            // if (receive.social_credit_code && receive.social_credit_code != '')
              // receive_remarkqtmp += "社会信用代码：" + (receive.social_credit_code || '') + '； ';
            // if (receive.license_address && receive.license_address != '')
            //   receive_remarkqtmp += "地址：" + (receive.license_address || '') + '； ';
          }
          if (receive_remarkqtmp)
          {
            receive_remarkq = receive_remarkq+ receive_remarkqtmp+ '\n';
          } 
         
        } else if (receive.receive_type == 3){
          var receive_remarkjwtmp="";
          if ((receive.personal_name && receive.personal_name != '') || (receive.company_name && receive.company_name != '')) {
            receive_remarkjwtmp += "受让人：" + (receive.personal_name || receive.company_name || '') + '； ';
          }
          if (receive.receive_type == 1) {
            // if (receive.identity_code && receive.identity_code != '')
            //   receive_remarkjwtmp += "身份证号：" + (receive.identity_code || '') + '； ';
            // if (receive.identity_address && receive.identity_address != '')
            //   receive_remarkjwtmp += "地址：" + (receive.identity_address || '') + '； ';
          }
          if (receive.receive_type == 3) {
            // if (receive.social_credit_code && receive.social_credit_code != '')
              // receive_remarkjwtmp += "公司注册编码：" + (receive.social_credit_code || '') + '； ';
            // if (receive.license_address && receive.license_address != '')
            //   receive_remarkjwtmp += "地址：" + (receive.license_address || '') + '； ';
          }
          if (receive_remarkjwtmp)
          {
            receive_remarkjw = receive_remarkjw+ receive_remarkjwtmp+ '\n';1
          }
        }
      
      }
    

    }

    if (receive_remark == '') {
      receive_remark = '暂无资料'
    }

    if (receive_remarkq == '') {
      receive_remarkq = '暂无资料'
    }
    if (receive_remarkjw == '') {
      receive_remarkjw = '暂无资料'
    }
    this.setData({
      receive_remark: receive_remark,
      receive_remarkq: receive_remarkq,
      receive_remarkjw: receive_remarkjw
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.unique_receive_info()
    this.reload();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})