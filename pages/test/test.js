// pages/test/test.js
Page({

  /**
   * 页面的初始数据1
   */
  data: {
    text: '罗伟烽'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // 生命周期函数--监听页面初次渲染完成
    var ctx = wx.createCanvasContext('canvas')
    // 设置背景
    ctx.setFillStyle('#ffffff')
    ctx.fillRect(0, 0, 300, 355)
    // logo
    // ctx.drawImage(this.data.logo, 100, 9, 100, 33)
    // logo下线条
    ctx.setStrokeStyle("rgba(0,0,0,0)")
    ctx.setLineWidth(1)
    ctx.moveTo(0, 49)
    ctx.lineTo(300, 49)
    ctx.stroke()

    ctx.setFontSize(32)
    ctx.font = 'normal normal 32px kaiti'
    ctx.setFillStyle("#000000")
    textHandle(this.data.text, 80, 70, 250, 32);


    // textHandle('罗伟烽', 100, 100, 150);

    /**
     * @function textHandle 绘制文本的换行处理
     * @param text 在画布上输出的文本
     * @param numX 绘制文本的左上角x坐标位置
     * @param numY 绘制文本的左上角y坐标位置
     * @param textWidth 文本宽度
     * @param lineHeight 文本的行高
     * @author Moss
     */
    function textHandle(text, numX, numY, textWidth, lineHeight) {
      var chr = text.split(""); // 将一个字符串分割成字符串数组
      var temp = "";
      var row = [];
      for (var a = 0; a < chr.length; a++) {
        if (ctx.measureText(temp).width < textWidth) {
          temp += chr[a];
        } else {
          a--; // 添加a--，防止字符丢失
          row.push(temp);
          temp = "";
        }
      }
      row.push(temp);

      // 如果数组长度大于2 则截取前两个
      if (row.length > 2) {
        var rowCut = row.slice(0, 2);
        var rowPart = rowCut[1];
        var test = "";
        var empty = [];
        for (var a = 0; a < rowPart.length; a++) {
          if (ctx.measureText(test).width < textWidth - 10) {
            test += rowPart[a];
          } else {
            break;
          }
        }
        empty.push(test);
        var group = empty[0] + "..."; // 这里只显示两行，超出的用...展示
        rowCut.splice(1, 1, group);
        row = rowCut;
      }
      for (var b = 0; b < row.length; b++) {
        ctx.fillText(row[b], numX, numY + b * lineHeight);
      }
    }

    // 完成
    ctx.draw()

  },
  saveImage() {
    wx.canvasToTempFilePath({
      canvasId: 'canvas',
      success(res) {
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success(res) {
            wx.showToast({
              title: '保存成功',
              icon: 'success'
            })
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})