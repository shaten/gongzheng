const util = require('../../utils/util.js')
Page({
  data: {
    orderStatus: ["全部", "待提交", "待出证", "已出证"],
    orderStatusIndex: 0,
    getFlow: ["未知","待付款", "已付款", "已上传签署文件", "已送出材料"],
    getOrder: ["未知", "待提交", "待出证", "已出证"]
  },

  del: function (e) {
    var order_id = e.currentTarget.dataset.id;
    console.log("订单order_id=="+order_id);

    var that = this;

    util.request('V2/orderdel',
      {
        'order_id': order_id
      },
      function (res) { 
          console.log(res);
          if (res.data.code == 0) {
            wx.showToast({
              title: '删除成功',
              icon: 'none',
              duration: 2000
            });
            that.reload(util.getUserdata('status'));

          }
        });

  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../my_notarization/my_notarization',
    })
  },
  bindOrderInfoTap: function(e){
    console.log(e)
    wx.navigateTo({
      url: '../order_info/order_info?id=' + e.currentTarget.dataset.id,
    })
  },
  updateOrder: function(e) {
    console.log(e.currentTarget.dataset.item)
    var current_transfer_info = e.currentTarget.dataset.item
    // console.log(util.getUserArr('transfer_info'));
    // if(util.getUserArr('transfer_info').)

    util.removeUserData('transfer_info')
    //把本地storage存放在 transfer_info => transfer_info_temp
    var transfer_info = util.getUserArr('transfer_info')
    console.log(transfer_info)
    // util.setUserdata('transfer_info_temp', transfer_info)
    


    if (current_transfer_info.main_transfer.transfer_type == 1) {

    } else if (current_transfer_info.main_transfer.transfer_type == 2) {
      console.log('公司');

      console.log(current_transfer_info.main_transfer)
      // console.log(current_transfer_info.main_transfer.length);
      // var arr = Object.keys(current_transfer_info.main_transfer)
      // console.log(arr);
      // // for(var i = 0; i < )
      // for (var i = 0; i < arr.length; i++) {

      // }
      var temp = {}
      temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.main_transfer.business_license_path]
      temp.company_birth_date = current_transfer_info.main_transfer.company_birth_date
      temp.company_address = current_transfer_info.main_transfer.company_address
      temp.company_gender = current_transfer_info.main_transfer.company_gender
      temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.main_transfer.company_identity_card_back_path]
      temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.main_transfer.company_identity_card_front_path] 
      temp.company_identity_code = current_transfer_info.main_transfer.company_identity_code
      temp.company_name = current_transfer_info.main_transfer.company_name
      temp.company_personal_name = current_transfer_info.main_transfer.company_personal_name
      temp.company_phone = current_transfer_info.main_transfer.company_phone
      temp.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.main_transfer.company_seal_path] 
      temp.license_address = current_transfer_info.main_transfer.license_address
      temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.main_transfer.signature_path]
      temp.social_credit_code = current_transfer_info.main_transfer.social_credit_code
      temp.verifycode = current_transfer_info.main_transfer.verifycode
      temp.transfer_type = 2
      
      console.log(temp);


      // temp.business_license_path = current_transfer_info.main_transfer.business_license_path
      // temp.company_birth_date = current_transfer_info.main_transfer.company_birth_date
      // temp.company_address = current_transfer_info.main_transfer.company_address
      // temp.company_gender = current_transfer_info.main_transfer.company_gender
      // temp.company_identity_card_back_path = current_transfer_info.main_transfer.company_identity_card_back_path
      // temp.company_identity_card_front_path = current_transfer_info.main_transfer.company_identity_card_front_path
      // temp.company_identity_code = current_transfer_info.main_transfer.company_identity_code
      // temp.company_name = current_transfer_info.main_transfer.company_name
      // temp.company_personal_name = current_transfer_info.main_transfer.company_personal_name
      // temp.company_phone = current_transfer_info.main_transfer.company_phone
      // temp.seal_path = current_transfer_info.main_transfer.company_seal_path
      // temp.license_address = current_transfer_info.main_transfer.license_address
      // temp.signature_path = current_transfer_info.main_transfer.signature_path
      // temp.social_credit_code = current_transfer_info.main_transfer.social_credit_code
      // temp.verifycode = current_transfer_info.main_transfer.verifycode
      // temp.transfer_type = 2
      
    }


    console.log(transfer_info);

    transfer_info.push(temp)
    console.log(transfer_info);
    util.setUserdata('transfer_info', transfer_info)
    console.log(transfer_info);
    return false;
    wx.navigateTo({
      // url: '../transfer_info_select/transfer_info'
      url: '../get_notarization/get_notarization?orderInfoId=' + current_transfer_info.main_transfer.transfer_id,
    })

  },
  bindOrderStatusChange: function(e) {
        console.log('picker account 发生选择改变，携带值为', e.detail.value);

        this.setData({
            orderStatusIndex: e.detail.value
        })
        this.reload(e.detail.value)
  },
  reload: function(status){
    var that = this;
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    if (status == 2) status = '2,4'
    util.request('V2/orderList', {'order_status': status},
      function(res){
        console.log(res);
        if (res.data.code == 0) {
          that.setData({
            order_count: res.data.order_count,
            order_list: res.data.order_list
          })
        }
      },
      function(res){
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function(res) {
        wx.hideToast();
      })
  },
  onLoad: function (opt){
    this.reload(opt.status)
    util.setUserdata('status', opt.status);
  }

})