const util = require('../../utils/util.js')
Page({
  data: {
    orderStatus: ["全部", "待提交", "待出证", "已出证", "已驳回","待认证","修改中"],
    orderStatusIndex: 0,
    getFlow: ["未知","待付款", "已付款", "已上传签署文件", "已送出材料"],
    getOrder: ["未知", "待提交", "待出证", "已出证", "已驳回","待认证", "修改中"],
    nowStatus: ''
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.reload(this.data.nowStatus)
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  toCertification: function (e) {
    var that = this;
    var order_id = e.currentTarget.dataset.id;
    var updateStatus = e.currentTarget.dataset.update
    console.log("订单order_id==" + order_id);
    console.log('去认证')
    wx.navigateTo({
      url: '../share_certification/share_certification?id=' + order_id + '&update=' + updateStatus,
    })
    
    
  },
  toShare: function (e) {
    var order_id = e.currentTarget.dataset.id;
    var updateStatus = e.currentTarget.dataset.update
    console.log("订单order_id==" + order_id);

    var that = this;
    wx.navigateTo({
      url: '../share_certification/share_certification?id=' + order_id + '&update=' + updateStatus,
    })
  },
  del: function (e) {
    var order_id = e.currentTarget.dataset.id;
    console.log("订单order_id=="+order_id);

    var that = this;

    util.request('V2/orderdel',
      {
        'order_id': order_id
      },
      function (res) { 
          console.log(res);
          if (res.data.code == 0) {
            wx.showToast({
              title: '删除成功',
              icon: 'none',
              duration: 2000
            });
            that.reload(util.getUserdata('status'));

          }
        });

  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../my_notarization/my_notarization',
    })
  },
  bindOrderInfoTap: function(e){1
    console.log(e)
    wx.navigateTo({
      url: '../order_info/order_info?id=' + e.currentTarget.dataset.id,
    })
  },
  updateOtherOrder: function(e) {
    let currentOrderInfo = e.currentTarget.dataset.item
    console.log(currentOrderInfo)

    util.removeUserData('help_transfer_info')
    util.removeUserData('help_receive_info')
    util.removeUserData('help_brand_info')
    util.removeUserData('help_other')

    let help_transfer_info = util.getUserArr('help_transfer_info')
    let help_receive_info = util.getUserArr('help_receive_info')
    let help_brand_info = util.getUserArr('help_brand_info')
    let help_other = util.getUserArr('help_other')

    //装让人集合
    for (let i = 0; i < currentOrderInfo.transfer_list.length; i++) {
      //转让人
      if (currentOrderInfo.transfer_list[i].transfer_type == 1) {
        console.log(currentOrderInfo.transfer_list[i])
        let temp = {}
        temp.identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].identity_card_front_path]
        temp.identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].identity_card_back_path]
        temp.identity_card_front_vc = currentOrderInfo.transfer_list[i].identity_card_front_vc
        temp.identity_card_back_vc = currentOrderInfo.transfer_list[i].identity_card_back_vc
        temp.identity_address = currentOrderInfo.transfer_list[i].identity_address
        temp.identity_code = currentOrderInfo.transfer_list[i].identity_code
        temp.personal_address = currentOrderInfo.transfer_list[i].personal_address
        temp.personal_birth_date = currentOrderInfo.transfer_list[i].personal_birth_date
        temp.personal_gender = currentOrderInfo.transfer_list[i].personal_gender
        temp.personal_name = currentOrderInfo.transfer_list[i].personal_name
        temp.personal_phone = currentOrderInfo.transfer_list[i].personal_phone

        // temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].signature_path]
        // temp.signature_vc = currentOrderInfo.transfer_list[i].signature_vc
        temp.signature_path = []
        temp.signature_vc = ''

        temp.verifycode = ''
        temp.apply_no = '0'
        temp.transfer_type = 1
        help_transfer_info.push(temp)
        console.log(help_transfer_info)
      } 
      //转让公司
      else if (currentOrderInfo.transfer_list[i].transfer_type == 2) {
        console.log(currentOrderInfo.transfer_list[i])
        let temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].business_license_path]
        temp.business_license_vc = currentOrderInfo.transfer_list[i].business_license_vc
        temp.company_birth_date = currentOrderInfo.transfer_list[i].company_birth_date
        temp.company_address = currentOrderInfo.transfer_list[i].company_address
        temp.company_gender = currentOrderInfo.transfer_list[i].company_gender
        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = currentOrderInfo.transfer_list[i].company_identity_card_back_vc
        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = currentOrderInfo.transfer_list[i].company_identity_card_front_vc
        temp.company_identity_code = currentOrderInfo.transfer_list[i].company_identity_code
        temp.company_name = currentOrderInfo.transfer_list[i].company_name
        temp.company_personal_name = currentOrderInfo.transfer_list[i].company_personal_name
        temp.company_phone = currentOrderInfo.transfer_list[i].company_phone
        temp.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_seal_path]
        temp.seal_vc = currentOrderInfo.transfer_list[i].seal_vc
        temp.license_address = currentOrderInfo.transfer_list[i].license_address
        // temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].signature_path]
        // temp.signature_vc = currentOrderInfo.transfer_list[i].signature_vc

        temp.signature_path = []
        temp.signature_vc = ''
        temp.social_credit_code = currentOrderInfo.transfer_list[i].social_credit_code

        temp.verifycode = ''
        temp.apply_no = '0'
        temp.transfer_type = 2
        help_transfer_info.push(temp)
      }
      //境外公司(香港公司)
      else if (currentOrderInfo.transfer_list[i].transfer_type == 3) {
        console.log(currentOrderInfo.transfer_list[i])
        let temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].business_license_path]

        temp.business_license_vc = currentOrderInfo.transfer_list[i].business_license_vc
        temp.company_birth_date = currentOrderInfo.transfer_list[i].company_birth_date
        temp.company_address = currentOrderInfo.transfer_list[i].company_address
        temp.company_gender = currentOrderInfo.transfer_list[i].company_gender

        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = currentOrderInfo.transfer_list[i].company_identity_card_back_vc

        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = currentOrderInfo.transfer_list[i].company_identity_card_front_vc

        temp.company_identity_code = currentOrderInfo.transfer_list[i].company_identity_code
        temp.company_name = currentOrderInfo.transfer_list[i].company_name
        temp.company_personal_name = currentOrderInfo.transfer_list[i].company_personal_name
        temp.company_phone = currentOrderInfo.transfer_list[i].company_phone
        temp.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].company_seal_path]
        temp.seal_vc = currentOrderInfo.transfer_list[i].seal_vc

        temp.license_address = currentOrderInfo.transfer_list[i].license_address
        // temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.transfer_list[i].signature_path]
        // temp.signature_vc = currentOrderInfo.transfer_list[i].signature_vc
        temp.signature_path = []
        temp.signature_vc = ''
        temp.social_credit_code = currentOrderInfo.transfer_list[i].social_credit_code

        temp.lawyer_img = currentOrderInfo.transfer_list[i].lawyer_img

        var layer_backimg = currentOrderInfo.transfer_list[i].lawyer_img.split(',')
        for (var k = 0; k < layer_backimg.length; k++) {
          console.log(layer_backimg[k])
          layer_backimg[k] = 'https://gongzheng.ma.cn/MYPublic/' + layer_backimg[k]
          
        }

        temp.overseas_other_files = layer_backimg
        temp.verifycode = ''
        temp.apply_no = '0'
        temp.transfer_type = 3
        help_transfer_info.push(temp)

      }
    }

    //受让人集合
    for (var i = 0; i < currentOrderInfo.receive_list.length; i++) {
      //受让人
      if (currentOrderInfo.receive_list[i].receive_type == 1) {
        console.log(currentOrderInfo.receive_list[i])
        let temp = {}
        console.log(currentOrderInfo.receive_list[i].personal_name)
        temp.identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].identity_card_front_path]
        temp.identity_card_front_vc = currentOrderInfo.receive_list[i].identity_card_front_vc
        temp.identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].identity_card_back_path]
        temp.identity_card_back_vc = currentOrderInfo.receive_list[i].identity_card_back_vc
        temp.identity_address = currentOrderInfo.receive_list[i].identity_address
        temp.personal_name = currentOrderInfo.receive_list[i].personal_name
        temp.identity_code = currentOrderInfo.receive_list[i].identity_code
        temp.personal_birth_date = currentOrderInfo.receive_list[i].personal_birth_date
        temp.personal_phone = currentOrderInfo.receive_list[i].personal_phone
        temp.personal_address = currentOrderInfo.receive_list[i].personal_address
        if (currentOrderInfo.receive_list[i].personal_gender_text == '未知') {
          temp.personal_gender = 0
        } else if (currentOrderInfo.receive_list[i].personal_gender_text == '男') {
          temp.personal_gender = 1
        } else if (currentOrderInfo.receive_list[i].personal_gender_text == '女') {
          temp.personal_gender = 2
        }
        temp.receive_type = 1
        help_receive_info.push(temp)
      }
      //受让公司
      else if (currentOrderInfo.receive_list[i].receive_type == 2) {
        console.log(currentOrderInfo.receive_list[i])
        let temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].business_license_path]
        temp.business_license_vc = currentOrderInfo.receive_list[i].business_license_vc
        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = currentOrderInfo.receive_list[i].company_identity_card_front_vc
        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = currentOrderInfo.receive_list[i].company_identity_card_back_vc
        temp.company_name = currentOrderInfo.receive_list[i].company_name
        temp.social_credit_code = currentOrderInfo.receive_list[i].social_credit_code
        temp.company_personal_name = currentOrderInfo.receive_list[i].company_personal_name
        temp.company_address = currentOrderInfo.receive_list[i].company_address
        temp.company_phone = currentOrderInfo.receive_list[i].company_phone
        temp.company_identity_code = currentOrderInfo.receive_list[i].company_identity_code
        if (currentOrderInfo.receive_list[i].company_gender_text == '未知') {
          temp.company_gender = 0
        } else if (currentOrderInfo.receive_list[i].company_gender_text == '男') {
          temp.company_gender = 1
        } else if (currentOrderInfo.receive_list[i].company_gender_text == '女') {
          temp.company_gender = 2
        }
        temp.company_birth_date = currentOrderInfo.receive_list[i].company_birth_date
        temp.license_address = currentOrderInfo.receive_list[i].license_address
        temp.receive_type = 2
        help_receive_info.push(temp)
      }
      //受让境外(香港公司)
      else if (currentOrderInfo.receive_list[i].receive_type == 3) {
        console.log(currentOrderInfo.receive_list[i])
        let temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].business_license_path]
        temp.business_license_vc = currentOrderInfo.receive_list[i].business_license_vc
        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = currentOrderInfo.receive_list[i].company_identity_card_front_vc
        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.receive_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = currentOrderInfo.receive_list[i].company_identity_card_back_vc
        temp.company_name = currentOrderInfo.receive_list[i].company_name
        temp.social_credit_code = currentOrderInfo.receive_list[i].social_credit_code
        temp.company_personal_name = currentOrderInfo.receive_list[i].company_personal_name
        temp.company_address = currentOrderInfo.receive_list[i].company_address
        temp.company_phone = currentOrderInfo.receive_list[i].company_phone
        temp.company_identity_code = currentOrderInfo.receive_list[i].company_identity_code
        if (currentOrderInfo.receive_list[i].company_gender_text == '未知') {
          temp.company_gender = 0
        } else if (currentOrderInfo.receive_list[i].company_gender_text == '男') {
          temp.company_gender = 1
        } else if (currentOrderInfo.receive_list[i].company_gender_text == '女') {
          temp.company_gender = 2
        }
        temp.company_birth_date = currentOrderInfo.receive_list[i].company_birth_date
        temp.license_address = currentOrderInfo.receive_list[i].license_address
        temp.receive_type = 3
        help_receive_info.push(temp)
      }
    }

    //商标集合
    var brand_temp = {}
    var brand_image_list_temp = []
    var brand_image_list_vc_temp = []
    for (var i = 0; i < currentOrderInfo.brand_image_list.length; i++) {
      brand_image_list_temp.push('https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.brand_image_list[i].image_path)
      brand_image_list_vc_temp.push(currentOrderInfo.brand_image_list[i].brand_vc)
    }
    brand_temp.brand_files = brand_image_list_temp
    brand_temp.brand_vc = brand_image_list_vc_temp

    var brand_other_image_list = []
    var brand_other_image_vc_list = []
    for (var i = 0; i < currentOrderInfo.brand_other_image_list.length; i++) {
      brand_other_image_list.push('https://gongzheng.ma.cn/MYPublic/' + currentOrderInfo.brand_other_image_list[i].image_path)
      brand_other_image_vc_list.push(currentOrderInfo.brand_other_image_list[i].brand_other_vc)
    }

    brand_temp.brand_other_files = brand_other_image_list
    brand_temp.brand_other_vc = brand_other_image_vc_list

    brand_temp.brand_register_code = currentOrderInfo.brand_register_code
    brand_temp.brand_name = currentOrderInfo.brand_name
    brand_temp.possessor_name = currentOrderInfo.possessor_name
    brand_temp.address = currentOrderInfo.address
    brand_temp.type_num = currentOrderInfo.type_num
    brand_temp.other_possessor = currentOrderInfo.other_possessor
    brand_temp.validity = currentOrderInfo.validity

    //其他信息
    var other_temp = {}
    other_temp.page_count = currentOrderInfo.page_count
    other_temp.mail_type = currentOrderInfo.mail_type == '0' ? false : true
    other_temp.send_type = currentOrderInfo.send_type == '0' ? false : true
    other_temp.care_type = currentOrderInfo.care_type == '0' ? false : true
    other_temp.comm_type = currentOrderInfo.comm_type == '0' ? false : true
    other_temp.remark = currentOrderInfo.remark

    // util.setUserdata('transfer_info_apply_nos', "");
    util.setUserdata('help_transfer_info', help_transfer_info)
    util.setUserdata('help_receive_info', help_receive_info)
    util.setUserdata('help_brand_info', brand_temp)
    util.setUserdata('help_other', other_temp)
    util.setUserdata('help_order_number', currentOrderInfo.order_num)
    util.setUserdata('help_order_id', currentOrderInfo.order_id)

    wx.navigateTo({
      // url: '../transfer_info_select/transfer_info'
      url: '../help_get_notarization/help_get_notarization?orderInfoId=' + currentOrderInfo.order_id,
    })

  },
  updateOrder: function(e) {
    console.log(e.currentTarget.dataset.item)
    var current_transfer_info = e.currentTarget.dataset.item
    console.log(util.getUserArr('transfer_info'));

    util.removeUserData('transfer_info')
    util.removeUserData('receive_info')
    util.removeUserData('brand_info')
    util.removeUserData('other')
    //把本地storage存放在 transfer_info => transfer_info_temp
    var transfer_info = util.getUserArr('transfer_info')
    var receive_info = util.getUserArr('receive_info')
    var brand_info = util.getUserArr('brand_info')
    var other = util.getUserArr('other')
    console.log(transfer_info)

    
    // console.log(transfer_info)
    // util.setUserdata('transfer_info_temp', transfer_info)
    
    // var arr_transfer_list = Object.keys(current_transfer_info.transfer_list)
    // console.log(current_transfer_info.transfer_list.length)
    for (var i = 0; i < current_transfer_info.transfer_list.length; i++) {
      if (current_transfer_info.transfer_list[i].transfer_type == 1) {
        var temp = {}
        temp.identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].identity_card_front_path]
        temp.identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].identity_card_back_path]
        temp.identity_card_front_vc = current_transfer_info.transfer_list[i].identity_card_front_vc
        temp.identity_card_back_vc = current_transfer_info.transfer_list[i].identity_card_back_vc

        temp.identity_address = current_transfer_info.transfer_list[i].identity_address
        temp.identity_code = current_transfer_info.transfer_list[i].identity_code
        temp.personal_address = current_transfer_info.transfer_list[i].personal_address
        temp.personal_birth_date = current_transfer_info.transfer_list[i].personal_birth_date
        temp.personal_gender = current_transfer_info.transfer_list[i].personal_gender
        temp.personal_name = current_transfer_info.transfer_list[i].personal_name
        temp.personal_phone = current_transfer_info.transfer_list[i].personal_phone
        temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].signature_path]
        temp.signature_vc = current_transfer_info.transfer_list[i].signature_vc
        temp.verifycode = ''
        temp.transfer_type = 1
        transfer_info.push(temp)
      } else if (current_transfer_info.transfer_list[i].transfer_type == 2) {
        var temp = {}
        console.log(i)
        console.log(current_transfer_info.transfer_list);

        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].business_license_path]

        temp.business_license_vc = current_transfer_info.transfer_list[i].business_license_vc
        temp.company_birth_date = current_transfer_info.transfer_list[i].company_birth_date
        temp.company_address = current_transfer_info.transfer_list[i].company_address
        temp.company_gender = current_transfer_info.transfer_list[i].company_gender

        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = current_transfer_info.transfer_list[i].company_identity_card_back_vc

        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = current_transfer_info.transfer_list[i].company_identity_card_front_vc

        temp.company_identity_code = current_transfer_info.transfer_list[i].company_identity_code
        temp.company_name = current_transfer_info.transfer_list[i].company_name
        temp.company_personal_name = current_transfer_info.transfer_list[i].company_personal_name
        temp.company_phone = current_transfer_info.transfer_list[i].company_phone
        temp.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_seal_path]
        temp.seal_vc = current_transfer_info.transfer_list[i].seal_vc

        temp.license_address = current_transfer_info.transfer_list[i].license_address
        temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].signature_path]
        temp.signature_vc = current_transfer_info.transfer_list[i].signature_vc
        temp.social_credit_code = current_transfer_info.transfer_list[i].social_credit_code
        temp.verifycode = ''
        temp.transfer_type = 2
        transfer_info.push(temp)
      } else if (current_transfer_info.transfer_list[i].transfer_type == 3) {
        var temp = {}
        console.log(i)
        console.log(current_transfer_info.transfer_list);

        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].business_license_path]

        temp.business_license_vc = current_transfer_info.transfer_list[i].business_license_vc
        temp.company_birth_date = current_transfer_info.transfer_list[i].company_birth_date
        temp.company_address = current_transfer_info.transfer_list[i].company_address
        temp.company_gender = current_transfer_info.transfer_list[i].company_gender

        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = current_transfer_info.transfer_list[i].company_identity_card_back_vc

        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = current_transfer_info.transfer_list[i].company_identity_card_front_vc

        temp.company_identity_code = current_transfer_info.transfer_list[i].company_identity_code
        temp.company_name = current_transfer_info.transfer_list[i].company_name
        temp.company_personal_name = current_transfer_info.transfer_list[i].company_personal_name
        temp.company_phone = current_transfer_info.transfer_list[i].company_phone
        temp.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].company_seal_path]
        temp.seal_vc = current_transfer_info.transfer_list[i].seal_vc

        temp.license_address = current_transfer_info.transfer_list[i].license_address
        temp.signature_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.transfer_list[i].signature_path]
        temp.signature_vc = current_transfer_info.transfer_list[i].signature_vc
        temp.social_credit_code = current_transfer_info.transfer_list[i].social_credit_code

        temp.lawyer_img = current_transfer_info.transfer_list[i].lawyer_img
        
        var layer_backimg = current_transfer_info.transfer_list[i].lawyer_img.split(',')
        for (var k = 0; k < layer_backimg.length; k++){
          console.log(layer_backimg[k])
          layer_backimg[k] = 'https://gongzheng.ma.cn/MYPublic/' + layer_backimg[k]
        }

        temp.overseas_other_files = layer_backimg
        temp.verifycode = ''
        temp.transfer_type = 3
        transfer_info.push(temp)
      }
    }

    // var arr_receive_list = Object.keys(current_transfer_info.receive_list)
    console.log(current_transfer_info.receive_list)
    // console.log(arr_receive_list.legth)
    for (var i = 0; i < current_transfer_info.receive_list.length; i++) {
      console.log(current_transfer_info.receive_list[i].receive_type);
      if (current_transfer_info.receive_list[i].receive_type == 1) {
        var temp = {}
        console.log(current_transfer_info.receive_list[i].personal_name)
        temp.identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].identity_card_front_path]
        temp.identity_card_front_vc = current_transfer_info.receive_list[i].identity_card_front_vc
        temp.identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].identity_card_back_path]
        temp.identity_card_back_vc = current_transfer_info.receive_list[i].identity_card_back_vc
        temp.identity_address = current_transfer_info.receive_list[i].identity_address
        temp.personal_name = current_transfer_info.receive_list[i].personal_name
        temp.identity_code = current_transfer_info.receive_list[i].identity_code
        temp.personal_birth_date = current_transfer_info.receive_list[i].personal_birth_date
        temp.personal_phone = current_transfer_info.receive_list[i].personal_phone
        temp.personal_address = current_transfer_info.receive_list[i].personal_address
        if (current_transfer_info.receive_list[i].personal_gender_text == '未知') {
          temp.personal_gender = 0
        } else if (current_transfer_info.receive_list[i].personal_gender_text == '男') {
          temp.personal_gender = 1
        } else if (current_transfer_info.receive_list[i].personal_gender_text == '女') {
          temp.personal_gender = 2
        }
        temp.receive_type = 1
        receive_info.push(temp)
      } else if (current_transfer_info.receive_list[i].receive_type == 2) {
        var temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].business_license_path]
        temp.business_license_vc = current_transfer_info.receive_list[i].business_license_vc
        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = current_transfer_info.receive_list[i].company_identity_card_front_vc
        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = current_transfer_info.receive_list[i].company_identity_card_back_vc
        temp.company_name = current_transfer_info.receive_list[i].company_name
        temp.social_credit_code = current_transfer_info.receive_list[i].social_credit_code
        temp.company_personal_name = current_transfer_info.receive_list[i].company_personal_name
        temp.company_address = current_transfer_info.receive_list[i].company_address
        temp.company_phone = current_transfer_info.receive_list[i].company_phone
        temp.company_identity_code = current_transfer_info.receive_list[i].company_identity_code
        if (current_transfer_info.receive_list[i].company_gender_text == '未知') {
          temp.company_gender = 0
        } else if (current_transfer_info.receive_list[i].company_gender_text == '男') {
          temp.company_gender = 1
        } else if (current_transfer_info.receive_list[i].company_gender_text == '女') {
          temp.company_gender = 2
        }
        temp.company_birth_date = current_transfer_info.receive_list[i].company_birth_date
        temp.license_address = current_transfer_info.receive_list[i].license_address
        temp.receive_type = 2
        receive_info.push(temp)
      } else if (current_transfer_info.receive_list[i].receive_type == 3) {
        var temp = {}
        temp.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].business_license_path]
        temp.business_license_vc = current_transfer_info.receive_list[i].business_license_vc
        temp.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].company_identity_card_front_path]
        temp.company_identity_card_front_vc = current_transfer_info.receive_list[i].company_identity_card_front_vc
        temp.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.receive_list[i].company_identity_card_back_path]
        temp.company_identity_card_back_vc = current_transfer_info.receive_list[i].company_identity_card_back_vc
        temp.company_name = current_transfer_info.receive_list[i].company_name
        temp.social_credit_code = current_transfer_info.receive_list[i].social_credit_code
        temp.company_personal_name = current_transfer_info.receive_list[i].company_personal_name
        temp.company_address = current_transfer_info.receive_list[i].company_address
        temp.company_phone = current_transfer_info.receive_list[i].company_phone
        temp.company_identity_code = current_transfer_info.receive_list[i].company_identity_code
        if (current_transfer_info.receive_list[i].company_gender_text == '未知') {
          temp.company_gender = 0
        } else if (current_transfer_info.receive_list[i].company_gender_text == '男') {
          temp.company_gender = 1
        } else if (current_transfer_info.receive_list[i].company_gender_text == '女') {
          temp.company_gender = 2
        }
        temp.company_birth_date = current_transfer_info.receive_list[i].company_birth_date
        temp.license_address = current_transfer_info.receive_list[i].license_address
        temp.receive_type = 3
        receive_info.push(temp)
      }
    }

    var brand_temp = {}
    var brand_image_list_temp = []
    var brand_image_list_vc_temp = []
    for (var i = 0; i < current_transfer_info.brand_image_list.length; i++) {
      brand_image_list_temp.push('https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.brand_image_list[i].image_path)
      brand_image_list_vc_temp.push(current_transfer_info.brand_image_list[i].brand_vc)
    }
    brand_temp.brand_files = brand_image_list_temp
    brand_temp.brand_vc = brand_image_list_vc_temp

    var brand_other_image_list = []
    var brand_other_image_vc_list = []
    for (var i = 0; i < current_transfer_info.brand_other_image_list.length; i++) {
      brand_other_image_list.push('https://gongzheng.ma.cn/MYPublic/' + current_transfer_info.brand_other_image_list[i].image_path)
      brand_other_image_vc_list.push(current_transfer_info.brand_other_image_list[i].brand_other_vc)
    }

    brand_temp.brand_other_files = brand_other_image_list
    brand_temp.brand_other_vc = brand_other_image_vc_list

    brand_temp.brand_register_code = current_transfer_info.brand_register_code
    brand_temp.brand_name = current_transfer_info.brand_name
    brand_temp.possessor_name = current_transfer_info.possessor_name
    brand_temp.address = current_transfer_info.address
    brand_temp.type_num = current_transfer_info.type_num
    brand_temp.other_possessor = current_transfer_info.other_possessor
    brand_temp.validity = current_transfer_info.validity
    // brand_info.push(brand_temp);

    var other_temp = {}
    other_temp.page_count = current_transfer_info.page_count
    other_temp.mail_type = current_transfer_info.mail_type == '0' ?  false : true
    other_temp.send_type = current_transfer_info.send_type == '0' ? false : true
    other_temp.care_type = current_transfer_info.care_type == '0' ? false : true
    other_temp.comm_type = current_transfer_info.comm_type == '0' ? false : true
    other_temp.remark = current_transfer_info.remark


    console.log(transfer_info);
    console.log('====');
    console.log(receive_info);
    console.log('===');
    console.log(brand_temp);
    console.log('===');
    console.log(other_temp);

    // return false;
    
    util.setUserdata('transfer_info_apply_nos', "");
    util.setUserdata('transfer_info', transfer_info)
    util.setUserdata('receive_info', receive_info)
    util.setUserdata('brand_info', brand_temp)
    util.setUserdata('other', other_temp)
    util.setUserdata('order_id', current_transfer_info.order_id)
    util.setUserdata('order_number', current_transfer_info.order_num)
    // console.log(transfer_info);
    // return false;
    wx.navigateTo({
      // url: '../transfer_info_select/transfer_info'
      url: '../get_notarization/get_notarization?orderInfoId=' + current_transfer_info.order_id,
    })

  },
  bindOrderStatusChange: function(e) {
        console.log('picker account 发生选择改变，携带值为', e.detail.value);

        this.setData({
            orderStatusIndex: e.detail.value,
          nowStatus: e.detail.value
        })
        this.reload(e.detail.value)
  },
  reload: function(status){
    var that = this;
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    if (status == 2) status = '2,4'
    if (status == 1 || status == 5) status = '1,5'
    util.request('V2/orderList', {'order_status': status},
      function(res){
        console.log(res);
        if (res.data.code == 0) {
          // for (var i = 0; i < res.data.order_list.length; i++) {
          //   if (res.data.order_list[i].order_status == 2 && res.data.order_list[i].order_msg == '') {
          //     res.data.order_list[i].order_status = 5
          //     res.data.order_list[i].order_msg = '未能提交到公证处，请求改后重试'
          //   }
          // }
          that.setData({
            order_count: res.data.order_count,
            order_list: res.data.order_list
          })
        }
      },
      function(res){
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function(res) {
        wx.hideToast();
      })
  },
  onLoad: function (opt){
    var that = this
    that.setData({
      nowStatus: opt.status
    })
    this.reload(opt.status)

    
    util.setUserdata('status', opt.status);
  },
  onShareAppMessage: function(res) {
    var that = this
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
      var order_id = res.target.dataset.id;
      var updateStatus = res.target.dataset.update
      console.log("订单order_id==" + order_id);
    }
    return {
      title: '请进行实名认证',
      path: 'pages/share_certification/share_certification?id=' + order_id + '&update=' + updateStatus,
      imageUrl: '../../images/shareimg.png',
      //分享成功后执行
      success: function (res) {
        console.log("--------------转发成功--------------------") 
      },
      fail: function (res) {
        console.log("--------------转发失败--------------------")
      }

    }
  }

})