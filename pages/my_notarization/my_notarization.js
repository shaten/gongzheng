const util = require('../../utils/util.js')

var app = getApp();
Page({
  data:{
    order_count_1: 0,
    order_count_2:0,
    order_count_3: 0,
    transfer_count: 0,
    receive_count: 0,
    userInfo: null,
    is_vip: null
  },
  toRecharge: function() {
    wx.navigateTo({
      url: '../pay_recharge/pay_recharge',
    })
  },
  toLogout: function() {
    let that = this
    util.removeUserData('identity_token')
    util.removeUserData('wx_username')
    wx.showModal({
      title: '退出登录',
      content: '确定要退出当前帐号？',
      showCancel: true,//是否显示取消按钮
      success: function (res) {
        console.log(res.confirm)
        if (res.confirm) {
          util.removeUserData('identity_token')
          wx.clearStorage()
          wx.login({
            success: res => {
              util.request('V2/getSign', { code: res.code },
                function (res) {
                  var session_key = util.getUserdata('session_key')
                  session_key = res.data.session_key
                  util.setUserdata('session_key', session_key)
                  var identity_token = util.getUserdata('identity_token')
                  console.log('切换帐号新token,getSin的旧token:' + identity_token)
                  console.log('切换帐号新token,getSin的旧token:' + identity_token.length)
                  console.log(typeof (identity_token))
                  console.log('getSin的最新token:' + res.data.identity_token)
                  // identity_token = res.data.identity_token
                  if (typeof (identity_token) == 'object') {
                    console.log('进来')
                    identity_token = res.data.identity_token
                  }
                  util.setUserdata('identity_token', identity_token)
                  console.log('logut')
                  that.onLoad()

                }
              )
            }
          })

          

        }
      },
      fail: function (res) { },//接口调用失败的回调函数
      complete: function (res) { },//接口调用结束的回调函数（调用成功、失败都会执行）
    })
    // 
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  bindBackTap: function () {
    wx.reLaunch({
      url: '../index/index',
    })
  },
  bindOrderTap: function (e) {
    wx.navigateTo({
      url: '../my_order/my_order?status='+e.currentTarget.dataset.status,
    })
  },
  weixinLogin: function(e) {
    console.log(e.currentTarget.dataset)
    if (e.currentTarget.dataset.band == '' && e.currentTarget.dataset.type == 1) {
      wx.navigateTo({
        url: '../userlogin/userlogin?type=1',
      })
    } else if (e.currentTarget.dataset.type == 2) {
      wx.navigateTo({
        url: '../userlogin/userlogin?type=2',
      })
    }
    
  },
  bindCertPersonalTap: function () {
    wx.navigateTo({
      url: '../certified_personal/certified_personal',
    })
  },
  bindCertCompanyTap: function () {
    wx.navigateTo({
      url: '../certified_company/certified_company',
    })
  },
  tradingtap:function(){
    wx.navigateTo({
      url: '../trading/trading',
    })
  },
  emailcompanytap:function(){
    wx.navigateTo({
      url: '../useremail/useremail',
    })
  },
  phonecompanytap: function () {
    var member = util.getUserdata('member')
    if (member.phone)
    {
      wx.navigateTo({
        url: '../userphonehas/userphonehas',
      })
    }else{
     
      wx.navigateTo({
        url: '../userphone/userphone',
      })
    }
  
  },
  usercall:function()
  {
    wx.makePhoneCall({
      phoneNumber: '13502418777',
    })
  },
  useraddrTap:function()
  {
   
    wx.navigateTo({
      url: '../useraddr/useraddr',
    })
  },
  usermoneyTap:function()
  {
    wx.navigateTo({
      url: '../usermoney/usermoney',
    })
  },
  onGotUserInfo: function(e){
    console.log("nickname=" + e.detail.userInfo.nickName);
  },
  onLoad: function () {
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    var wx_username = util.getUserArr('wx_username')
    // if (wx_username) {
    //   util.setUserdata('identity_token', res.data.data.identity_token)
    // }
    // util.setUserdata('identity_token', res.data.data.identity_token)
    var that = this;
    
    util.request(
      'V2/myAllInfo',
      {'type': 1},
      function(res){
        if (res.data.code == 0){
          console.log(res);
          wx.hideToast()
          util.setUserdata('member', res.data.member)
          var address="未设置";
          if (res.data.member.address)
          {
            address = "已设置";
          }
          that.setData({
            order_count_1: res.data.order_count_1,
            order_count_2: res.data.order_count_2,
            order_count_3: res.data.order_count_3,
            personal_count: res.data.personal_count,
            company_count: res.data.company_count,
            member: res.data.member,
            address: address,
            momey: res.data.member.money,
            is_vip: res.data.member.is_vip,
            wx_username: wx_username
          })
        } else if (res.data.code == '-10000') {
          // 登录
          wx.login({
            success: res => {
              util.request('V2/getSign', { code: res.code },
                function (res) {
                  var session_key = util.getUserdata('session_key')
                  session_key = res.data.session_key
                  util.setUserdata('session_key', session_key)
                  var identity_token = util.getUserdata('identity_token')
                  console.log('切换帐号新token,getSin的旧token:' + identity_token)
                  console.log('切换帐号新token,getSin的旧token:' + identity_token.length)
                  console.log(typeof (identity_token))
                  console.log('getSin的最新token:' + res.data.identity_token)
                  // identity_token = res.data.identity_token
                  if (typeof (identity_token) == 'object') {
                    console.log('进来')
                    identity_token = res.data.identity_token
                  }
                  util.setUserdata('identity_token', identity_token)

                  wx.getSystemInfo({
                    success(res) {
                      var rtn = res
                      console.log(rtn)
                      wx.request({
                        url: util.reqUrl('V2/getSystemInfo'),
                        header: util.headers(),
                        method: 'post',
                        data: { 'identity_token': util.getUserdata('identity_token'), 'res': JSON.stringify(rtn) },
                        success(res) {
                          console.log('用户信息')
                          console.log(res)
                          if (res.data.code == '-10001') {
                          }
                        }
                      })
                    }
                  })

                  if (that.userInfoReadyCallback) {
                    that.userInfoReadyCallback(res)
                  }
                }
              )
            }
          })
        }
      },
      function(res){
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function(res){
        // wx.hideToast();
      })


    
  },
  onShow:function(){
    // var that = this;
    // util.request('V2/myAllInfo', { 'type': 1 },
    //   function (res) {
    //     if (res.data.code == 0) {
    //       console.log(res);
    //       util.setUserdata('member', res.data.member)
    //       that.setData({
    //         order_count_1: res.data.order_count_1,
    //         order_count_2: res.data.order_count_2,
    //         order_count_3: res.data.order_count_3,
    //         personal_count: res.data.personal_count,
    //         company_count: res.data.company_count,
    //         member: res.data.member
    //       })
    //     }
    //   })
    this.onLoad()
  }

})