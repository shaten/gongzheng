Page({
  data: {
    tempFilePaths: '',
    hidden: true,
    buthidden: false,
    sourceType: ['album', 'camera'],
  },
  onLoad: function (options) {
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
    } catch (e) { }
  },
  frontimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          FilePaths: res.tempFilePaths
        })
      }
    })
  },
  reciteimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          recitePaths: res.tempFilePaths
        })
      }
    })
  },
  sure: function () {
    wx.navigateTo({ url: '../assignee_info/personal/personal' })
  },
  remake: function () {
    wx.navigateTo({ url: '../upload/upload' })
  }
})