const util = require('../../utils/util.js')
Page({
  data: {
    showDialog: false,
    hitone:false,
    hittow: false,
    hitthree:false,
    comfirmRead: false,
    orderInfoId: 0,
    order_status: false,
    openId: null
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  operatingInstructions: function(event) {
    var $this = this;
    console.log(event.currentTarget.dataset.read);
    var readStatus = event.currentTarget.dataset.read
    this.setData({
      comfirmRead:!readStatus
    })
  },
  hitone:function(){
    console.log('hitone==' + this.data.hitone)
    this.setData({
      hitone: true,
    });
  },
  hittow: function () {
    console.log('hittow==' + this.data.hittow)
    this.setData({
      hittow: true,
    });
  },
  hitthree: function () {
    console.log('hitthree==' + this.data.hitthree)
    this.setData({
      hitthree: true,
    });
  },
  //说明
  explain:function(){

  

    if (this.data.showDialog)
    {
      // if (this.data.hitone !== true || this.data.hittow !== true || this.data.hitthree !== true) {
      if (this.data.comfirmRead !== true) {
        wx.showModal({
          content: "请先阅读完必读项",
          showCancel: false,
          success: function (res) {
            if (res.confirm) {
            }
          }
        })

        return;
      }

      var that = this;
      util.request('V2/updatemyinfo', { 'unionid': 1},
        function (res) {
          console.log(res);
          if (res.data.code == 0) {
             console.log(res);
           
          }  
        })
    }

    this.setData({
      showDialog: !this.data.showDialog
    });
    
  },
  bindBackTap: function () {
    wx.reLaunch({
      url: '../index/index',
    })
  },
  bindOtherTap: function (){
    wx.navigateTo({
      url: '../get_notarization_other/get_notarization_other',
    })
    
  },
  transfer_info: function () {
    if (this.data.orderInfoId != 0) {
      wx.navigateTo({
        url: '../transfer_info/transfer_info?orderInfoId=' + this.data.orderInfoId,
      })
    } else {
      wx.navigateTo({
        url: '../transfer_info/transfer_info',
      })
    }
    // wx.navigateTo({
    //   url: '../transfer_info/transfer_info',
    // })
  },
  receive_info: function () {
    wx.navigateTo({
      url: '../receive_info/receive_info',
    })
  },
  brand_info: function () {
    wx.navigateTo({
      url: '../brand_info/brand_info',
    })
  },
  delete_order: function(){
    var that = this
    var order_number = util.getUserdata('order_number')
    wx.showModal({
      title: '确认删除',
      content: '删除后作为新的订单提交，需要重新支付',
      // showCancel: true,//是否显示取消按钮
      // cancelText: "清空信息",//默认是“取消”
      // cancelColor: 'skyblue',//取消文字的颜色
      // confirmText: "继续修改",//默认是“确定”
      // confirmColor: 'skyblue',//确定文字的颜色
      success: function(res) {
        if(res.confirm) {
          util.setUserdata('order_id', '')
          util.setUserdata('order_number', '')
          that.setData({
            order_status: false
          })
        }
      }
    })
  },
  bindCommitTap: function (e) {
    var that = this
    var formId = e.detail.formId
    var openId = that.data.openId;
    var order_id = util.getUserArr('order_id')

    var other = util.getUserdata('other')
    console.log(other);
    var brand_info = util.getUserdata('brand_info')
    console.log(brand_info)
    var transfer_info = util.getUserdata('transfer_info')
    console.log(transfer_info)
    var receive_info = util.getUserdata('receive_info')
    console.log(receive_info)

    var msg = this.input_check()
    console.log(msg)
    if(msg != ''){
      wx.showModal({
          content: msg,
          showCancel: false,
          success: function (res) {
              if (res.confirm) {
              }
          }
      })
      return
    }
    if (order_id>0) {
      //修改驳回提交
      wx.showLoading({
        title: '订单提交中',
        icon: 'loading',
        mask: true
      })
      wx.showModal({
        title: '提示',
        content: '是否确定提交',
        success: function(sm) {
          if(sm.confirm) {
            util.request('V2/commitOrder',
              {
                'order_id': order_id,
                'other': JSON.stringify(other),
                'brand_info': JSON.stringify(brand_info),
                'transfer_info': JSON.stringify(transfer_info),
                'receive_info': JSON.stringify(receive_info),
                'wx_tmpl_formid': formId,
                'wx_openid': openId
              },
              function (res) {
                wx.hideLoading()
                console.log(res.data);
                if (res.data.code != 0) {
                  wx.showToast({
                    title: res.data.msg,
                    icon: 'none',
                    duration: 2000
                  })
                  return false
                }
                if(res.data.code == '-101') {
                  util.removeUserData('order_id')
                  wx.navigateTo({
                    url: '../order_commit_info/order_commit_info',
                  })
                }
                //回调成功清除提交信息
                if (res.data.order_id > 0) {
                  util.setUserdata('transfer_info_apply_nos', "");
                  util.clear_all_input()
                  util.removeUserData('order_id')
                  util.removeUserData('order_number')
                  wx.navigateTo({
                    url: '../my_order/my_order?status=2,4',
                  })

                }
                //util.setUserdata('transfer_info_apply_nos', "");
                //util.clear_all_input()
              }
            )
          }
        }
      })
      
    }else{
      wx.navigateTo({
        url: '../order_commit_info/order_commit_info',
      })
    }
  },
  unique_transfer_info: function () {
    console.log('转让人去重')
    var one = 1
    var transfer_info = util.getUserArr('transfer_info');
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 1) {
        var transfer_personal_flag = 0
        if (!transfer_info[i].personal_name || transfer_info[i].personal_name == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_card_front_vc || transfer_info[i].identity_card_front_vc == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_card_back_vc || transfer_info[i].identity_card_back_vc == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_code || transfer_info[i].identity_code == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].personal_gender || (transfer_info[i].personal_gender != 1 && transfer_info[i].personal_gender != 2)) {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].personal_birth_date || transfer_info[i].personal_birth_date == '') {
          transfer_personal_flag += one
        }
        if (!util.checkDate(transfer_info[i].personal_birth_date)) {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          transfer_personal_flag += one
        }
        console.log(transfer_personal_flag)
        if (transfer_personal_flag == 9) {
          transfer_info.splice(i, 1)
        }

      } else if (transfer_info[i].transfer_type == 2) {
        var transfer_company_flag = 0
        if (!transfer_info[i].company_name || transfer_info[i].company_name == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].business_license_vc || transfer_info[i].business_license_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].social_credit_code || transfer_info[i].social_credit_code == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_personal_name || transfer_info[i].company_personal_name == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_card_front_vc || transfer_info[i].company_identity_card_front_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_card_back_vc || transfer_info[i].company_identity_card_back_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_gender || (transfer_info[i].company_gender != 1 && transfer_info[i].company_gender != 2)) {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_birth_date || transfer_info[i].company_birth_date == '') {
          transfer_company_flag += one
        }
        if (!util.checkDate(transfer_info[i].company_birth_date)) {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_code || transfer_info[i].company_identity_code == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          transfer_company_flag += one
        }

        console.log(transfer_company_flag)
        if (transfer_company_flag == 12) {
          transfer_info.splice(i, 1)
        }
      }
    }

    util.setUserdata('transfer_info', transfer_info)
  },
  unique_receive_info: function () {
    var receive_info = util.getUserArr('receive_info')
    console.log('受让人去重')
    var one = 1
    console.log(receive_info)
    for (var i = 0; i < receive_info.length; i++) {
      if (receive_info[i].receive_type == 1) {
        var receive_personal_flag = 0
        if (!receive_info[i].personal_name || receive_info[i].personal_name == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_card_front_vc || receive_info[i].identity_card_front_vc == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_card_back_vc || receive_info[i].identity_card_back_vc == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].identity_code || receive_info[i].identity_code == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_gender || (receive_info[i].personal_gender != 1 && receive_info[i].personal_gender != 2)) {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_birth_date || receive_info[i].personal_birth_date == '') {
          receive_personal_flag += one
        }
        if (!util.checkDate(receive_info[i].personal_birth_date)) {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_phone || receive_info[i].personal_phone == '') {
          receive_personal_flag += one
        }
        if (!receive_info[i].personal_address || receive_info[i].personal_address == '') {
          receive_personal_flag += one
        }

        console.log(receive_personal_flag)
        if (receive_personal_flag == 9) {
          receive_info.splice(i, 1)
        }

      } else if (receive_info[i].receive_type == 2) {
        var receive_company_flag = 0
        if (!receive_info[i].company_name || receive_info[i].company_name == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].business_license_vc || receive_info[i].business_license_vc == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].social_credit_code || receive_info[i].social_credit_code == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_personal_name || receive_info[i].company_personal_name == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_phone || receive_info[i].company_phone == '') {
          receive_company_flag += one
        }
        if (!receive_info[i].company_address || receive_info[i].company_address == '') {
          receive_company_flag += one
        }

        console.log(receive_company_flag)
        if (receive_company_flag == 6) {
          receive_info.splice(i, 1)
        }
      }
    }

    util.setUserdata('receive_info', receive_info)
  },
  input_check: function(){
    var msg = ''
    var transfer_info = util.getUserArr('transfer_info')
    var receive_info = util.getUserArr('receive_info')
    var brand_info = util.getUserdata('brand_info')
    console.log(receive_info)
    var i
    if(transfer_info.length <= 0){
      msg += '请填写转让人信息';
      return msg;
    }

    var member = util.getUserdata('member')
    if (!member.address || member.address == '' || !member.uphone || member.uphone == '' || !member.nickname || member.nickname == '') {
      msg += '请在(我的公证->快递地址)填写完整的邮寄地址';
      return msg;
    }
    
    for(i=0;i<transfer_info.length;i++){
      if(transfer_info[i].ot) continue;
      if(transfer_info[i].transfer_type == 1){


        /*
        console.log('检查是否包含该名字');
        if (brand_info.possessor_name.indexOf(transfer_info[0].company_name) != -1) {
          console.log('包含该名字');

        } else {
          console.log('不包含该名字');
        }

        return;
        */

        if(!transfer_info[i].personal_name || transfer_info[i].personal_name == '' || transfer_info[i].personal_name.length > 30 ){
          msg += '转让人姓名不合法';
          return msg;
        }
        if (!transfer_info[i].identity_card_front_vc || transfer_info[i].identity_card_front_vc == ''){
          msg += '请上传转让人身份证正面照片';
          return msg;
        }
        if (!transfer_info[i].identity_card_back_vc || transfer_info[i].identity_card_back_vc == ''){
          msg += '请上传转让人身份证反面照片';
          return msg;
        }
        if(!transfer_info[i].identity_code || transfer_info[i].identity_code == ''){
          msg += '请填写转让人身份证号码';
          return msg;
        }
        if(!transfer_info[i].personal_gender || (transfer_info[i].personal_gender != 1 && transfer_info[i].personal_gender != 2) ){
          msg += '请填写转让人性别';
          return msg;
        }
        if(!transfer_info[i].personal_birth_date || transfer_info[i].personal_birth_date == ''){
          msg += '请填写转让人出生日期';
          return msg;
        }
        if(!util.checkDate(transfer_info[i].personal_birth_date)){
          msg += '转让人出生日期格式不正确';
          return msg;
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          msg += '转让人请先通过身份认证';
          return msg;
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          msg += '转让人请先填写法人授权签名';
          return msg;
        }
        
      }else if(transfer_info[i].transfer_type == 2){
        if(!transfer_info[i].company_name || transfer_info[i].company_name == ''){
          msg += '请填写公司名称';
          return msg;
        }
        if (!transfer_info[i].business_license_vc || transfer_info[i].business_license_vc == ''){
          msg += '请上传营业执照';
          return msg;
        }
        if(!transfer_info[i].social_credit_code || transfer_info[i].social_credit_code == ''){
          msg += '请填写社会信用代码';
          return msg;
        }
        if(!transfer_info[i].company_personal_name || transfer_info[i].company_personal_name == ''){
          msg += '请填写法定代表人';
          return msg;
        }
        if (!transfer_info[i].company_identity_card_front_vc || transfer_info[i].company_identity_card_front_vc == ''){
          msg += '请上传法人身份证正面照片';
          return msg;
        }
        if (!transfer_info[i].company_identity_card_back_vc || transfer_info[i].company_identity_card_back_vc == ''){
          msg += '请上传法人身份证反面照片';
          return msg;
        }
        if(!transfer_info[i].company_gender || (transfer_info[i].company_gender != 1 && transfer_info[i].company_gender != 2) ){
          msg += '请填写法人性别';
          return msg;
        }
        if(!transfer_info[i].company_birth_date || transfer_info[i].company_birth_date == ''){
          msg += '请填写法人出生日期';
          return msg;
        }
        if(!util.checkDate(transfer_info[i].company_birth_date)){
          msg += '法人出生日期格式不正确';
          return msg;
        }
        if(!transfer_info[i].company_identity_code || transfer_info[i].company_identity_code == ''){
          msg += '请填写法人身份证号码';
          return msg;
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          msg += '转让人请先通过身份认证';
          return msg;
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          msg += '转让人请先填写法人授权签名';
          return msg;
        }
      } else if (transfer_info[i].transfer_type == 3) {
        if (!transfer_info[i].company_name || transfer_info[i].company_name == '') {
          msg += '请填写境外公司名称';
          return msg;
        }
        if (!transfer_info[i].business_license_vc || transfer_info[i].business_license_vc == '') {
          msg += '请上传营业执照';
          return msg;
        }
        if (!transfer_info[i].social_credit_code || transfer_info[i].social_credit_code == '') {
          msg += '请填写社会信用代码';
          return msg;
        }
        if (!transfer_info[i].company_personal_name || transfer_info[i].company_personal_name == '') {
          msg += '请填写法定代表人';
          return msg;
        }
        if (!transfer_info[i].company_identity_card_front_vc || transfer_info[i].company_identity_card_front_vc == '') {
          msg += '请上传法人身份证正面照片';
          return msg;
        }
        if (!transfer_info[i].company_identity_card_back_vc || transfer_info[i].company_identity_card_back_vc == '') {
          msg += '请上传法人身份证反面照片';
          return msg;
        }
        
        console.log(transfer_info[i].overseas_other_files.length)
        console.log(transfer_info[i].overseas_other_files.length < 1)
        if (transfer_info[i].overseas_other_files.length < 1) {
          msg += '请上传香港公司律师证明';
          return msg;
        }
        if (!transfer_info[i].company_gender || (transfer_info[i].company_gender != 1 && transfer_info[i].company_gender != 2)) {
          msg += '请填写法人性别';
          return msg;
        }
        if (!transfer_info[i].company_birth_date || transfer_info[i].company_birth_date == '') {
          msg += '请填写法人出生日期';
          return msg;
        }
        if (!util.checkDate(transfer_info[i].company_birth_date)) {
          msg += '法人出生日期格式不正确';
          return msg;
        }
        if (!transfer_info[i].company_identity_code || transfer_info[i].company_identity_code == '') {
          msg += '请填写法人身份证号码';
          return msg;
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          msg += '转让人请先通过身份认证';
          return msg;
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          msg += '转让人请先填写法人授权签名';
          return msg;
        }
      }

    }

    if(!brand_info.brand_register_code || brand_info.brand_register_code == ''){
      msg += '请填写商标注册号';
      return msg;
    }
    if(!brand_info.brand_name || brand_info.brand_name == ''){
      msg += '请填写商标名称';
      return msg;
    }
    if(!brand_info.possessor_name || brand_info.possessor_name == ''){
      msg += '请填写商标持有人';
      return msg;
    }
    if(!brand_info.validity || brand_info.validity == ''){
      msg += '请填写商标有效期';
      return msg;
    }
    // if(!util.checkDate(brand_info.validity)){
    //   msg += '商标有效期格式不正确';
    //   return msg;
    // }
    if (!brand_info.brand_vc || brand_info.brand_vc.length <= 0){
      msg += '请上传商标注册证';
      return msg;
    }

    if (transfer_info[0].transfer_type == 1) { //判断主体转让人是否与商标持有人一致

      if (brand_info.possessor_name.indexOf(transfer_info[0].personal_name) != -1 )
      {
        console.log('包含该名字');

      } else {

        if (brand_info.possessor_name != transfer_info[0].personal_name) {
          msg += '转让人名称和商标注册证的持有人名称不一致';
          return msg;
        }

      }

     

    } else if (transfer_info[0].transfer_type == 2){


      if (brand_info.possessor_name.indexOf(transfer_info[0].company_name) != -1) {
        console.log('包含该名字');

      } else {

        if (brand_info.possessor_name != transfer_info[0].company_name) {
          msg += '转让公司名称和商标注册证的持有人名称不一致';
          return msg;
        }

      }
     
    } else if (transfer_info[0].transfer_type == 3) {
      if (brand_info.possessor_name.indexOf(transfer_info[0].company_name) != -1) {
        console.log('包含该名字');

      } else {

        if (brand_info.possessor_name != transfer_info[0].company_name) {
          msg += '转让公司名称和商标注册证的持有人名称不一致';
          return msg;
        }

      }
    }

    if(receive_info.length <= 0){
      msg += '请填写受让人信息';
      return msg;
    }
    for(i=0;i<receive_info.length;i++){
      if(receive_info[i].ot) continue;
      if(receive_info[i].receive_type == 1){
         
        if(!receive_info[i].personal_name || receive_info[i].personal_name == '' || receive_info[i].personal_name.length > 30 ){
          msg += '受让人姓名不合法';
          return msg;
        }
        if (!receive_info[i].identity_card_front_vc || receive_info[i].identity_card_front_vc == ''){
          msg += '请上传受让人身份证正面照片';
          return msg;
        }
        if (!receive_info[i].identity_card_back_vc || receive_info[i].identity_card_back_vc == ''){
          msg += '请上传受让人身份证反面照片';
          return msg;
        }
        if(!receive_info[i].identity_code || receive_info[i].identity_code == ''){
          msg += '请填写受让人身份证号码';
          return msg;
        }
        if(!receive_info[i].personal_gender || (receive_info[i].personal_gender != 1 && receive_info[i].personal_gender != 2) ){
          msg += '请填写受让人性别';
          return msg;
        }
        if(!receive_info[i].personal_birth_date || receive_info[i].personal_birth_date == ''){
          msg += '请填写受让人出生日期';
          return msg;
        }
        if(!util.checkDate(receive_info[i].personal_birth_date)){
          msg += '受让人出生日期格式不正确';
          return msg;
        }


        if (!receive_info[i].personal_phone || receive_info[i].personal_phone == '') {
          msg += '请填写受让人联系电话';
          return msg;
        }

        if (!receive_info[i].personal_address || receive_info[i].personal_address == '') {
          msg += '请填写受让人联系地址';
          return msg;
        }
      
        
      }else if(receive_info[i].receive_type == 2){
        if(!receive_info[i].company_name || receive_info[i].company_name == ''){
         msg += '请填写受让公司名称';
         return msg;
        }
        if (!receive_info[i].business_license_vc || receive_info[i].business_license_vc == ''){
          msg += '请上传受让营业执照';
          return msg;
        }
        if(!receive_info[i].social_credit_code || receive_info[i].social_credit_code == ''){
         msg += '请填写受让社会信用代码';
         return msg;
        }
        if(!receive_info[i].company_personal_name || receive_info[i].company_personal_name == ''){
         msg += '请填写受让法定代表人';
         return msg;
        }
        // if (!receive_info[i].company_identity_card_front_vc || receive_info[i].company_identity_card_front_vc == ''){
        //  msg += '请上传法受让人身份证正面照片';
        //  return msg;
        // }
        // if (!receive_info[i].company_identity_card_back_vc || receive_info[i].company_identity_card_back_vc == ''){
        //  msg += '请上传法受让人身份证反面照片';
        //  return msg;
        // }
        // if(!receive_info[i].company_gender || (receive_info[i].company_gender != 1 && receive_info[i].company_gender != 2) ){
        //  msg += '请填写受让法人性别';
        // return msg;
        // }
        // if(!receive_info[i].company_birth_date || receive_info[i].company_birth_date == ''){
        //  msg += '请填写受让法人出生日期';
        //  return msg;
        // }
        // if(!util.checkDate(receive_info[i].company_birth_date)){
        //  msg += '受让法人出生日期格式不正确';
        //  return msg;
        // }
        // if(!receive_info[i].company_identity_code || receive_info[i].company_identity_code == ''){
        //  msg += '请填写受让法人身份证号码';
        //  return msg;
        // }


        if (!receive_info[i].company_phone || receive_info[i].company_phone == '') {
         msg += '请填写受让人联系电话';
         return msg;
        }

        if (!receive_info[i].company_address || receive_info[i].company_address == '') {
         msg += '请填写受让人联系地址';
         return msg;
        }

       

      } else if (receive_info[i].receive_type == 3){
        if (!receive_info[i].company_name || receive_info[i].company_name == '') {
          msg += '请填写受让公司名称';
          return msg;
        }
        if (!receive_info[i].business_license_vc || receive_info[i].business_license_vc == '') {
          msg += '请上传受让营业执照';
          return msg;
        }
        if (!receive_info[i].social_credit_code || receive_info[i].social_credit_code == '') {
          msg += '请填写受让社会信用代码';
          return msg;
        }
        if (!receive_info[i].company_personal_name || receive_info[i].company_personal_name == '') {
          msg += '请填写受让法定代表人';
          return msg;
        }
        if (!receive_info[i].company_phone || receive_info[i].company_phone == '') {
          msg += '请填写受让人联系电话';
          return msg;
        }

        if (!receive_info[i].company_address || receive_info[i].company_address == '') {
          msg += '请填写受让人联系地址';
          return msg;
        }
      }

    }
    return msg
  },
  reload: function (){
    var other = util.getUserdata('other')
    var brand_info = util.getUserdata('brand_info')
    var transfer_info = util.getUserArr('transfer_info')
    var receive_info = util.getUserArr('receive_info')
    
    var transfer_remark = ''
    var brand_remark = ''
    var receive_remark = ''
    var other_remark = ''

    for (var i = 0; i < transfer_info.length; i++) {
      console.log(transfer_info[i]);

      if (transfer_info.length > 0 && transfer_info[i]) {
        var transfer = transfer_info[i];
        var transfer_remarktmp="";
        if ((transfer.personal_name && transfer.personal_name != '') || (transfer.company_name && transfer.company_name != '')) {
          transfer_remarktmp += "转让人：" + (transfer.personal_name || transfer.company_name || '') + '； '
        }

        if (transfer.transfer_type == 1) {
          if (transfer.identity_code && transfer.identity_code != '')
            transfer_remarktmp += "身份证号：" + (transfer.identity_code || '') + '； ';
          if (transfer.identity_address && transfer.identity_address != '')
            transfer_remarktmp += "地址：" + (transfer.identity_address || '') + '； ';
        }
        if (transfer.transfer_type == 2) {
          if (transfer.social_credit_code && transfer.social_credit_code != '')
          transfer_remarktmp += "社会信用代码：" + (transfer.social_credit_code || '') + '； ';
          if (transfer.license_address && transfer.license_address != '')
          transfer_remarktmp += "地址：" + (transfer.license_address || '') + '； ';
        }
        if (transfer.transfer_type == 3) {
          if (transfer.social_credit_code && transfer.social_credit_code != '')
            transfer_remarktmp += "香港注册编号：" + (transfer.social_credit_code || '') + '； ';
          if (transfer.license_address && transfer.license_address != '')
            transfer_remarktmp += "地址：" + (transfer.license_address || '') + '； ';
        }
      }
      if (transfer_remarktmp){
        transfer_remark = transfer_remark+ transfer_remarktmp +"\n";
      }
     
    }

   /* if(transfer_info.length > 0 && transfer_info[0]){
      var transfer = transfer_info[0];
      if( (transfer.personal_name && transfer.personal_name !='') || (transfer.company_name && transfer.company_name !='') ){
        transfer_remark += "转让人：" + (transfer.personal_name||transfer.company_name||'') + '； '
      }
      
      if(transfer.transfer_type == 1){
        if(transfer.identity_code && transfer.identity_code !='')
        transfer_remark += "身份证号：" + (transfer.identity_code||'') + '； ';
        if(transfer.identity_address && transfer.identity_address !='')
        transfer_remark += "地址：" + (transfer.identity_address||'') + '； ';
      }
      if(transfer.transfer_type == 2){
        if(transfer.social_credit_code && transfer.social_credit_code !='')
        transfer_remark += "社会信用代码：" + (transfer.social_credit_code||'') + '； ';
        if(transfer.license_address && transfer.license_address !='')
        transfer_remark += "地址：" + (transfer.license_address||'') + '； ';
      }
    } */

    for (var j = 0; j < receive_info.length; j++) {
    
      var receive_remarktmp="";
      if (receive_info.length > 0 && receive_info[j]) {
        var receive = receive_info[j];
        if ((receive.personal_name && receive.personal_name != '') || (receive.company_name && receive.company_name != '')) {
          receive_remarktmp += "受让人：" + (receive.personal_name || receive.company_name || '') + '； ';
        }
        if (receive.receive_type == 1) {
          if (receive.identity_code && receive.identity_code != '')
            receive_remarktmp += "身份证号：" + (receive.identity_code || '') + '； ';
          if (receive.identity_address && receive.identity_address != '')
            receive_remarktmp += "地址：" + (receive.identity_address || '') + '； ';
        }
        if (receive.receive_type == 2) {
          if (receive.social_credit_code && receive.social_credit_code != '')
            receive_remarktmp += "社会信用代码：" + (receive.social_credit_code || '') + '； ';
          if (receive.license_address && receive.license_address != '')
            receive_remarktmp += "地址：" + (receive.license_address || '') + '； ';
        }
        if (receive.receive_type == 3) {
          if (receive.social_credit_code && receive.social_credit_code != '')
            receive_remarktmp += "公司注册编码：" + (receive.social_credit_code || '') + '； ';
          if (receive.license_address && receive.license_address != '')
            receive_remarktmp += "地址：" + (receive.license_address || '') + '； ';
        }
      }
      if (receive_remarktmp){
        receive_remark = receive_remark + receive_remarktmp+'\n';
      }
     

    }

  /*  if(receive_info.length > 0 && receive_info[0]){
      var receive = receive_info[0];
      if( (receive.personal_name && receive.personal_name !='') || (receive.company_name && receive.company_name !='') ){
        receive_remark += "转让人：" + (receive.personal_name||receive.company_name||'') + '； '
      }
      if(receive.receive_type == 1){
        if(receive.identity_code && receive.identity_code !='')
        receive_remark += "身份证号：" + (receive.identity_code||'') + '； ';
        if(receive.identity_address && receive.identity_address !='')
        receive_remark += "地址：" + (receive.identity_address||'') + '； ';
      }
      if(receive.receive_type == 2){
        if(receive.social_credit_code && receive.social_credit_code !='')
        receive_remark += "社会信用代码：" + (receive.social_credit_code||'') + '； ';
        if(receive.license_address && receive.license_address !='')
        receive_remark += "地址：" + (receive.license_address||'') + '； ';
      }
    }*/

    if (other.page_count>0)
    {
      other_remark += '份数：' + (other.page_count || 0) + '份； '
    }
    
    if(other.mail_type) other_remark += '当天出公证并邮寄； ';
    if(other.send_type) other_remark += '发送公证书电子版； ';
    if(other.care_type) other_remark += '公证书原件代保管； ';

    if(brand_info.brand_register_code) brand_remark += '商标注册号：' + (brand_info.brand_register_code||'') + '； ';
    if(brand_info.possessor_name) brand_remark += '持有人：' + (brand_info.possessor_name||'') + '； ';
    if(brand_info.validity) brand_remark += '商标有效期：' + (brand_info.validity||'') + '； ';

    if (transfer_remark=='')
    {
      transfer_remark = '暂无资料'
    }
     
    if (brand_remark == '') {
      brand_remark = '暂无资料'
    }

    if (receive_remark == '') {
      receive_remark = '暂无资料'
    }
    if (other_remark == '') {
      other_remark = '暂无资料'
    }
     
    this.setData({
      transfer_remark:transfer_remark,
      brand_remark:brand_remark,
      receive_remark:receive_remark,
      other_remark:other_remark
    })
  },
  onShow: function () {
    var order_number = util.getUserdata('order_number')
    if (order_number > 0) {
      this.setData({
        order_status: true,
        order_number: order_number
      })
    } else {
      this.setData({
        order_status: false,
        order_number: ''
      })
    }
    var that = this;
    util.request('V2/myAllInfo', { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          util.setUserdata('member', res.data.member)
          
          var ishas=false;

          if (res.data.member.unionid!=='1')
          {
            ishas = true;
          }

          that.setData({
            showDialog:ishas,
            order_count_1: res.data.order_count_1,
            order_count_2: res.data.order_count_2,
            order_count_3: res.data.order_count_3,
            personal_count: res.data.personal_count,
            company_count: res.data.company_count,
            member: res.data.member,
            openId: res.data.member.openid
          })


        }
      })

    this.reload()
  },
  onLoad: function (options){
    var order_number = util.getUserdata('order_number')
    if (order_number > 0){
      this.setData({
        order_status: true,
        order_number: order_number
      })
    }else{
      this.setData({
        order_status: false,
        order_number: ''
      })
    }

    this.unique_transfer_info()
    this.unique_receive_info()
    this.reload()
  }

})