// pages/transfer_info_select/transfer_info_select.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据1
   */
  data: {
    checked_arr: [],
    checked_str: ''
  },
  deleteHis: function(e) {
    let that = this
    console.log(that.data)
    var opt = {
      identity_type: that.data.identity_type,
      type: that.data.type,
    }
    

    wx.showModal({
      title: '删除',
      content: '是否确定删除',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          console.log(that.data.checked_str)
          util.request('V2/storageDel',
          {
            'id': that.data.checked_str,
          },
          function (res) {
            console.log(res.data.code)

            if (res.data.code == 1) {
              wx.showToast({
                title: '删除成功',
                icon: 'none',
                duration: 2000
              })
              that.onLoad(opt)

            }
          })
        }
      }
    })
    
  },
  checkboxChange: function (e) {
    let that = this
    let checked_id = e.currentTarget.dataset.id
    // let checked_id = e.detail.value[0]
    let arr = that.data.checked_arr
    let idx = arr.indexOf(checked_id)
    if (idx >= 0){
      arr.splice(idx, 1)
    } else {
      arr.push(checked_id)
    }
    
    that.setData({
      checked_arr: arr,
      checked_str: arr.join(',')
    })
    console.log(that.data.checked_str)
  },
  changeSort: function() {
    
    let that = this
    console.log(that.data)
    var opt = {
      identity_type: that.data.identity_type,
      type: that.data.type,
    } 
    console.log(that.data.checked_str)
    util.request('V2/storageSort',
    {
      'id': that.data.checked_str,
    },
    function (res) {
      console.log(res.data.code)
      
      if(res.data.code == 1) {
        wx.showToast({
          title: '置顶成功',
          icon: 'none',
          duration: 2000
        })
        that.onLoad(opt)
        
      }
    })
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  select_item: function(e){
    var that = this
    var id = e.currentTarget.dataset.id
    var tmp_transfer_list = util.getUserArr('tmp_transfer_list')
    var transfer_info = util.getUserArr('help_transfer_info')
    var transfer_type = that.data.transfer_type
    if(tmp_transfer_list.length > 0){
      for(var i = 0;i<tmp_transfer_list.length;i++){
        if(tmp_transfer_list[i].id == id){
          console.log(tmp_transfer_list[i])

          var tmp_item = tmp_transfer_list[i]
          tmp_item.ot = true

          

          if (tmp_item.side_type == 1) {
            tmp_item.identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.identity_card_front_path]
            tmp_item.identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.identity_card_back_path]
          } else if (tmp_item.side_type == 2) {
            tmp_item.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.business_license_path]
            tmp_item.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_identity_card_back_path]
            tmp_item.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_identity_card_front_path]
            // tmp_item.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_seal_path];
            tmp_item.seal_path ='';
          } else if (tmp_item.side_type == 3) {
            tmp_item.business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.business_license_path]
            tmp_item.company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_identity_card_back_path]
            tmp_item.company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_identity_card_front_path]
            // tmp_item.seal_path = ['https://gongzheng.ma.cn/MYPublic/' + tmp_item.company_seal_path];
            tmp_item.seal_path = '';


            tmp_item.overseas_lawyer_img = []
            tmp_item.overseas_other_files = []

            let lawyer_img_arr = tmp_item.lawyer_img.split(',')

            for (var k = 0; k < lawyer_img_arr.length; k++) {
              tmp_item.overseas_lawyer_img = lawyer_img_arr[k].split(',')
              tmp_item.overseas_other_files = ('https://gongzheng.ma.cn/MYPublic/' + lawyer_img_arr[k]).split(',')
            }
          }

          tmp_item.transfer_type = tmp_item.side_type//转让类型

          

          
          tmp_item.apply_no = '';
          tmp_item.verifycode = '';
          

            
          transfer_info.push(tmp_item)
          // break
  

          // var tmp_item = tmp_transfer_list[i]
          // tmp_item.ot = true
          // tmp_item.transfer_type = tmp_item.side_type

          // tmp_item.overseas_lawyer_img = []
          // tmp_item.overseas_other_files = []

          // let lawyer_img_arr = tmp_item.lawyer_img.split(',')

          // for (var k = 0; k < lawyer_img_arr.length; k++) {
          //   tmp_item.overseas_lawyer_img = lawyer_img_arr[k].split(',')
          //   tmp_item.overseas_other_files = ('https://gongzheng.ma.cn/MYPublic/' + lawyer_img_arr[k]).split(',')
          // }

          // tmp_item.apply_no = '';
          // tmp_item.verifycode = ''; 
          
          // transfer_info.push(tmp_item)
        }
      }
    }

    console.log(transfer_info)

    // return false
    util.setUserdata('help_transfer_info',transfer_info)

    if (transfer_type == 1) {
      wx.reLaunch({
        url: '../help_transfer_info_personal/help_transfer_info_personal'
      })
    } else if (transfer_type == 2) {
      wx.reLaunch({
        url: '../help_transfer_info_company/help_transfer_info_company'
      })
    } else if (transfer_type == 3) {
      wx.reLaunch({
        url: '../help_transfer_info_overseas/help_transfer_info_overseas'
      })
    }
    
    // var url = this.data.type == 1 ? '../transfer_info_personal/transfer_info_personal' : '../transfer_info_company/transfer_info_company'
    // wx.navigateTo({
    //   url: url,
    // })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.setData({
      type:options.type,
      identity_type: options.identity_type
    })
    util.request('V2/storageList',
    {
      'type': options.type||1,
      'identity_type': options.identity_type || 1
    },
    function(res){
      console.log(res)
      if (res.data.code == 0) {
        // var tmp_transfer_list = util.getUserArr('tmp_transfer_list');
        // console.log(tmp_transfer_list)
        console.log(res.data.list)
        // tmp_transfer_list = res.data.list
        that.setData({
          transfer_type: options.type,
          transfer_list: res.data.list
        });
        util.setUserdata('tmp_transfer_list', res.data.list)
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})