//index.js
//获取应用实例
const util = require('../../utils/util.js')
const app = getApp()
Page({
  data: {
    tittle: '商标转让公证',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    hide_mask: true,
    commtime: 5,
    hide_selected: true
  },
  toRecharge: function() {
    wx.navigateTo({
      url: '../pay_recharge/pay_recharge',
      
    })
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  send_msg: function() {
    // wx.navigateTo({
    //   url: '../share_certification/share_certification',
    // })
  },
  clear_local: function() {
    // let empty = []
    // util.setUserdata('share_transfer_info', empty)
  },
  to_recharge: function(){
    wx.navigateTo({
      url: '../pay_recharge/pay_recharge',
    })
  },
  close_all: function () {
    let that = this
    that.setData({
      hide_mask: !that.data.hide_mask
    })
  },
  close_selected: function () {
    let that = this
    that.setData({
      hide_selected: !that.data.hide_selected
    })
  },
  close_adv: function(){
    let that = this
    that.setData({
      hide_mask: false
    })
  },
  //事件处理函数
  bindGetNotarizationTap: function(){
    // wx.navigateTo({
    //   url: '../get_notarization/get_notarization',
    // })
    wx.reLaunch({
      url: '../help_get_notarization/help_get_notarization',
    })

    // let that = this
    // that.setData({
    //   // commtime: 0,
    //   hide_selected: false
    // })
  },
  normal_operate: function() {
    wx.navigateTo({
      url: '../get_notarization/get_notarization',
    })
  },
  other_operate: function () {
    wx.navigateTo({
      url: '../help_get_notarization/help_get_notarization',
    })
  },
  bindMyNotarizationTap: function() {
    wx.reLaunch({
      url: '../my_notarization/my_notarization'
    })

   /*  wx.navigateTo({
      url: '../openpdf/openpdf'
    })*/

  },
  onLoad: function () {
    let that = this
    let myTime = setInterval(function(){
      console.log('倒计时减一')
      let time = that.data.commtime
      if (that.data.commtime == 0) {
        that.setData({
          commtime: 0,
          hide_mask: true
        })
        clearInterval(myTime)
      }else{
        that.setData({
          commtime: time - 1
        })
      }
    },1000)

/*
    wx.login({
      success(res) {
        console.log('小程序登录' + res.code)
        if (res.code) {
          //发起网络请求
          wx.request({
            url: 'https://test.com/onLogin',
            data: {
              code: res.code
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })
*/

     /*if (app.globalData.userInfo) {
       this.setData({
         userInfo: app.globalData.userInfo,
         hasUserInfo: true
       })
     } else if (this.data.canIUse){
       // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
       // 所以此处加入 callback 以防止这种情况
       app.userInfoReadyCallback = res => {
         this.setData({
           userInfo: res.userInfo,
           hasUserInfo: true
         })
       }
     } else {
       // 在没有 open-type=getUserInfo 版本的兼容处理
       wx.getUserInfo({
         success: res => {
           app.globalData.userInfo = res.userInfo
           this.setData({
             userInfo: res.userInfo,
             hasUserInfo: true
           })
         }
       })
     } */

  },
  getUserInfo: function(e) {
    console.log(e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  /**
   * 进行页面分享
   */
  onShareAppMessage: function (options) {
    if (options.from === 'button') {
      // 来自页面内转发按钮
      console.log(options.target)
    }
    return {
      //## 此为转发页面所显示的标题
      //title: '好友代付', 
      //## 此为转发页面的描述性文字
      desc: '商标转让在线公证，足不出户公证到家。',
      //## 此为转发给微信好友或微信群后，对方点击后进入的页面链接，可以根据自己的需求添加参数
      path: 'pages/index/index?sn=' + this.data.sn,
      imageUrl: '../../images/share_img.jpg',
      // path: 'pages/my_notarization/my_notarization?sn=' + this.data.sn,
      //## 转发操作成功后的回调函数，用于对发起者的提示语句或其他逻辑处理
      
      success: function (res) {
        //这是我自定义的函数，可替换自己的操作
      //  util.showToast(1, '发送成功');
        console.log('发送成功')
      },
      //## 转发操作失败/取消 后的回调处理，一般是个提示语句即可
      fail: function () {
      //  util.showToast(0, '朋友代付转发失败...');
        console.log('朋友转发失败...')
      }
    }
  }
})
