// pages/transfer_info/transfer_info.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderInfoId: 0
  },
  help_transfer_info_personal: function(){
    wx.navigateTo({
      url: '../help_transfer_info_personal/help_transfer_info_personal',
    })
  },
  help_transfer_info_company: function(){
    wx.navigateTo({
      url: '../help_transfer_info_company/help_transfer_info_company',
    })
  },
  help_transfer_info_overseas: function(){
    wx.navigateTo({
      url: '../help_transfer_info_overseas/help_transfer_info_overseas',
    })
  },
  unique_transfer_info: function(){
    console.log('转让人去重')
    var one = 1
    var transfer_info = util.getUserArr('help_transfer_info');
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 1) {
        var transfer_personal_flag = 0
        if (!transfer_info[i].personal_name || transfer_info[i].personal_name == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_card_front_vc || transfer_info[i].identity_card_front_vc == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_card_back_vc || transfer_info[i].identity_card_back_vc == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].identity_code || transfer_info[i].identity_code == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].personal_gender || (transfer_info[i].personal_gender != 1 && transfer_info[i].personal_gender != 2)) {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].personal_birth_date || transfer_info[i].personal_birth_date == '') {
          transfer_personal_flag += one
        }
        if (!util.checkDate(transfer_info[i].personal_birth_date)) {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          transfer_personal_flag += one
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          transfer_personal_flag += one
        }
        console.log(transfer_personal_flag)
        if (transfer_personal_flag == 9) {
          transfer_info.splice(i, 1)
        }

      } else if (transfer_info[i].transfer_type == 2) {
        var transfer_company_flag = 0
        if (!transfer_info[i].company_name || transfer_info[i].company_name == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].business_license_vc || transfer_info[i].business_license_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].social_credit_code || transfer_info[i].social_credit_code == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_personal_name || transfer_info[i].company_personal_name == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_card_front_vc || transfer_info[i].company_identity_card_front_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_card_back_vc || transfer_info[i].company_identity_card_back_vc == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_gender || (transfer_info[i].company_gender != 1 && transfer_info[i].company_gender != 2)) {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_birth_date || transfer_info[i].company_birth_date == '') {
          transfer_company_flag += one
        }
        if (!util.checkDate(transfer_info[i].company_birth_date)) {
          transfer_company_flag += one
        }
        if (!transfer_info[i].company_identity_code || transfer_info[i].company_identity_code == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          transfer_company_flag += one
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          transfer_company_flag += one
        }

        console.log(transfer_company_flag)
        if (transfer_company_flag == 12) {
          transfer_info.splice(i, 1)
        }
      } else if (transfer_info[i].transfer_type == 3) {
        var transfer_overseas_flag = 0
        if (!transfer_info[i].company_name || transfer_info[i].company_name == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].business_license_vc || transfer_info[i].business_license_vc == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].social_credit_code || transfer_info[i].social_credit_code == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_personal_name || transfer_info[i].company_personal_name == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_identity_card_front_vc || transfer_info[i].company_identity_card_front_vc == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_identity_card_back_vc || transfer_info[i].company_identity_card_back_vc == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].overseas_other_files || transfer_info[i].overseas_other_files == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_gender || (transfer_info[i].company_gender != 1 && transfer_info[i].company_gender != 2)) {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_birth_date || transfer_info[i].company_birth_date == '') {
          transfer_overseas_flag += one
        }
        if (!util.checkDate(transfer_info[i].company_birth_date)) {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].company_identity_code || transfer_info[i].company_identity_code == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].verifycode || transfer_info[i].verifycode == '') {
          transfer_overseas_flag += one
        }
        if (!transfer_info[i].signature_vc || transfer_info[i].signature_vc == '') {
          transfer_overseas_flag += one
        }

        console.log(transfer_overseas_flag)
        if (transfer_overseas_flag == 13) {
          transfer_info.splice(i, 1)
        }
      }
    }

    util.setUserdata('help_transfer_info', transfer_info)
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // if (options.orderInfoId) {
    //   this.setData({
    //     orderInfoId: options.orderInfoId
    //   })
    // }
    this.unique_transfer_info();
    this.reload();

  },
  reload: function () {
    var transfer_info = util.getUserArr('help_transfer_info');
    var transfer_remark = '';
    var transfer_remarkq = '';
    var transfer_remarkhk = '';
    for (var i = 0; i < transfer_info.length; i++) {
      console.log(transfer_info[i]);

      if (transfer_info.length > 0 && transfer_info[i]) {
        var transfer = transfer_info[i];
        var transfer_remarktmp="";
        if (transfer.transfer_type==1)
        {
          if ((transfer.personal_name && transfer.personal_name != '') || (transfer.company_name && transfer.company_name != '')) {
            transfer_remarktmp += "转让人：" + (transfer.personal_name || transfer.company_name || '') + '； '
          }

          if (transfer.transfer_type == 1) {
            if (transfer.identity_code && transfer.identity_code != '')
              transfer_remarktmp += "身份证号：" + (transfer.identity_code || '') + '； ';
            // if (transfer.identity_address && transfer.identity_address != '')
              // transfer_remarktmp += "地址：" + (transfer.identity_address || '') + '； ';
          }
          if (transfer.transfer_type == 2) {
            if (transfer.social_credit_code && transfer.social_credit_code != '')
              transfer_remarktmp += "社会信用代码：" + (transfer.social_credit_code || '') + '； ';
            // if (transfer.license_address && transfer.license_address != '')
              // transfer_remarktmp += "地址：" + (transfer.license_address || '') + '； ';
          }
          if (transfer_remarktmp){
            transfer_remark = transfer_remark+transfer_remarktmp+ "\n";
          }
          
        }
        else if (transfer.transfer_type == 2){

          var transfer_remarkqtmp="";
          if ((transfer.personal_name && transfer.personal_name != '') || (transfer.company_name && transfer.company_name != '')) {
            transfer_remarkqtmp += "转让人：" + (transfer.personal_name || transfer.company_name || '') + '； '
          }

          if (transfer.transfer_type == 1) {
            if (transfer.identity_code && transfer.identity_code != '')
              transfer_remarkqtmp += "身份证号：" + (transfer.identity_code || '') + '； ';
            // if (transfer.identity_address && transfer.identity_address != '')
            //   transfer_remarkqtmp += "地址：" + (transfer.identity_address || '') + '； ';
          }
          if (transfer.transfer_type == 2) {
            if (transfer.social_credit_code && transfer.social_credit_code != '')
              transfer_remarkqtmp += "社会信用代码：" + (transfer.social_credit_code || '') + '； ';
            // if (transfer.license_address && transfer.license_address != '')
            // transfer_remarkqtmp += "地址：" + (transfer.license_address || '') + '； ';
          }
          if (transfer_remarkqtmp)
          {
            transfer_remarkq = transfer_remarkq + transfer_remarkqtmp+ "\n";
          }
          
        }
        else if (transfer.transfer_type == 3){
          var transfer_remarkhktmp = "";
          if ((transfer.personal_name && transfer.personal_name != '') || (transfer.company_name && transfer.company_name != '')) {
            transfer_remarkhktmp += "转让人：" + (transfer.personal_name || transfer.company_name || '') + '； '
          }

          if (transfer.transfer_type == 3) {
            if (transfer.social_credit_code && transfer.social_credit_code != '')
              transfer_remarkhktmp += "香港注册编号：" + (transfer.social_credit_code || '') + '； ';
            // if (transfer.license_address && transfer.license_address != '')
            //   transfer_remarkhktmp += "地址：" + (transfer.license_address || '') + '； ';
          }

          if (transfer_remarkhktmp) {
            transfer_remarkhk = transfer_remarkhk + transfer_remarkhktmp + "\n";
          }

        }

    
     
        }
      
    }

    if (transfer_remark == '') {
      transfer_remark = '暂无资料'
    }

    if (transfer_remarkq == '') {
      transfer_remarkq = '暂无资料'
    }

    if (transfer_remarkhk == '') {
      transfer_remarkhk = '暂无资料'
    }

   
    this.setData({
      transfer_remarkp: transfer_remark,
      transfer_remark: transfer_remarkq,
      transfer_remarkhk: transfer_remarkhk,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    this.unique_transfer_info();
    this.reload();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})