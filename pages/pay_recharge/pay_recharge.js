// pages/send_msg/send_msg.js
const util = require('../../utils/util.js')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    member: null,
    money: '0.00',
    rechargeList: []
  },
  bindRechargeMoney: function(e){
    let that = this
    console.log(e)
    console.log(that.data.member)
    let openid = that.data.member.openid
    let member_id = that.data.member.id
    let rechargeList = that.data.rechargeList
    let selected_money = 0
    for (let i = 0; i < rechargeList.length; i++) {
      if (rechargeList[i].selected == true) {
        selected_money = rechargeList[i].money
      }
    }

    util.request(
      'V2/rechargeMoney', 
      {
        openid: openid,
        total_fee: selected_money,
        member_id: member_id
      },
      function (res) {
        console.log(res)
        let rtn = res
        if(res.data.code == 1) {
          wx.requestPayment({
            timeStamp: rtn.data.timeStamp,
            nonceStr: rtn.data.nonceStr,
            package: rtn.data.package,
            signType: 'MD5',
            paySign: rtn.data.paySign,
            success(res) {
            },
            fail(res) {
            }
          })
        }
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        wx.hideToast();
      }
    )
    
  },
  pay_selected: function(e) {
    console.log(e)
    let that = this
    let index = e.currentTarget.dataset.index
    let openid = that.data.member.openid
    let member_id = that.data.member.id
    let rechargeList = that.data.rechargeList
    let selected_money = e.currentTarget.dataset.money
    for(let i = 0; i < rechargeList.length; i++) {
      if(index == i) {
        rechargeList[i].selected = true
      }else{
        rechargeList[i].selected = false
      }
    }
    that.setData({
      rechargeList: rechargeList
    })

    setTimeout(() => {
      for (let i = 0; i < rechargeList.length; i++) {
        rechargeList[i].selected = false
      }
      that.setData({
        rechargeList: rechargeList
      })
    }, 500);



    wx.showModal({
      title: '提示',
      content: '确定充值' + selected_money+'元',
      success: function(sm){
        if(sm.confirm) {
          wx.showLoading({
            title: '正在充值中',
            icon: 'loading',
            mask: true
          })
          util.request(
            'V2/rechargeMoney',
            {
              openid: openid,
              total_fee: selected_money,
              // total_fee: 0.01,
              member_id: member_id
            },
            function (res) {
              wx.hideLoading();
              let rtn = res
              if (res.data.code == 1) {
                wx.requestPayment({
                  timeStamp: rtn.data.timeStamp,
                  nonceStr: rtn.data.nonceStr,
                  package: rtn.data.package,
                  signType: 'MD5',
                  paySign: rtn.data.paySign,
                  success(res) {
                    wx.navigateTo({
                      url: '../my_notarization/my_notarization',
                    })
                  },
                  fail(res) {
                    wx.showToast({
                      title: '充值已取消',
                      icon: 'none',
                      duration: 2000
                    })
                  }
                })
              } else {
                wx.showToast({
                  title: rtn.data.msg,
                  icon: 'none',
                  duration: 2000
                })
              }
            },
            function (res) {
              //尝试重启
              wx.reLaunch({
                url: 'pages/index/index'
              })
            },
            function (res) {
              wx.hideToast();
            }
          )
        }
      }
    })

    

    
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    let that = this;
    util.request(
      'V2/rechargeList', {},
      function (res) {
        console.log(res.data.data)
        let rechargeList = res.data.data
        for (let i = 0; i < rechargeList.length; i++) {
          rechargeList[i].selected = false
        }

        that.setData({
          rechargeList: rechargeList
        })
        wx.hideLoading()
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        wx.hideToast();
      }
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this
    util.request(
      'V2/myAllInfo',
      { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          that.setData({
            member: res.data.member,
            money: res.data.member.money
          })
        }
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        wx.hideToast();
      }
    )
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})