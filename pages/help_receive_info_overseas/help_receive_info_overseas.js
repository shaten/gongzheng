// pages/receive_info_overseas/receive_info_overseas.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    genderType: ["点击选择性别", "男", "女"],
    personal_list: [],
    cWidth: 0,
    cHeight: 0
  },
  social_credit_code_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].social_credit_code = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info);
  },
  company_name_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    console.log(receive_info);
    receive_info[index].company_name = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info)
  },
  license_address_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].license_address = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info);
  },
  company_personal_name_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].company_personal_name = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info);
  },
  company_address_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].company_address = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info);
  },
  company_phone_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].company_phone = e.detail.value
    this.setData({
      personal_list: receive_info
    });
    util.setUserdata('help_receive_info', receive_info);
  },
  touchStart: function (e) {
    this.touchTimeStamp = e.timeStamp
  },
  touchEnd: function (e) {
    var diff = e.timeStamp - this.touchTimeStamp
    var tag = e.currentTarget.dataset.tag
    if (tag == 'overseas_license_block') {
      diff >= 500 ? this.delete_overseas_license_files(e) : this.preview_overseas_license_files(e)
    }
  },
  delete_overseas_license_files: function (e) {
    var index = e.currentTarget.dataset.index
    var that = this
    if (this.data.personal_list[index].business_license_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var receive_info = util.getUserArr('help_receive_info')
            receive_info[index].business_license_path = '';
            receive_info[index].business_license_vc = '';
            util.setUserdata('help_receive_info', receive_info)
            that.reload()
          }
        }
      })
    }
  },
  preview_overseas_license_files: function (e) {
    var index = e.currentTarget.dataset.index
    if (this.data.personal_list[index].business_license_path) {
      wx.previewImage({
        urls: this.data.personal_list[index].business_license_path
      })
    }
  },
  choose_overseas_license_files: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    if (this.data.personal_list[index].business_license_path) return;
    wx.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('business_license_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'business_license_files',
                destWidth: canvasWidth,
                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadBusinessLicense'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token') },
                    filePath: result.tempFilePath,
                    name: 'business_license_' + index,
                    success(res) {
                      wx.hideLoading()
                      var rtn = JSON.parse(res.data)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      var receive_info = util.getUserArr('help_receive_info')
                      receive_info[index].business_license_vc = rtn.verify_code

                      receive_info[index].business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                      receive_info[index].social_credit_code = '000000000000000000'
                      receive_info[index].company_personal_name = '000000'

                      // if (rtn.company_name) {
                      //   receive_info[index].company_name = rtn.company_name
                      // }

                      // if (rtn.social_credit_code) {
                      //   receive_info[index].social_credit_code = rtn.social_credit_code
                      // }

                      // if (rtn.company_personal_name) {
                      //   receive_info[index].company_personal_name = rtn.company_personal_name
                      // }

                      // if (rtn.license_address) {
                      //   receive_info[index].license_address = rtn.license_address
                      // }

                      that.setData({
                        personal_list: receive_info
                      })

                      util.setUserdata('help_receive_info', receive_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })
      }
    })
  },
  save: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var receive_remarkp = ''

    // for (var i = 0; i < pageData.length; i++) {
    if (pageData[index].receive_type == 3) {
      if (!pageData[index].business_license_path) {
        wx.showToast({
          title: '请上传营业执照',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      // if (!pageData[index].social_credit_code) {
      //   wx.showToast({
      //     title: '请填写公司注册编码',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }


      if (!pageData[index].company_name) {
        wx.showToast({
          title: '请填写公司名',
          icon: 'none',
          duration: 2000
        })
        return false
      }
      // receive_remarkp += '受让人:' + pageData[index].company_name + ';'
      // receive_remarkp += '公司注册编码:' + pageData[index].social_credit_code + ';'

      // if (!pageData[index].license_address) {
      //   wx.showToast({
      //     title: '请填写公司地址',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      // receive_remarkp += '地址:' + pageData[index].license_address + ';'

      // if (!pageData[index].company_personal_name) {
      //   wx.showToast({
      //     title: '请填写法人代表',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      // if (!pageData[index].company_address) {
      //   wx.showToast({
      //     title: '请填写联系地址',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      // if (!pageData[index].company_phone) {
      //   wx.showToast({
      //     title: '请填写联系电话',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      pageData[index].isShow = true
      pageData[index].receive_remarkp = receive_remarkp
    }
    // }

    var receive_info = util.getUserArr('help_receive_info')
    receive_info[index].business_license_path = pageData[index].business_license_path
    receive_info[index].business_license_vc = pageData[index].business_license_vc
    receive_info[index].company_address = pageData[index].company_address
    receive_info[index].company_name = pageData[index].company_name
    receive_info[index].company_personal_name = pageData[index].company_personal_name
    receive_info[index].company_phone = pageData[index].company_phone
    receive_info[index].license_address = pageData[index].license_address
    receive_info[index].receive_type = 3
    receive_info[index].social_credit_code = pageData[index].social_credit_code
    receive_info[index].isShow = pageData[index].isShow

    that.setData({
      personal_list: pageData
    })

    console.log(receive_info);
    wx.showToast({
      title: '保存成功',
      icon: 'none',
      duration: 2000
    })
    wx.reLaunch({
      url: '../help_get_notarization/help_get_notarization',
    })
    return false

    wx.showModal({
      title: '提示',
      content: '是否确认保存',
      cancelText: "返回",
      confirmText: "继续添加",
      success: function (sm) {
        util.setUserdata('help_receive_info', receive_info)
        if (sm.cancel) {
          wx.navigateTo({
            url: '../help_receive_info/help_receive_info',
          })
        }
      }
    })

  },
  clear_input: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    var receive_info = util.getUserArr('help_receive_info')
    var pageList = that.data.personal_list

    console.log(receive_info)
    console.log(pageList)

    wx.showModal({
      title: '提示',
      content: '确定要清空吗？',
      success: function (sm) {
        if (sm.confirm) {
          for (var i = 0; i < pageList.length; i++) {
            if (i == index && pageList[i].receive_type == 3) {
              pageList[i].business_license_path = '';
              pageList[i].business_license_vc = '';
              pageList[i].company_name = '';
              pageList[i].company_personal_name = '';
              pageList[i].company_address = '';
              pageList[i].company_phone = '';
              pageList[i].license_address = '';
              pageList[i].receive_type = 3;
              pageList[i].social_credit_code = '';
              pageList[i].receive_remarkp = '';
            }
          }
          that.setData({
            personal_list: pageList
          });

          for (var i = 0; i < receive_info.length; i++) {
            if (i == index && receive_info[i].receive_type == 3) {
              receive_info[i].business_license_path = '';
              receive_info[i].business_license_vc = '';
              receive_info[i].company_name = '';
              receive_info[i].company_personal_name = '';
              receive_info[i].company_address = '';
              receive_info[i].company_phone = '';
              receive_info[i].license_address = '';
              receive_info[i].receive_type = 3;
              receive_info[i].social_credit_code = '';
              receive_info[i].receive_remarkp = '';
            }
          }

          console.log(receive_info)
          util.setUserdata('help_receive_info', receive_info)
        }
      }
    })
  },
  add: function(){
    let receive_info = util.getUserArr('help_receive_info')
    receive_info.push({ 'receive_type': 3, 'ot': false, isShow: false, receive_remarkp: '' })
    this.setData({
      personal_list: receive_info
    })
    console.log(receive_info)
    util.setUserdata('help_receive_info', receive_info)
  },
  change_isShow: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var receive_remarkp = ''
    var receive_info = util.getUserArr('help_receive_info')

    console.log(receive_info);
    receive_info[index].isShow = !receive_info[index].isShow
    pageData[index].company_name ? receive_remarkp += '受让人:' + pageData[index].company_name + ';' : ''
    // pageData[index].social_credit_code ? receive_remarkp += '公司注册编码:' + pageData[index].social_credit_code + ';' : ''
    // pageData[index].license_address ? receive_remarkp += '地址:' + pageData[index].license_address + ';' : ''

    receive_info[index].receive_remarkp = receive_remarkp
    that.setData({
      personal_list: receive_info
    })
    util.setUserdata('help_receive_info', receive_info)
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    var receive_info = util.getUserArr('help_receive_info')
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          receive_info.splice(index, 1)
          that.setData({
            personal_list: receive_info
          })
          util.setUserdata('help_receive_info', receive_info)
        }
      }
    })
  },
  reload: function(){
    var receive_info = util.getUserArr('help_receive_info')
    console.log(receive_info)
    for (var i = 0; i < receive_info.length; i++) {
      if (receive_info[i].transfer_type == 3) {
        var transfer_remarkp = ''
        var num = 0

        // if (receive_info[i].overseas_license_path) {
        //   num += 1
        // }

        // if (receive_info[i].overseas_identity_card_front_path) {
        //   num += 1
        // }
        // if (receive_info[i].choose_overseas_identity_card_back_files) {
        //   num += 1
        // }
        // if (receive_info[i].company_name) {
        //   transfer_remarkp += '公司名:' + receive_info[i].company_name + ';'
        //   num += 1
        // }
        // if (receive_info[i].social_credit_code) {
        //   transfer_remarkp += '公司注册编号:' + receive_info[i].social_credit_code + ';'
        //   num += 1
        // }
        // if (receive_info[i].company_personal_name) {
        //   num += 1
        // }
        // if (receive_info[i].company_identity_code) {
        //   num += 1
        // }
        // if (receive_info[i].company_gender) {
        //   num += 1
        // }
        // if (receive_info[i].company_birth_date) {
        //   num += 1
        // }

        // if (receive_info[i].verifycode) {
        //   num += 1
        // }
        // if (receive_info[i].signature_vc) {
        //   num += 1
        // }

        // if (receive_info[i].signature_path) {
        //   num += 1
        // }


        console.log(num)
        //后面改12
        if (num >= 0) {
          console.log('ok')
          receive_info[i].isShow = true
          receive_info[i].transfer_remarkp = transfer_remarkp
        }
      }
    }

    this.setData({
      personal_list: receive_info
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reload()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})