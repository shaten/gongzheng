Page({
  data: {
    tempFilePaths: '',
    hidden: true,
    buthidden: false,
    sourceType: ['album', 'camera'],
  },
  onLoad: function (options) {
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
      console.log(platform)
    } catch (e) { }
  },
  frontimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          FilePaths: res.tempFilePaths
        })
      }
    })
  },
  reciteimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          recitePaths: res.tempFilePaths
        })
      }
    })
  },
  start: function () {
    wx.showActionSheet({
      itemList: ['从手机相册选择', '拍照'],
      success: function (res) {
        console.log(res.tapIndex)
        wx.chooseImage({
          count: 1, // 默认9
          sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
          sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
          success: function (res) {
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            var tempFilePaths = res.tempFilePaths;
          },
          fail: function (res) {
            console.log(res.errMsg)
          }
        })
      },
      fail: function (res) {
        console.log(res.errMsg)
      }
    })
  }
})