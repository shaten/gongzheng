// pages/pay_msg/pay_msg.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  order_info: function(e){
    wx.navigateTo({
      url: '../order_info/order_info?id=' + e.currentTarget.dataset.id,
    })
  },
  go_index: function(){
    wx.reLaunch({
      url: '../index/index',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   * 
   */
  onLoad: function (options) {
    console.log(options)
    var hs_icon = false
    var hs_title = '支付失败'
    var hs_content = ''
    var oid = options.id

    if(options.s && options.s == 1){
      hs_icon = true
      hs_title = '支付成功'
    }
    this.setData({
      hs_icon:hs_icon,
      hs_title:hs_title,
      hs_content:hs_content,
      oid:oid
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})