const util = require('../../utils/util.js')

// pages/userphonehas/userphonehas.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    util.request('V2/myAllInfo', { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          util.setUserdata('member', res.data.member)
          that.setData({
            order_count_1: res.data.order_count_1,
            order_count_2: res.data.order_count_2,
            order_count_3: res.data.order_count_3,
            personal_count: res.data.personal_count,
            company_count: res.data.company_count,
            member: res.data.member
          })
        }
      })


    var member = util.getUserdata('member')
    console.log(member);
    this.setData({
      member: member
    })
  },
  bindGetNotarizationTap:function(){

    wx.navigateTo({
      url: '../userphone/userphone',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    util.request('V2/myAllInfo', { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          util.setUserdata('member', res.data.member)
          that.setData({
            order_count_1: res.data.order_count_1,
            order_count_2: res.data.order_count_2,
            order_count_3: res.data.order_count_3,
            personal_count: res.data.personal_count,
            company_count: res.data.company_count,
            member: res.data.member
          })
        }
      })

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})