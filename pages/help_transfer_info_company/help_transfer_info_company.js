// pages/transfer_info_company/transfer_info_company.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    genderType: ["点击选择性别","男", "女"],
    genderTypeIndex: 0,
    personal_list : [],
    orderInfoId: 0,
    cWidth: 0,
    cHeight: 0
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  go_select: function(e){
    wx.navigateTo({
      // url: '../transfer_info_select/transfer_info_select?type=2&identity_type=1',
      url: '../help_transfer_info_select/help_transfer_info_select?type=2&identity_type=1',
    })
  },
  buildSeal: function(e){
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    var company_name = transfer_info[index].company_name
    console.log(company_name)
    if (company_name) {
      this.createSeal(company_name, index)
    }else{
      wx.showToast({
        title: '公司名不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }
    
  },
  identify_check(e) {
    console.log("identify_check");
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('transfer_info')
    var name = transfer_info[index].company_personal_name
    var identity_code = transfer_info[index].company_identity_code

    if (e.currentTarget.dataset.text == '已通过') {
      return false
    }

    var msg = "";
    if (!name || name == "") {
      wx.showToast({
        title: '姓名不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }

    if (!identity_code || identity_code == "") {
      wx.showToast({
        title: '身份证号码不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }
    console.log("第" + index + "个转让人");
    util.setUserdata('transfer_info_GetToken_index', index + 1);
    //return wx.showToast("身份证号码不能为空!");
    //   util.request('V3/identifyCheck',
    wx.showLoading({
      title: '程序加载中',
      icon: 'loading',
      mask: true
    })

    var apply_no = "";
    if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
      apply_no = util.getUserArr('transfer_info_apply_nos');
    }
    console.log('transfer_info_apply_nos==')
    console.log(util.getUserArr('transfer_info_apply_nos'))
    util.request('V3/GetToken',
      {
        'name': name,
        'identity_code': identity_code,
        'apply_no': apply_no
      },
      function (res) {
        wx.hideLoading();
        console.log(res)
        if (res.data.code == 0) {

          var transfer_info = util.getUserArr('transfer_info')

          if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
            console.log("transfer_info_apply_nos已存在值")
            console.log(util.getUserArr('transfer_info_apply_nos'))
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = util.getUserArr('transfer_info_apply_nos');//apply_no赋值
          } else {
            console.log("给transfer_info_apply_nos赋值")
            util.setUserdata('transfer_info_apply_nos', res.data.apply_no);
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = res.data.apply_no; //apply_no赋值
          }

          util.setUserdata('transfer_info', transfer_info)

          /*
         //旧的认证
          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: 'pages/idverification/idverification?certoken=' + res.data.token,//pages/idverification/idverification?certoken=
            envVersion: 'trial',
            success(res) {
              // 打开成功
              console.log("open other app ok!")
            }
          })
          */


          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: '/pages/idverification/idverification',
            envVersion: 'release',
            extraData: {
              'token': res.data.access_token, //身份凭证accessToken
              'applyNo': res.data.apply_no,//订单号applyNo
              'name': name, //当事人姓名
              'idno': identity_code //当事人身份证号码
            },
            success(res) {
              console.log(res);
            }
          });
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 1000
          });
        }
      })
  },
  genderChange: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_gender = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info)
  },
  choose_business_license_files: function (e) {
      var that = this
      var index = e.currentTarget.dataset.index
      if(this.data.personal_list[index].business_license_path) return;
      wx.chooseImage({
          count:　1,
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (photo) {
            var image_size = 5 * 1024 * 1024
            if (photo.tempFiles[0].size > image_size) {
              wx.showToast({
                title: '上传图片不能超过5M',
                icon: 'none',
                duration: 2000
              })
              return false
            }

            wx.getImageInfo({
              src: photo.tempFilePaths[0], 
              success: function (res) {
                console.log(res);
                //---------利用canvas压缩图片--------------
                var ratio = 2;
                var canvasWidth = res.width //图片原始长宽
                var canvasHeight = res.height
                while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
                  canvasWidth = Math.trunc(res.width / ratio)
                  canvasHeight = Math.trunc(res.height / ratio)
                  ratio++;
                }
                that.setData({
                  cWidth: canvasWidth,
                  cHeight: canvasHeight
                })

                //----------绘制图形并取出图片路径--------------
                var ctx = wx.createCanvasContext('business_license_files')
                ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
                ctx.draw(false, setTimeout(function () {
                  wx.canvasToTempFilePath({
                    canvasId: 'business_license_files',
                    destWidth: canvasWidth,
                    destHeight: canvasHeight,
                    success: function (result) {
                      console.log(result.tempFilePath)//最终图片路径
                      
                      //上传图片
                      wx.showLoading({
                        title: '图片上传中',
                        icon: 'loading',
                        mask: true
                      })
                      wx.uploadFile({
                        url: util.reqUrl('V2/uploadBusinessLicense'),
                        header: util.headers(),
                        formData: { 'identity_token': util.getUserdata('identity_token') },
                        filePath: result.tempFilePath,
                        name: 'business_license_' + index,
                        success(res) {
                          wx.hideLoading();
                          console.log(res);
                          var rtn = JSON.parse(res.data)
                          if (rtn.code < 0) {
                            wx.showToast({
                              title: rtn.err,
                              icon: 'none',
                              duration: 2000
                            })
                            return false
                          }
                          console.log(rtn)
                          var transfer_info = util.getUserArr('help_transfer_info')
                          transfer_info[index].business_license_vc = rtn.verify_code

                          // transfer_info[index].business_license_path = res.image_name

                          transfer_info[index].business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                          if (rtn.company_name) {
                            transfer_info[index].company_name = rtn.company_name
                            that.createSeal(rtn.company_name, index)
                          }

                          if (rtn.social_credit_code) {
                            transfer_info[index].social_credit_code = rtn.social_credit_code
                          }

                          if (rtn.company_personal_name) {
                            transfer_info[index].company_personal_name = rtn.company_personal_name
                          }

                          // if (rtn.license_address) {
                          //   transfer_info[index].license_address = rtn.license_address
                          // }

                          that.setData({
                            personal_list: transfer_info
                          })

                          util.setUserdata('help_transfer_info', transfer_info)
                        }
                      })
                      
                    },
                    fail: function (res) {
                      console.log(res.errMsg)
                    }
                  })
                },500))
              }
            })
          }
      })
  },
  choose_company_identity_card_front_files: function (e) {
      var that = this
      var index = e.currentTarget.dataset.index
      if(this.data.personal_list[index].company_identity_card_front_path) return;
      wx.chooseImage({
          count: 1,
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (photo) {
            var image_size = 5 * 1024 * 1024
            if (photo.tempFiles[0].size > image_size) {
              wx.showToast({
                title: '上传图片不能超过5M',
                icon: 'none',
                duration: 2000
              })
              return false
            }

            wx.getImageInfo({
              src: photo.tempFilePaths[0],
              success: function (res) {
                console.log(res);
                //---------利用canvas压缩图片--------------
                var ratio = 2;
                var canvasWidth = res.width //图片原始长宽
                var canvasHeight = res.height
                while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
                  canvasWidth = Math.trunc(res.width / ratio)
                  canvasHeight = Math.trunc(res.height / ratio)
                  ratio++;
                }
                that.setData({
                  cWidth: canvasWidth,
                  cHeight: canvasHeight
                })

                //----------绘制图形并取出图片路径--------------
                var ctx = wx.createCanvasContext('company_identity_card_front_files')
                ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
                ctx.draw(false, setTimeout(function () {
                  wx.canvasToTempFilePath({
                    canvasId: 'company_identity_card_front_files',
                    destWidth: canvasWidth,
                    destHeight: canvasHeight,
                    success: function (result) {
                      console.log(result.tempFilePath)//最终图片路径

                      //上传图片
                      wx.showLoading({
                        title: '图片上传中',
                        icon: 'loading',
                        mask: true
                      })
                      wx.uploadFile({
                        url: util.reqUrl('V2/uploadIdentityFront'),
                        header: util.headers(),
                        formData: { 'identity_token': util.getUserdata('identity_token') },
                        filePath: result.tempFilePath,
                        name: 'identity_card_front_' + index,
                        success(res) {
                          wx.hideLoading()
                          var rtn = JSON.parse(res.data)
                          console.log(rtn)
                          if (rtn.code < 0) {
                            wx.showToast({
                              title: rtn.err,
                              icon: 'none',
                              duration: 2000
                            })
                            return false
                          }
                          var transfer_info = util.getUserArr('help_transfer_info')
                          transfer_info[index].company_identity_card_front_vc = rtn.verify_code

                          transfer_info[index].company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                          if (rtn.personal_name) {
                            transfer_info[index].company_personal_name = rtn.personal_name
                          }

                          if (rtn.identity_code) {
                            transfer_info[index].company_identity_code = rtn.identity_code
                          }

                          if (rtn.personal_gender) {
                            transfer_info[index].company_gender = rtn.personal_gender
                          }

                          if (rtn.personal_birth_date) {
                            transfer_info[index].company_birth_date = rtn.personal_birth_date
                          }

                          that.setData({
                            personal_list: transfer_info
                          })

                          util.setUserdata('help_transfer_info', transfer_info)
                        }
                      })

                    },
                    fail: function (res) {
                      console.log(res.errMsg)
                    }
                  })
                }, 500))
              }
            })
          }
      })
  },
  choose_company_identity_card_back_files: function (e) {
      var that = this
      var index = e.currentTarget.dataset.index
      if(this.data.personal_list[index].company_identity_card_back_path) return;
      wx.chooseImage({
          count: 1,
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (photo) {
            var image_size = 5 * 1024 * 1024
            if (photo.tempFiles[0].size > image_size) {
              wx.showToast({
                title: '上传图片不能超过5M',
                icon: 'none',
                duration: 2000
              })
              return false
            }

            wx.getImageInfo({
              src: photo.tempFilePaths[0],
              success: function (res) {
                console.log(res);
                //---------利用canvas压缩图片--------------
                var ratio = 2;
                var canvasWidth = res.width //图片原始长宽
                var canvasHeight = res.height
                while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
                  canvasWidth = Math.trunc(res.width / ratio)
                  canvasHeight = Math.trunc(res.height / ratio)
                  ratio++;
                }
                that.setData({
                  cWidth: canvasWidth,
                  cHeight: canvasHeight
                })

                //----------绘制图形并取出图片路径--------------
                var ctx = wx.createCanvasContext('company_identity_card_back_files')
                ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
                ctx.draw(false, setTimeout(function () {
                  wx.canvasToTempFilePath({
                    canvasId: 'company_identity_card_back_files',
                    destWidth: canvasWidth,
                    destHeight: canvasHeight,
                    success: function (result) {
                      console.log(result.tempFilePath)//最终图片路径

                      //上传图片
                      wx.showLoading({
                        title: '图片上传中',
                        icon: 'loading',
                        mask: true
                      })
                      wx.uploadFile({
                        url: util.reqUrl('V2/uploadIdentityBack'),
                        header: util.headers(),
                        formData: { 'identity_token': util.getUserdata('identity_token') },
                        filePath: result.tempFilePath,
                        name: 'identity_card_back_' + index,
                        success(res) {
                          wx.hideLoading()
                          var rtn = JSON.parse(res.data)
                          if (rtn.code < 0) {
                            wx.showToast({
                              title: rtn.err,
                              icon: 'none',
                              duration: 2000
                            })
                            return false
                          }
                          var transfer_info = util.getUserArr('help_transfer_info')
                          transfer_info[index].company_identity_card_back_vc = rtn.verify_code

                          transfer_info[index].company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                          that.setData({
                            personal_list: transfer_info
                          })

                          util.setUserdata('help_transfer_info', transfer_info)
                        }
                      })

                    },
                    fail: function (res) {
                      console.log(res.errMsg)
                    }
                  })
                }, 500))
              }
            })
          }
      })
  },
  choose_signature_files(e){
    var that = this
    var index = e.currentTarget.dataset.index
    if(this.data.personal_list[index].signature_path) return;
    wx.navigateTo({
      url: '../help_signature/help_signature?page=help_transfer_info_company&type=1&index=' + index
    })
  },
  save: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''

    console.log(pageData);
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    console.log(index);
    // console.log(pageData[index].personal_birth_date)

    // for(var i = 0; i < pageData.length; i++) {
      if (pageData[index].transfer_type == 2) {
        if (!pageData[index].business_license_path) {
          wx.showToast({
            title: '请上传营业执照',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (!pageData[index].company_identity_card_front_path) {
          wx.showToast({
            title: '请上传身份证正面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (!pageData[index].company_identity_card_back_path) {
          wx.showToast({
            title: '请上传身份证背面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].company_name == '') {
          wx.showToast({
            title: '请输入公司名',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        transfer_remarkp += '转让人:' + pageData[index].company_name + ';'

        if (pageData[index].social_credit_code == '' || pageData[index].social_credit_code == '无') {
          wx.showToast({
            title: '请输入社会信用代码',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        transfer_remarkp += '社会信用代码:' + pageData[index].social_credit_code + ';'

        // if (pageData[index].license_address == '') {
        //   wx.showToast({
        //     title: '请输入公司地址',
        //     icon: 'none',
        //     duration: 2000
        //   })
        //   return false
        // }
        transfer_remarkp += '地址:' + pageData[index].license_address + ';'

        if (pageData[index].company_personal_name == '') {
          wx.showToast({
            title: '请输入法定代表人',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].company_identity_code == '') {
          wx.showToast({
            title: '请输入法人身份证号',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].company_gender == 0) {
          wx.showToast({
            title: '请选择法人性别',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (pageData[index].company_birth_date == '') {
          wx.showToast({
            title: '请选择法人出生日期',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (!pageData[index].seal_path) {
          wx.showToast({
            title: '请生成公司电子印章',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        pageData[index].isShow = true
        pageData[index].transfer_remarkp = transfer_remarkp
      }
    // }

    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].business_license_path = pageData[index].business_license_path
    transfer_info[index].business_license_vc = pageData[index].business_license_vc
    transfer_info[index].company_birth_date = pageData[index].company_birth_date
    transfer_info[index].company_gender = pageData[index].company_gender
    transfer_info[index].company_identity_card_back_path = pageData[index].company_identity_card_back_path
    transfer_info[index].company_identity_card_back_vc = pageData[index].company_identity_card_back_vc
    transfer_info[index].company_identity_card_front_path = pageData[index].company_identity_card_front_path
    transfer_info[index].company_identity_card_front_vc = pageData[index].company_identity_card_front_vc
    transfer_info[index].company_identity_code = pageData[index].company_identity_code
    transfer_info[index].company_name = pageData[index].company_name
    transfer_info[index].company_personal_name = pageData[index].company_personal_name
    transfer_info[index].license_address = pageData[index].license_address
    transfer_info[index].seal_path = pageData[index].seal_path
    transfer_info[index].company_address = pageData[index].company_address
    transfer_info[index].company_phone = pageData[index].company_phone
    transfer_info[index].social_credit_code = pageData[index].social_credit_code
    transfer_info[index].transfer_type = 2
    transfer_info[index].isShow = pageData[index].isShow

    that.setData({
      personal_list: pageData
    })

    console.log(transfer_info);
    wx.showToast({
      title: '保存成功',
      icon: 'none',
      duration: 2000
    })
    wx.reLaunch({
      url: '../help_get_notarization/help_get_notarization',
    })
    return false

    // wx.showModal({
    //   title: '提示',
    //   content: '是否确认保存',
    //   cancelText: "返回",
    //   confirmText: "继续添加",
    //   success: function (sm) {
    //     util.setUserdata('transfer_info', transfer_info)
    //     if (sm.cancel) {
    //       wx.navigateBack({
    //         delta: 1
    //       })
    //     }
    //   }
    // })
  },
  add: function () {
    var transfer_info = util.getUserArr('help_transfer_info')
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 2) {
        var transfer_remarkp = ''
        var num = 0

        if (transfer_info[i].business_license_path) {
          console.log('kk' + 1)
          num += 1
        } else {
          wx.showToast({
            title: '请上传以上营业执照',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (transfer_info[i].company_identity_card_front_path) {
          console.log('kk' + 1)
          num += 1
        } else {
          wx.showToast({
            title: '请上传以上身份证正面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].company_identity_card_back_path) {
          console.log('kk' + 2)
          num += 1
        } else {
          wx.showToast({
            title: '请上传以上身份证背面照片',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].company_name) {
          console.log('kk' + 3)
          transfer_remarkp += '转让人:' + transfer_info[i].company_name + ';'
          num += 1
        } else {
          wx.showToast({
            title: '请填写以上转让人公司名',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].social_credit_code && transfer_info[i].social_credit_code != '无' && transfer_info[i].social_credit_code != '') {
          console.log('kk' + 4)
          transfer_remarkp += '社会信用代码:' + transfer_info[i].social_credit_code + ';'
          num += 1
        } else {
          wx.showToast({
            title: '请填写以上社会信用代码',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        // if (transfer_info[i].license_address) {
        //   console.log('kk' + 5)
        //   num += 1
        // } else {
        //   wx.showToast({
        //     title: '请填写以上公司地址',
        //     icon: 'none',
        //     duration: 2000
        //   })
        //   return false
        // }
        if (transfer_info[i].company_personal_name) {
          console.log('kk' + 7)
          num += 1
        } else {
          wx.showToast({
            title: '请填写以上法定代表人',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].company_identity_code) {
          console.log('kk' + 7)
          num += 1
        } else {
          wx.showToast({
            title: '请填写以上身份证号码',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].company_gender) {
          console.log('kk' + 6)
          num += 1
        } else {
          wx.showToast({
            title: '请选择以上转让人性别',
            icon: 'none',
            duration: 2000
          })
          return false
        }
        if (transfer_info[i].company_birth_date) {
          console.log('kk' + 6)
          num += 1
        } else {
          wx.showToast({
            title: '请选择以上出生日期',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        if (transfer_info[i].seal_path) {
          console.log('kk' + 9)
          num += 1
        } else {
          wx.showToast({
            title: '请生成第' + (i + 1) + '转让人公司电子印章',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        console.log(num)
        //后面改11
        if (num >= 11) {
          console.log('ok')
          transfer_info[i].isShow = true
          transfer_info[i].transfer_remarkp = transfer_remarkp
        }
      }
    }

    transfer_info.push({ 'transfer_type': 2, 'ot': false, isShow: false, transfer_remarkp: ''})
    this.setData({
      personal_list: transfer_info
    })
    console.log(transfer_info)
    var index = transfer_info.length - 1;
    console.log(index);
    util.setUserdata('help_transfer_info',transfer_info)
  },
  change_isShow: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''
    var transfer_info = util.getUserArr('help_transfer_info')

    console.log(transfer_info[index].isShow);
    console.log(pageData[index].company_name);
    transfer_info[index].isShow = !transfer_info[index].isShow
    pageData[index].company_name ? transfer_remarkp += '转让人:' + pageData[index].company_name + ';' : ''
    pageData[index].social_credit_code ? transfer_remarkp += '社会信用代码:' + pageData[index].social_credit_code + ';' : ''
    pageData[index].license_address ? transfer_remarkp += '地址:' + pageData[index].license_address + ';' : ''

    transfer_info[index].transfer_remarkp = transfer_remarkp
    that.setData({
      personal_list: transfer_info
    })
    util.setUserdata('help_transfer_info', transfer_info)
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function(sm) {
        if(sm.confirm) {
          if (transfer_info[index].transfer_type != 2) {
            console.log(transfer_info[index])
          }
          transfer_info.splice(index, 1)
          that.setData({
            personal_list: transfer_info
          })
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })

    
    
  },
  preview_business_license_files: function(e){
    var that = this
    var index = e.currentTarget.dataset.index
    if(this.data.personal_list[index].business_license_path){
      wx.previewImage({
        urls: this.data.personal_list[index].business_license_path
      })
    }
  },
  preview_company_identity_card_front_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this

    if(this.data.personal_list[index].company_identity_card_front_path){
      wx.previewImage({
        urls: this.data.personal_list[index].company_identity_card_front_path
      })
    }
  },
  preview_company_identity_card_back_files: function(e){
    var index = e.currentTarget.dataset.index;
    if(this.data.personal_list[index].company_identity_card_back_path){
      wx.previewImage({
        urls: this.data.personal_list[index].company_identity_card_back_path
      })
    }
  },
  preview_signature_files: function(e){
    var index = e.currentTarget.dataset.index;
    if(this.data.personal_list[index].signature_path){
      wx.previewImage({
        urls: [this.data.personal_list[index].signature_path]
      })
    }
  },
  preview_seal_files_img: function(e) {
    var index = e.currentTarget.dataset.index;
    if (this.data.personal_list[index].seal_path) {
      wx.previewImage({
        urls: this.data.personal_list[index].seal_path
      })
    }
  },
  preview_seal_files: function(e){
    var that = this
    var index = e.currentTarget.dataset.index
    console.log(this.data.personal_list);
    if (this.data.personal_list[index].seal_path) return;



    // var index = e.currentTarget.dataset.index;
    // if(this.data.personal_list[index].seal_path){
    //   wx.previewImage({
    //     urls: [this.data.personal_list[index].seal_path]
    //   })
    // }
  },
  touchStart: function(e){
    console.log(e);
    this.touchTimeStamp = e.timeStamp
  },
  touchEnd: function (e) {
    var diff = e.timeStamp - this.touchTimeStamp
    var tag = e.currentTarget.dataset.tag
    if (tag == 'business_license_block') {
      diff >= 500 ? this.delete_business_license_files(e) : this.preview_business_license_files(e)
    } 
    if (tag == 'company_identity_card_front_block'){
      diff >= 500 ? this.delete_company_identity_card_front_files(e) : this.preview_company_identity_card_front_files(e)
    } 
    if (tag == 'company_identity_card_back_block') {
      diff >= 500 ? this.delete_company_identity_card_back_files(e) : this.preview_company_identity_card_back_files(e)
    }
    if (tag == 'signature_block') {
      diff >= 500 ? this.delete_signature_files(e) : this.preview_signature_files(e)
    }
    if (tag == 'seal_files_test') {
      console.log(diff)
      diff >= 500 ? this.delete_seal_files(e) : this.preview_seal_files_img(e)
    }
  },
  delete_business_license_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    console.log(this.data.personal_list[index] + 'fsaaffa')
    if(this.data.personal_list[index].business_license_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            console.log(transfer_info);
            transfer_info[index].business_license_path = '';
            transfer_info[index].business_license_vc = '';
            // if (transfer_info[index].company_identity_card_front_path) {
            //   transfer_info[index].company_identity_card_front_path = that.data.personal_list[index].company_identity_card_front_path
            // }
            // if (transfer_info[index].company_identity_card_back_path) {
            //   transfer_info[index].company_identity_card_back_path = that.data.personal_list[index].company_identity_card_back_path
            // }
            // if (transfer_info[index].seal_path) {
            //   transfer_info[index].seal_path = that.data.personal_list[index].seal_path
            // }
            // if (transfer_info[index].signature_path) {
            //   transfer_info[index].signature_path = that.data.personal_list[index].signature_path
            // }
            util.setUserdata('help_transfer_info',transfer_info)

            that.reload()
          } 
        }
      })
    }
  },
  delete_company_identity_card_front_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    if(this.data.personal_list[index].company_identity_card_front_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].company_identity_card_front_path = '';
            transfer_info[index].company_identity_card_front_vc = '';

            // if (transfer_info[index].business_license_path) {
            //   transfer_info[index].business_license_path = that.data.personal_list[index].business_license_path
            // }
            // if (transfer_info[index].company_identity_card_back_path) {
            //   transfer_info[index].company_identity_card_back_path = that.data.personal_list[index].company_identity_card_back_path
            // }
            // if (transfer_info[index].seal_path) {
            //   transfer_info[index].seal_path = that.data.personal_list[index].seal_path
            // }
            // if (transfer_info[index].signature_path) {
            //   transfer_info[index].signature_path = that.data.personal_list[index].signature_path
            // }

            util.setUserdata('help_transfer_info',transfer_info)
            that.reload()
          } 
        }
      })
    }
  },
  delete_company_identity_card_back_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    if(this.data.personal_list[index].company_identity_card_back_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].company_identity_card_back_path = '';
            transfer_info[index].company_identity_card_back_vc = '';
            util.setUserdata('help_transfer_info',transfer_info)
            that.reload()
          } 
        }
      })
    }
  },
  delete_signature_files: function(e){
    var index = e.currentTarget.dataset.index
    var that = this
    if(this.data.personal_list[index].signature_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].signature_path = '';
            transfer_info[index].signature_vc = '';
            util.setUserdata('help_transfer_info',transfer_info)
            that.reload()
          } 
        }
      })
    }
  },
  delete_seal_files: function (e) {
    // console.log('删除公司印章')
    // return false
    var index = e.currentTarget.dataset.index
    var that = this
    console.log(this.data.personal_list)
    // if (this.data.personal_list[index].seal_path) {
    //   wx.showModal({
    //     title: '删除图片',
    //     content: '是否确认要删除该图片?',
    //     confirmText: "确认",
    //     cancelText: "取消",
    //     success: function (res) {
    //       if (res.confirm) {
    //         var transfer_info = util.getUserArr('transfer_info')
    //         transfer_info[index].seal_path = '';
    //         transfer_info[index].seal_vc = '';
    //         util.setUserdata('transfer_info', transfer_info)
    //         that.reload()
    //       }
    //     }
    //   })
    // }
  },
  createSeal(company_name,index){
    var that = this
    let context = this.data.ctx;

    let w = 150
    let h = 150
    let color = '#E69696'
    let sx = w/2
    let sy = h/2
    let rotato = 0
    let radius = 15

    let fontSize = 16
    let max = 40

    //context.save();  
    context.setFillStyle(color);  
    // context.moveTo(10, 10)
    // context.rect(10, 10, 100, 50)
    // context.lineTo(110, 60)
    // context.stroke()
    // context.draw()
    context.translate(0,0)
    context.beginPath();
    context.arc(sx,sy,65,0,Math.PI*2)
    context.setStrokeStyle(color)
    context.stroke()
    context.save()

    context.translate(sx,sy);//移动坐标原点  
    context.rotate(Math.PI+rotato);//旋转  
    context.beginPath();//创建路径  
    var x = Math.sin(0);  
    var y= Math.cos(0);  
    var dig = Math.PI/5 *4;  
    console.log(dig)
    for(var i = 0;i< 5;i++){//画五角星的五条边  
     var x = Math.sin(i*dig);  
     var y = Math.cos(i*dig);  
     context.lineTo(x*radius,y*radius);  
    }   
    context.closePath();  
    context.stroke();  
    context.fill();  
    context.restore();

    context.translate(sx,sy);
    context.font = '16px STFANGSO'
    context.textBaseline = 'middle';
    context.textAlign = 'center'; 
    context.lineWidth=1
    

    var count = company_name.length;
    if(count>16) fontSize=12;
    if(count>22) fontSize=10;
    if(count>28) fontSize=8;
   // if(count>max)count=max;

    context.setFontSize(fontSize)
    
    var angle = 4*Math.PI/(3*(count - 1));
    var chars = company_name.split("");   
    var c;
    for (var i = 0; i < count; i++){
        c = chars[i];

        if(i==0)
            context.rotate(5*Math.PI/6);
        else
          context.rotate(angle);
        context.save(); 
        context.translate(75-fontSize, 0);  
        context.rotate(Math.PI/2); 
        //context.font = 'bolder 16px SimSun '
        //context.setFontSize(fontSize)
        console.log(c)
        context.fillText(c,0,5);
        context.restore();             
    }

    context.draw()
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: 150,
      height: 150,
      destWidth: 150,
      destHeight: 150,
      canvasId: 'sealCreate',
      success(res) {
        console.log(res)
        console.log(res.tempFilePath)
        var transfer_info = util.getUserArr('help_transfer_info')
        // transfer_info[index].seal_path_name = res.tempFilePath
        wx.uploadFile({
          url: util.reqUrl('V2/uploadSeal'),
          header: util.headers(),
          formData: { 'identity_token': util.getUserdata('identity_token')},
          filePath: res.tempFilePath,
          name: 'company_seal_' + index,
          success(result) {
            var rtn = JSON.parse(result.data)
            console.log(rtn);
            console.log(rtn.media_unid);
            var transfer_info = util.getUserArr('help_transfer_info')
            // console.log('https://gongzheng.ma.cn/Files/Media/fget.html/media_id/' + rtn.FILES.media_unid);
            // transfer_info[index].seal_path = 'https://gongzheng.ma.cn/Files/Media/fget.html/media_id/' + rtn.media_unid

            transfer_info[index].seal_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]
            transfer_info[index].seal_vc = rtn.verify_code
            transfer_info[index].seal_path_name = res.tempFilePath
            
            console.log(transfer_info)
            that.setData({
              personal_list: transfer_info
            })

            util.setUserdata('help_transfer_info', transfer_info)
          }
        })
      }
    })
  },
  company_name_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    transfer_info[index].company_name = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info)
    // this.createSeal(e.detail.value,index)
  },
  social_credit_code_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].social_credit_code = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  license_address_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].license_address = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  company_personal_name_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_personal_name = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  company_identity_code_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_identity_code = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  company_gender_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('transfer_info')
    transfer_info[index].company_gender_input = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('transfer_info',transfer_info);
  },
  company_birth_date_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_birth_date = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  company_address_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_address = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  company_phone_input: function(e){
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_phone = e.detail.value
    this.setData({
      personal_list : transfer_info
    });
    util.setUserdata('help_transfer_info',transfer_info);
  },
  clear_input: function(e){
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    var pageList = that.data.personal_list

    console.log(transfer_info);
    console.log(that.data.personal_list);

    wx.showModal({
      title: '提示',
      content: '确定要清空吗？',
      success: function (sm) {
        if (sm.confirm) {
          for (var i = 0; i < pageList.length; i++) {
            if (i == index && pageList[i].transfer_type == 2) {
              pageList[i].company_identity_card_back_path = '';
              pageList[i].company_identity_card_back_vc = '';
              pageList[i].company_identity_card_front_path = '';
              pageList[i].company_identity_card_front_vc = '';
              pageList[i].verifycode = '';
              pageList[i].company_address = '';
              pageList[i].company_phone = '';
              pageList[i].signature_path = '';
              pageList[i].company_identity_code = '';
              pageList[i].business_license_path = '';
              pageList[i].business_license_vc = '';
              pageList[i].company_name = '';
              pageList[i].company_personal_name = '';
              pageList[i].license_address = '';
              pageList[i].seal_path = '';
              pageList[i].social_credit_code = '';
              pageList[i].company_gender = 0;
              pageList[i].company_birth_date = '';
              pageList[i].seal_path_name = '';
              pageList[i].seal_vc = '';
              pageList[i].transfer_remarkp = '';
               
            }
          }
          that.setData({
            personal_list: pageList
          });

          
          for (var i = 0; i < transfer_info.length; i++) {
            if (i == index && transfer_info[i].transfer_type == 2) {
              transfer_info[i].company_identity_card_back_path = '';
              transfer_info[i].company_identity_card_back_vc = '';
              transfer_info[i].company_identity_card_front_path = '';
              transfer_info[i].company_identity_card_front_vc = '';
              transfer_info[i].verifycode = '';
              transfer_info[i].company_address = '';
              transfer_info[i].company_phone = '';
              transfer_info[i].signature_path = '';
              transfer_info[i].company_identity_code = '';
              transfer_info[i].business_license_path = '';
              transfer_info[i].business_license_vc = '';
              transfer_info[i].company_name = '';
              transfer_info[i].company_personal_name = '';
              transfer_info[i].license_address = '';
              transfer_info[i].seal_path = '';
              transfer_info[i].social_credit_code = '';
              transfer_info[i].company_gender = 0;
              transfer_info[i].company_birth_date = '';
              transfer_info[i].seal_path_name = '';
              transfer_info[i].seal_vc = '';
              transfer_info[i].transfer_remarkp = '';
            }
          }
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })
  },
  reload: function(){
    // type = type || 1
    var transfer_info = util.getUserArr('help_transfer_info')
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 2) {
        var transfer_remarkp = ''
        var num = 0

        if (transfer_info[i].business_license_path) {
          num += 1
        }

        if (transfer_info[i].company_identity_card_front_path) {
          num += 1
        }
        if (transfer_info[i].company_identity_card_back_path) {
          num += 1
        }
        if (transfer_info[i].company_name) {
          transfer_remarkp += '转让人:' + transfer_info[i].company_name + ';'
          num += 1
        }
        if (transfer_info[i].social_credit_code) {
          transfer_remarkp += '社会信用代码:' + transfer_info[i].social_credit_code + ';'
          num += 1
        }
        if (transfer_info[i].license_address) {
          transfer_remarkp += '地址:' + transfer_info[i].license_address + ';'
          num += 1
        }
        if (transfer_info[i].company_personal_name) {
          num += 1
        }
        if (transfer_info[i].company_identity_code) {
          num += 1
        }
        if (transfer_info[i].company_gender) {
          num += 1
        }
        if (transfer_info[i].company_birth_date) {
          num += 1
        }

        if (transfer_info[i].seal_path) {
          num += 1
        }


        console.log(num)
        if (num >= 11) {
          console.log('ok')
          transfer_info[i].isShow = true
          transfer_info[i].transfer_remarkp = transfer_remarkp
        }
      }
    }
    console.log(transfer_info);
    this.setData({
      personal_list : transfer_info
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var pages = getCurrentPages() 
    var currentPage = pages[pages.length - 1] 
    var url = currentPage    //当前页面url
    if (options.orderInfoId) {
      this.setData({
        orderInfoId: options.orderInfoId
      })
    }

    this.reload()
    let ctx = wx.createCanvasContext('sealCreate')
    this.setData({
      ctx: ctx
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  /* onShow: function () {
    this.reload()
  },*/

  /**
  * 生命周期函数--监听页面显示
  */
  onShow: function (options) {
    //代人认证
    // console.log("监听页面显示");
    // console.log(options);
    // if (options) {
    //   options.referrerInfo.extraData.verifyCode; //结果码，除“0”以外，其余都是错误代码
    //   options.referrerInfo.extraData.verifyResult;//认证凭证,32位凭证，提交申请时提供
    //   if (options.referrerInfo.extraData.verifyCode == 0) {
    //     var myindex = util.getUserArr('transfer_info_GetToken_index');
    //     var transfer_info = util.getUserArr('transfer_info')
    //     transfer_info[myindex].id = myindex
    //     this.setData({
    //       personal_list: transfer_info
    //     });
    //     util.setUserdata('transfer_info', transfer_info);
    //     util.setUserdata('transfer_info_GetToken_index', -1);
    //   } else {
    //     wx.showToast({
    //       title: options.referrerInfo.extraData.verifyResult,
    //       icon: 'none',
    //       duration: 1000
    //     });
    //   }
    // } else {
    //   console.log("监听页面没有数据传输显示");
    // }


    this.reload()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  touchTimeStamp: 0
})