const util = require('../../utils/util.js')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    status: false
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../my_notarization/my_notarization',
    })
  },
  bindOtherTap: function (){
    wx.navigateTo({
      url: '../get_notarization_other/get_notarization_other',
    })
  },
  onLoad: function (){
    var that = this;
    util.request('V2/storageList', { 'type': 1, 'identity_type': 1},
          function(res){
            if (res.data.code == 0) {
              that.setData({
                transfer_list: res.data.list
              })
            }
          })
  },
  del: function (e) {
    // 获取当前点击下标    
    var Index = e.currentTarget.dataset.index;
    console.log(Index);
    // var displayArr = this.data.Display;
    // for (let i in displayArr) {
    //   //遍历列表数据      
    //   if (i == Index) {
    //     //根据下标找到目标,改变状态  
    //     if (displayArr[i].status == 0) {
    //       displayArr[i].status = parseInt(displayArr[i].status) + 1
    //     }
    //   }
    // }
    // this.setData({
    //   Display: displayArr
    // })
  },
  openOrClose: function (e) {
    var Index = e.currentTarget.index;
    // console.log(Index);
    this.setData({ status: !this.data.status })
    // if (!this.data.status){
    //   var query = wx.createSelectorQuery();
    //   query.select('.icon-xiajiantou')
    // }
  },

})