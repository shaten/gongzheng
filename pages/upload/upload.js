Page({
  data: {
    tempFilePaths: '',
    hidden: true,
    buthidden: false,
    sourceType: ['album', 'camera'],
    page: ''
  },
  onLoad: function (options) {
    this.setData({
      page: options.page
    })
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
    } catch (e) { }
  },
  frontimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          FilePaths: res.tempFilePaths
        })
      }
    })
  },
  reciteimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          recitePaths: res.tempFilePaths
        })
      }
    })
  },
  upload: function () {
    var _this = this;
    wx.showActionSheet({
      itemList: ['相册', '拍照'],
      success: function (res) {
        // console.log(res.tapIndex)
        if (res.tapIndex == 0) {
          wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['album'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
              // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
              var tempFilePaths = res.tempFilePaths;

              const util = require('../../utils/util.js')
              let FData = {};
              if (_this.data.page == 1) {
                FData.type = 1;
                FData.preInfo = JSON.stringify(wx.getStorageSync('PTransfer'));
              }
              if (_this.data.page == 2) {
                FData.type = 2;
                FData.single = 0;
                FData.preInfo = JSON.stringify(wx.getStorageSync('CTransfer'));
              }
              if (_this.data.page == 3) {
                FData.type = 1;
                FData.preInfo = JSON.stringify(wx.getStorageSync('PAllowed'));
              }
              if (_this.data.page == 4) {
                FData.type = 3;
                FData.preInfo = JSON.stringify(wx.getStorageSync('CAllowed'));
              }
              // console.log(FData.preInfo)
              FData.identity_token = util.getUserdata('identity_token');

              wx.uploadFile({
                url: util.reqUrl('V1/photographed'),
                filePath: tempFilePaths[0],
                name: 'file',
                formData: FData,
                success: function (res) {
                  if (res.data == '-1') {
                    wx.showToast({
                      title: '不是身份证',
                      icon: 'none',
                      duration: 1000
                    })
                    return false;
                  }
                  if (res.data == '-1.1') {
                    wx.showToast({
                      title: '请上传身份证正面',
                      icon: 'none',
                      duration: 1000
                    })
                    return false;
                  }
                  if (res.data == '-2') {
                    wx.showToast({
                      title: '不是营业执照',
                      icon: 'none',
                      duration: 1000
                    })
                    return false;
                  }
                  // console.log('----------------------------')
                  // console.log(res)
                  // var session = wx.getStorageSync('idCardInfo');
                  var data = JSON.parse(res.data);
                  if (_this.data.page == 1) {
                    wx.setStorageSync('PTransfer', data);
                  }
                  if (_this.data.page == 2) {
                    wx.setStorageSync('CTransfer', data);
                  }
                  if (_this.data.page == 3) {
                    wx.setStorageSync('PAllowed', data);
                  }
                  if (_this.data.page == 4) {
                    wx.setStorageSync('CAllowed', data);
                  }
                  // console.log(wx.getStorageSync('CAllowed'))

                  // 操作上一个页面
                  // 页面指针数组
                  var pages = getCurrentPages();
                  // 上一页面指针
                  var prepage = pages[pages.length - 2];
                  if (FData.type == 1) {
                    prepage.setData({
                      personal: data,
                      btn: 1
                    });
                  }
                  else {
                    prepage.setData({
                      companies: data,
                      btn: 1
                    });
                  }
                  // console.log(data)
                  // wx.navigateTo({ url: '../../' + prepage.route })
                  wx.navigateBack({ delta: 1 });
                },
                fail: function (res) {
                  // console.log(res)
                }
              })
              // wx.navigateTo({ url: '../recognition/recognition' })
            },
            fail: function (res) {
              // console.log(res.errMsg)
            }
          })
        }
        else {
          wx.chooseImage({
            count: 1, // 默认9
            sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
            sourceType: ['camera'], // 可以指定来源是相册还是相机，默认二者都有
            success: function (res) {
              // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
              var tempFiles = res.tempFiles;
              // console.log(res)

              const util = require('../../utils/util.js')
              wx.request({
                url: util.reqUrl('V1/photographed'),
                filePath: tempFiles[0],
                success(res) {
                  // console.log(res)
                  var session = wx.getStorageSync('idCardInfo');
                  if (session == '') {
                    wx.setStorageSync('idCardInfo', res)
                  }

                  // 操作上一个页面
                  // 页面指针数组
                  var pages = getCurrentPages();
                  // 上一页面指针
                  var prepage = pages[pages.length - 2];
                  prepage.setData({
                    personal: res.data
                  });
                  wx.navigateTo({ url: '../../' + prepage.route });
                  wx.navigateBack({ delta: 1 });
                },
                fail: function (res) {
                  // console.log(res)
                }
              })
            },
            fail: function (res) {
            }
          })
        }
      },
      fail: function (res) {
        // console.log(res.errMsg)
      }
    })
  }
})