// pages/trading/trading.js
const util = require('../../utils/util.js')

var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    member: null,
    tradList: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    let that = this
    util.request(
      'V2/myAllInfo',
      { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          console.log(res);
          let member_id = res.data.member.id
          util.request(
            'V2/moneyLog',
            {
              member_id: member_id
            },
            function (res) {
              console.log(res.data.list)
              for (let i = 0; i < res.data.list.length;i++) {
                res.data.list[i].obj_createdate = util.toformatTime(res.data.list[i].obj_createdate, 'YMD h:m') 
              }

              that.setData({
                tradList: res.data.list
              })
            },
            function (res) {
              //尝试重启
              wx.reLaunch({
                url: 'pages/index/index'
              })
            },
            function (res) {
              wx.hideToast();
            }
          )
        }
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        wx.hideToast();
      }
    )

    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    let that = this
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})