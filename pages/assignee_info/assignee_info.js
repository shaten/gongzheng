Page({
  personal: function () {
    wx.navigateTo({
      url: './personal/personal'
    })
  },
  companies: function () {
    wx.navigateTo({
      url: './companies/companies'
    })
  },
  onUnload: function () {
    let pa = wx.getStorageSync('PAllowed');
    let ca = wx.getStorageSync('CAllowed');
    let data = new Array();
    if (pa != undefined) {
      for (let key in pa) {
        pa[key].receive_type = 1;
        data.push(pa[key]);
      }
    }
    if (ca != undefined) {
      for (let key in ca) {
        ca[key].receive_type = 2;
        data.push(ca[key]);
      }
    }
    wx.setStorageSync('Allowed', data);
    // console.log(wx.getStorageSync('Allowed'))
  }
})