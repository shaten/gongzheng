const util = require('../../../utils/util.js')
Page({
  data: {
    tempFilePaths: '',
    hidden: true,
    buthidden: false,
    sourceType: ['album', 'camera'],
    temp: 'none',
    companies: [],
    btn: 0
  },
  onLoad: function (options) {
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
    } catch (e) { }
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let _this = this;
    if (this.data.companies.length != 0) {
      wx.showModal({
        title: '是否保存数据',
        success: function (res) {
          // console.log(wx.getStorageSync('CAllowed'))
          if (res.confirm) {
            wx.setStorageSync('CAllowed', _this.data.companies);
          }
          else if (res.cancel) {
            wx.setStorageSync('CAllowed', []);
          }
        }
      })
    }
  },
  frontimage: function () {
    var _this = this;
    var Type = _this.data.sourceType
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: Type,
      success: function (res) {
        _this.setData({
          FilePaths: res.tempFilePaths
        })
      }
    })
  },
  camera: function () {
    wx.navigateTo({
      url: '../../upload/upload?page=4'
    })
  },
  add: function () {
    this.data.companies.push({})
    this.setData({
      companies: this.data.companies
    })
    // console.log(this.data.companies)
  },
  del: function (e) {
    // 获取当前点击下标
    var Index = e.currentTarget.dataset.index;
    var img = new Array();
    img.push(this.data.companies[Index].business_license_path);
    this.data.companies.splice(Index, 1)
    // 刷新数据
    this.setData({
      companies: this.data.companies,
      btn: this.data.companies.length === 0 ? 0 : 1
    });
    wx.setStorageSync('CAllowed', this.data.companies)

    wx.request({
      url: util.reqUrl('V1/deleteImg'),
      method: 'POST',
      data: { "data": img },
      header: util.headers(),
      success(res) {
        if (res.data) {
          wx.showToast({
            title: '删除成功',
            // icon: 'success',
            duration: 1000
          })
        }
      },
      fail: function (res) {
        // console.log(res)
      }
    })
  },





  setCode(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].social_credit_code = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setLicense(e) {
    var Index = e.currentTarget.dataset.index;
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        let FData = {};
        FData.type = 3;
        FData.preInfo = JSON.stringify(wx.getStorageSync('CAllowed'));
        FData.identity_token = util.getUserdata('identity_token');
        wx.uploadFile({
          url: util.reqUrl('V1/photographed'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-2') {
              wx.showToast({
                title: '不是营业执照',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            var data = JSON.parse(res.data);
            wx.setStorageSync('CAllowed', data);
            _this.setData({
              companies: data,
              btn: 1
            })
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      },
    })
  },
  setAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].license_address = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setCorporation(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_personal_name = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setCAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_address = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setPhone(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_phone = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  }
})