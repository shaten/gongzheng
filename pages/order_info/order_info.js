const util = require('../../utils/util.js')
var app = getApp();
Page({
  data: {
    transfer_ohter: '',
    receive_ohter: '',
    loadfiles: '',
    pay_status: false,
    vip_status: null,
    express: false
  },
  onchangeStatus: function () {
    let that = this
    wx.request({
      url: 'https://gongzheng.ma.cn/Lapp/V1/init', //url
      method: 'POST', //请求方式
      header: {
        'Content-Type': 'application/json',
      },
      data: {

      },
      success: function (res) {
        console.log(res)
        let rtn = res.data
        if (rtn.code == 200) {
          that.setData({
            express: rtn.data.express
          })
        }
      },
      fail: function () {
        app.consoleLog("请求数据失败");
      },
      complete: function () {
        // complete 
      }
    })

  },
  copyExpressMsg: function (e) {
    console.log(e)
    let that = this
    let express_msg = e.currentTarget.dataset.express_msg
    
    let expressNumberSplit = express_msg.split('|')
    let expressNumber = expressNumberSplit[1]
    console.log(expressNumber)
    //复制到本地黏贴板
    wx.setClipboardData({
      data: expressNumber,
      success: function (res) {
        wx.hideToast()
        wx.showToast({
          title: '快递单号' + expressNumber+'已成功拷贝到本地,请根据单号查询快递详情',
          icon: 'none',
          duration: 6000
        })
      }
    })
  },
   bindBackTap: function () {
    wx.reLaunch({
      url: '../index/index',
    })
  },
  
   bindOrderPdf:function(e){

     var oneorderInfo = util.getUserdata('oneorderInfo')
     var that = this;
     console.log(oneorderInfo.statement_path)
     
     wx.downloadFile({
       url: oneorderInfo.statement_path,
       success: function (res) {
         console.log(res);
         console.log('下载成功')
         console.log(res.tempFilePath)
         var filePath = res.tempFilePath;
         console.log('文件路径' + filePath)
         that.setData({
           url: filePath,
         });
         wx.openDocument({
           filePath: filePath,
           fileType: 'pdf',
           success: function (res) {
             console.log('打开文档成功')

           },
           fail: function (res) {
             console.log('打开文档失败');
             console.log(res);
           }
         });
       }
     });
     /*
     wx.navigateTo({
       url: '../openpdf/openpdf'
     })
     */
   },
  downloadFile: function(e) {
    console.log(wx.env)
    console.log(e.currentTarget.dataset.id)
    var order_id = e.currentTarget.dataset.id

    wx.showModal({
      title: '提示',
      content: '公证书电子版连接已拷贝到本地,请打开浏览器进行下载,下载连接可分享给他人',
      success: function(result){
        if (result.confirm) {
          wx.setClipboardData({
            data: 'https://gongzheng.ma.cn/Lapp/V2/viewpdf/order_id/' + order_id,
            success: function (res) {
              wx.hideToast()
              wx.showToast({
                title: '公证书电子版连接已拷贝到本地,请打开浏览器进行下载,下载连接可分享给他人',
                icon: 'none',
                duration: 6000
              })
            }
          })

        }
      }
    })
    return false;
    
    wx.setClipboardData({
      data: 'https://gongzheng.ma.cn/Lapp/V2/viewpdf/order_id/' + order_id,
      success: function(res) {
        wx.hideToast()
        wx.showToast({
          title: '公证书电子版已拷贝到本地,请打开浏览器进行下载',
          icon: 'none',
          duration: 6000
        })
      }
    })


    return false;
    var oneorderInfo = util.getUserdata('oneorderInfo')
    var that = this;
    console.log(oneorderInfo.statement_path)
    
    wx.downloadFile({
      url: oneorderInfo.statement_path,
      success: function (res) {
        console.log(res);
        console.log('下载成功')
        console.log(res.tempFilePath)
        var filePath = res.tempFilePath;
        console.log('文件路径' + filePath)

        wx.saveFile({
          tempFilePath: res.tempFilePath,
          success: function(res) {
            console.log(res);
            var savedFilePath = res.savedFilePath;
            console.log('文件已下载到' + savedFilePath);

            wx.openDocument({
              filePath: savedFilePath,
              fileType: 'pdf',
              success: function (res) {
                console.log('打开文档成功')
                console.log(res)
              },
              fail: function (res) {
                console.log('打开文档失败');
                console.log(res);
              }
            });
          }
        })

        


        // wx.saveFile({
        //   tempFilePath: res.tempFilePath,
        //   success: function(res) {
        //     console.log(res);
        //     var savedFilePath = res.savedFilePath;
        //     console.log('文件已下载到' + savedFilePath);

            

        //     // 查看下载的文件列表
        //     wx.getSavedFileList({
        //       success: function (res) {
        //         console.log(res);
        //       }
        //     })
        //     // 打开文档
        //     wx.openDocument({
        //       filePath: savedFilePath,
        //       success: function (res) {
        //         console.log('打开文档成功')
        //       }
        //     })
        //   }
        // })
      }
    });
  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../my_order/my_order',
    })
  },
  choose_statement_files: function (e) {
      var that = this
      if(this.data.statement_path) return;
      wx.chooseImage({
          sizeType: ['original', 'compressed'],
          sourceType: ['album', 'camera'], 
          success: function (res) {
            wx.uploadFile({
              url: util.reqUrl('V2/uploadStatement'),
              header: util.headers(),
              formData: { 
                'identity_token': util.getUserdata('identity_token'),
                'order_id': that.data.order_info.order_id
              },
              filePath: res.tempFilePaths[0],
              name: 'statement',
              success(res) {
                var rtn = JSON.parse(res.data)
              }
            })

            var statement_path = util.getUserArr('statement_path')
            statement_path = res.tempFilePaths
            that.setData({
              statement_path: statement_path
            })
            util.setUserdata('statement_path',statement_path)
          }
      })
  },
  preview_statement_files: function(e){
    if(this.data.statement_path){
      wx.previewImage({
        urls: this.data.statement_path
      })
    }
  },
  touchStart: function(e){
    this.touchTimeStamp = e.timeStamp
  },
  touchEnd: function (e) {
    var diff = e.timeStamp - this.touchTimeStamp
    var tag = e.currentTarget.dataset.tag
    if (tag == 'statement_block'){
      diff >= 500 ? this.delete_statement_files(e) : this.preview_statement_files(e)
    } 
  },
  delete_statement_files: function(e){
    var that = this
    if(this.data.statement_path){
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            // var receive_info = util.getUserArr('receive_info')
            // receive_info[index].identity_card_front_path = '';
            // receive_info[index].identity_card_front_vc = '';
            // util.setUserdata('receive_info',receive_info)
            // that.reload()
          } 
        }
      })
    }
  },
  pay_again: function(e){
    var that = this
    that.setData({
      pay_status: true
    })
    console.log(that.data)
    let other = that.data.share_other
    let brand_info = that.data.share_brand_info
    let transfer_info = that.data.share_transfer_list
    let receive_info = that.data.share_receive_list
    let order_id = that.data.order_id
    let member_id = that.data.order_info.member_id
    // let formId = that.data.order_info.formId
    let formId = that.data.order_info.wx_tmpl_formid;
    let openId = that.data.order_info.wx_openid

    console.log(other)
    console.log(brand_info)
    console.log(transfer_info)
    console.log(receive_info)
    console.log(order_id)

    wx.showLoading({
      title: '订单审核中',
      icon: 'loading',
      duration: 10000,
      mask: true
    })

    util.request('V2/commitOrder2',
      {
        'other': JSON.stringify(other),
        'brand_info': JSON.stringify(brand_info),
        'transfer_info': JSON.stringify(transfer_info),
        'receive_info': JSON.stringify(receive_info),
        'wx_tmpl_formid': formId,
        'wx_openid': openId,
        'order_id': order_id,
        'member_id': member_id,
        'delivery_name': that.data.order_info.delivery_name,
        'delivery_mobile': that.data.order_info.delivery_mobile,
        'delivery_addr': that.data.order_info.delivery_addr,
        'step': 'commit'
        // 'wx_openid': openId,
      },
      function (res) {
        var rtn = res
        console.log(res.data)
        that.setData({
          resMsg: JSON.stringify(res.data)
        })

        wx.hideLoading()
        if (rtn.data.code == 0) {
          //code=>1,微信支付
          console.log('wx.requestPayment')

          console.log(that.data.order_info.commit_time)
          wx.requestPayment({
            timeStamp: res.data.timeStamp,
            nonceStr: res.data.nonceStr,
            package: res.data.package,
            signType: 'MD5',
            paySign: res.data.paySign,
            success(res) {
              console.log('支付成功')
              console.log(res)
              wx.redirectTo({
                url: '../pay_msg/pay_msg?id=' + rtn.data.order_id + '&s=1',
              })
            },
            fail(res) {
              console.log('支付失败')
              console.log(res)
              wx.redirectTo({
                url: '../pay_msg/pay_msg?id=' + rtn.data.order_id + '&s=0',
              })
            }
          })


        } else if (rtn.data.code == 1) {
          //扣余额成功
          wx.showToast({
            title: '提交成功,请耐心等待审核结果',
            icon: 'none',
            duration: 2000
          })
          wx.redirectTo({
            url: '../my_order/my_order?status=2,4,6'
          })

        } else if (rtn.data.code < 0) {
          wx.showToast({
            title: rtn.data.msg,
            icon: 'none',
            duration: 2000
          })
          return false
        } else {
          wx.showModal({
            content: rtn.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
              }
            }
          })
        }

      }
    )

    
  },
  bindOrderInfoTap: function (){
    wx.navigateTo({
      url: '../order_info/order_info',
    })
  },
  bindOrderStatusChange: function(e) {
        console.log('picker account 发生选择改变，携带值为', e.detail.value);

        this.setData({
            orderStatusIndex: e.detail.value
        })
  },
  statement_mail: function(e){
    var index = e.currentTarget.dataset.index;
    var user_email = util.getUserdata('user_email')
    user_email = e.detail.value
    this.setData({
      statement_mail : user_email
    });
    util.setUserdata('user_email',user_email);
  },
  send_statement: function(){
    var that = this;
    util.request('V2/sendDocByMail',
      {
        'order_id': that.data.order_info.order_id,
        'mail': util.getUserdata('user_email')||''
      },
      function(res){
        if(res.data.code == 0){
          console.log('OK')
        }
      })
  },
  download_statement:function(){
    var that = this;
    console.log('download_statement')
    wx.downloadFile({
      url: util.reqUrl('V2/docDownload', true, { order_id: that.data.order_info.order_id}),
      header: util.headers(),
      success(res){
          console.log(res)
          wx.saveFile({
            tempFilePath: res.tempFilePath,
            success(res){
              console.log(res)
            }
          })
      }
    })
  },
  onLoad: function (opt){
    var that = this;
    that.onchangeStatus()
    wx.showToast({ title: '加载中', icon: 'loading', duration: 10000 });
    util.request(
    'V2/myAllInfo',
    { 'type': 1 },
    function (datas) {
      if (datas.data.code == 0) {
        console.log(datas);
        let is_vip = datas.data.member.is_vip

        //订单信息
        util.request('V2/orderInfo',
          {
            'order_id': opt.id
          },
          function (res) {
            if (res.data.code == 0) {
              console.log('LLWWWFF')
              console.log(res.data.order_info);
              var transfer_ohter = ''
              for (var i = 0; i < res.data.order_info.transfer_list.length; i++) {
                if (res.data.order_info.transfer_list[i].transfer_id != res.data.order_info.main_transfer.transfer_id) {
                  console.log('存在公司' + res.data.order_info.main_transfer.company_name)
                  console.log('存在个人' + res.data.order_info.main_transfer.personal_name)

                  if (res.data.order_info.transfer_list[i].transfer_type == 1) {
                    transfer_ohter += '个人(' + res.data.order_info.transfer_list[i].personal_name + ');'
                  }
                  if (res.data.order_info.transfer_list[i].transfer_type == 2) {
                    transfer_ohter += '公司(' + res.data.order_info.transfer_list[i].company_name + ');'
                  }
                }
              }
              console.log(transfer_ohter)

              var receive_ohter = ''
              for (var i = 0; i < res.data.order_info.receive_list.length; i++) {
                if (res.data.order_info.receive_list[i].receive_id != res.data.order_info.main_receive.receive_id) {
                  console.log('存在公司' + res.data.order_info.main_receive.company_name)
                  console.log('存在个人' + res.data.order_info.main_receive.personal_name)

                  if (res.data.order_info.receive_list[i].receive_type == 1) {
                    receive_ohter += '个人(' + res.data.order_info.receive_list[i].personal_name + ');'
                  }
                  if (res.data.order_info.receive_list[i].receive_type == 2) {
                    receive_ohter += '公司(' + res.data.order_info.receive_list[i].company_name + ');'
                  }
                }
              }

              var page_money = ''
              var page_number = 0
              var page_count_money = 0
              if (res.data.order_info.page_count > 2) {
                // page_number = other.page_count - 2
                var page_number = res.data.order_info.page_count - 2
                page_money = '20*' + page_number
              } else {
                page_money = '0'
              }

              let base_money = 350
              //会员优惠价格
              let vip_money = 0
              if (is_vip > 0) {
                // vip_money = base_money - 70
                // base_money = base_money - 70
                vip_money = base_money - 102
                base_money = base_money - 102
              } else {
                vip_money = base_money
              }

              if (is_vip == 1) {
                page_count_money = page_number * 20
                var other_money = 0
                if (res.data.order_info.mail_type == "1") other_money += 0;
                if (res.data.order_info.send_type == "1") other_money += 0;
                if (res.data.order_info.care_type == "1") other_money += 50;
              }else{
                page_count_money = page_number * 20
                var other_money = 0
                if (res.data.order_info.mail_type == "1") other_money += 100;
                if (res.data.order_info.send_type == "1") other_money += 50;
                if (res.data.order_info.care_type == "1") other_money += 50;
              }
              

              var all_money = 0
              all_money = base_money + other_money + page_count_money

              console.log(receive_ohter)

              let share_transfer_list = res.data.order_info.transfer_list
              let share_receive_list = res.data.order_info.receive_list

              let share_brand_info = {}
              share_brand_info.address = res.data.order_info.address
              let brand_files = []
              let brand_vc = []
              for (var i = 0; i < res.data.order_info.brand_image_list.length; i++) {
                brand_files.push('https://gongzheng.ma.cn/MYPublic/' + res.data.order_info.brand_image_list[i].image_path)
                brand_vc.push(res.data.order_info.brand_image_list[i].brand_vc)
              }
              console.log(brand_files)
              share_brand_info.brand_files = brand_files
              share_brand_info.brand_vc = brand_vc

              let brand_other_files = []
              let brand_other_vc = []
              for (var k = 0; k < res.data.order_info.brand_other_image_list.length; k++) {
                brand_other_files.push('https://gongzheng.ma.cn/MYPublic/' + res.data.order_info.brand_other_image_list[k].image_path)
                brand_other_vc.push(res.data.order_info.brand_other_image_list[k].brand_other_vc)
              }
              share_brand_info.brand_other_files = brand_other_files
              share_brand_info.brand_other_vc = brand_other_vc

              share_brand_info.brand_register_code = res.data.order_info.brand_register_code
              share_brand_info.brand_name = res.data.order_info.brand_name
              share_brand_info.other_possessor = res.data.order_info.other_possessor
              share_brand_info.possessor_name = res.data.order_info.possessor_name
              share_brand_info.type_num = res.data.order_info.type_num
              share_brand_info.validity = res.data.order_info.validity

              let share_other = {}
              share_other.care_type = res.data.order_info.care_type == 0 ? false : true
              share_other.comm_type = res.data.order_info.comm_type == 0 ? false : true
              share_other.mail_type = res.data.order_info.mail_type == 0 ? false : true
              share_other.page_count = res.data.order_info.page_count
              share_other.remark = res.data.order_info.remark

              //提交
              console.log('订单信息')
              console.log(res.data);
              console.log(share_transfer_list)
              console.log(share_receive_list)
              console.log(share_brand_info)
              console.log(share_other)


              util.setUserdata('oneorderInfo', res.data.order_info)
              that.setData({
                share_transfer_list: share_transfer_list,
                share_receive_list: share_receive_list,
                share_brand_info: share_brand_info,
                share_other: share_other,
                formId: res.data.order_info.wx_tmpl_formid,
                order_id: res.data.order_info.order_id,

                transfer_ohter: transfer_ohter,
                receive_ohter: receive_ohter,
                order_info: res.data.order_info,
                page_money: page_money,
                other_money: other_money,
                all_money: all_money,
                vip_money: vip_money,
                vip_status: is_vip
              })
            }
          },
          function (res) {
            //尝试重启
            wx.reLaunch({
              url: 'pages/index/index'
            })
          },
          function (res) {
            wx.hideToast();
          })

      }
    },
    function (res) {
      //尝试重启
      wx.reLaunch({
        url: 'pages/index/index'
      })
    },
    function (res) {
      wx.hideToast();
    })
    // console.log(is_vip.member)
    // setTimeout(function () { 
    //   util.request('V2/orderInfo',
    //     {
    //       'order_id': opt.id
    //     },
    //     function (res) {
    //       if (res.data.code == 0) {
    //         console.log(res.data.order_info);
    //         var transfer_ohter = ''
    //         for (var i = 0; i < res.data.order_info.transfer_list.length; i++) {
    //           if (res.data.order_info.transfer_list[i].transfer_id != res.data.order_info.main_transfer.transfer_id) {
    //             console.log('存在公司' + res.data.order_info.main_transfer.company_name)
    //             console.log('存在个人' + res.data.order_info.main_transfer.personal_name)

    //             if (res.data.order_info.transfer_list[i].transfer_type == 1) {
    //               transfer_ohter += '个人(' + res.data.order_info.transfer_list[i].personal_name + ');'
    //             }
    //             if (res.data.order_info.transfer_list[i].transfer_type == 2) {
    //               transfer_ohter += '公司(' + res.data.order_info.transfer_list[i].company_name + ');'
    //             }
    //           }
    //         }
    //         console.log(transfer_ohter)

    //         var receive_ohter = ''
    //         for (var i = 0; i < res.data.order_info.receive_list.length; i++) {
    //           if (res.data.order_info.receive_list[i].receive_id != res.data.order_info.main_receive.receive_id) {
    //             console.log('存在公司' + res.data.order_info.main_receive.company_name)
    //             console.log('存在个人' + res.data.order_info.main_receive.personal_name)

    //             if (res.data.order_info.receive_list[i].receive_type == 1) {
    //               receive_ohter += '个人(' + res.data.order_info.receive_list[i].personal_name + ');'
    //             }
    //             if (res.data.order_info.receive_list[i].receive_type == 2) {
    //               receive_ohter += '公司(' + res.data.order_info.receive_list[i].company_name + ');'
    //             }
    //           }
    //         }

    //         var page_money = ''
    //         var page_number = 0
    //         var page_count_money = 0
    //         if (res.data.order_info.page_count > 2) {
    //           page_number = other.page_count - 2
    //           var page_number = res.data.order_info.page_count - 2
    //           page_money = '20*' + page_number
    //         } else {
    //           page_money = '0'
    //         }

    //         let base_money = 350
    //         //会员优惠价格
    //         let vip_money = 0
    //         if (app.globalData.member.is_vip > 0) {
    //           vip_money = base_money - 70
    //           base_money = base_money - 70
    //         } else {
    //           vip_money = base_money
    //         }

    //         page_count_money = page_number * 20
    //         var other_money = 0
    //         if (res.data.order_info.mail_type == "1") other_money += 100;
    //         if (res.data.order_info.send_type == "1") other_money += 50;
    //         if (res.data.order_info.care_type == "1") other_money += 50;

    //         var all_money = 0
    //         all_money = base_money + other_money + page_count_money

    //         console.log(receive_ohter)

    //         let share_transfer_list = res.data.order_info.transfer_list
    //         let share_receive_list = res.data.order_info.receive_list

    //         let share_brand_info = {}
    //         share_brand_info.address = res.data.order_info.address
    //         let brand_files = []
    //         let brand_vc = []
    //         for (var i = 0; i < res.data.order_info.brand_image_list.length; i++) {
    //           brand_files.push('https://gongzheng.ma.cn/MYPublic/' + res.data.order_info.brand_image_list[i].image_path)
    //           brand_vc.push(res.data.order_info.brand_image_list[i].brand_vc)
    //         }
    //         console.log(brand_files)
    //         share_brand_info.brand_files = brand_files
    //         share_brand_info.brand_vc = brand_vc

    //         let brand_other_files = []
    //         let brand_other_vc = []
    //         for (var k = 0; k < res.data.order_info.brand_other_image_list.length; k++) {
    //           brand_other_files.push('https://gongzheng.ma.cn/MYPublic/' + res.data.order_info.brand_other_image_list[k].image_path)
    //           brand_other_vc.push(res.data.order_info.brand_other_image_list[k].brand_other_vc)
    //         }
    //         share_brand_info.brand_other_files = brand_other_files
    //         share_brand_info.brand_other_vc = brand_other_vc

    //         share_brand_info.brand_register_code = res.data.order_info.brand_register_code
    //         share_brand_info.brand_name = res.data.order_info.brand_name
    //         share_brand_info.other_possessor = res.data.order_info.other_possessor
    //         share_brand_info.possessor_name = res.data.order_info.possessor_name
    //         share_brand_info.type_num = res.data.order_info.type_num
    //         share_brand_info.validity = res.data.order_info.validity

    //         let share_other = {}
    //         share_other.care_type = res.data.order_info.care_type == 0 ? false : true
    //         share_other.comm_type = res.data.order_info.comm_type == 0 ? false : true
    //         share_other.mail_type = res.data.order_info.mail_type == 0 ? false : true
    //         share_other.page_count = res.data.order_info.page_count
    //         share_other.remark = res.data.order_info.remark

    //         //提交
    //         console.log('订单信息')
    //         console.log(res.data);
    //         console.log(share_transfer_list)
    //         console.log(share_receive_list)
    //         console.log(share_brand_info)
    //         console.log(share_other)

            
    //         util.setUserdata('oneorderInfo', res.data.order_info)
    //         that.setData({
    //           share_transfer_list: share_transfer_list,
    //           share_receive_list: share_receive_list,
    //           share_brand_info: share_brand_info,
    //           share_other: share_other,
    //           formId: res.data.order_info.wx_tmpl_formid,

    //           transfer_ohter: transfer_ohter,
    //           receive_ohter: receive_ohter,
    //           order_info: res.data.order_info,
    //           page_money: page_money,
    //           other_money: other_money,
    //           all_money: all_money,
    //           vip_money: vip_money,
    //           vip_status: app.globalData.member.is_vip
    //         })
    //       }
    //     },
    //     function (res) {
    //       //尝试重启
    //       wx.reLaunch({
    //         url: 'pages/index/index'
    //       })
    //     },
    //     function (res) {
    //       wx.hideToast();
    //     })
    // }, 1000)
    

    var timestamp = parseInt((new Date()).valueOf() / 1000);
    console.log('当前时间戳==' + timestamp);
    that.setData({
      timestamp: timestamp
    })
  }, 
  touchTimeStamp: 0
})