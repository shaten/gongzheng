const util = require('../../utils/util.js')
Page({
  data: {
    page_count: '2',
    remark: '',
    mail_type: false,
    care_type: false,
    send_type: false,
    comm_type:false,
    mail_type_status: false,
    care_type_status: false,
    send_type_status: false,
    comm_type_status: false,
    input_status: false
  },  
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../get_notarization/get_notarization',
    })
  },
  bindOtherTap: function () {
    wx.navigateTo({
      url: '../get_notarization_other/get_notarization_other',
    })
  },
  page_countadd:function()
  {
    var order_id = util.getUserArr('order_id')
    console.log(order_id)
    if (order_id > 0) {
      return false;
    }
    var other = util.getUserdata('other');
    console.log(other);
    other.page_count = parseInt(other.page_count)
    var page_count = other.page_count+1;
    
    console.log(page_count);
    this.setData({
      page_count: page_count
    });
    other.page_count = page_count;
    util.setUserdata('other', other);
  },
  page_countlost: function () {
    var order_id = util.getUserArr('order_id')
    console.log(order_id)
    if(order_id > 0) {
      return false;
    }
    var other = util.getUserdata('other');
    console.log(other.page_count);
    var page_count =2;
    if (other.page_count>2){
      page_count = other.page_count-1;
    } 
    console.log(page_count);
    this.setData({
      page_count: page_count
    });

    other.page_count = page_count;
    util.setUserdata('other', other);

  },
  countInput: function (e){
    // var order_id = util.getUserArr('order_id')
    // console.log(order_id)
    // if (order_id > 0) {
    //   return false;
    // }
    console.log(e.detail.value)
    var other = util.getUserdata('other');
    other.page_count = e.detail.value
    console.log(other)
    console.log(isNaN(parseInt(other.page_count)))
    if (isNaN(parseInt(other.page_count)) == true || parseInt(other.page_count) < 2) {
      other.page_count = 2
    }
    
    // return false
    this.setData({
      page_count: other.page_count
    });
    util.setUserdata('other',other);
  },
  remarkInput: function (e){
    var other = util.getUserdata('other');
    other.remark = e.detail.value
    console.log(other)
    this.setData({
      remark: other.remark
    });
    util.setUserdata('other',other);
  },
  mailTypeChange: function(e){
    var other = util.getUserdata('other');
    console.log(other)
    other.mail_type = e.detail.value
    console.log(other)
    this.setData({
      mail_type: other.mail_type
    });
    util.setUserdata('other',other);
  },
  sendTypeChange: function(e){
    var other = util.getUserdata('other');
    other.send_type = e.detail.value
    console.log(other)
    this.setData({
      send_type: other.send_type
    });
    util.setUserdata('other',other);
  },
  careTypeChange: function(e){
    var other = util.getUserdata('other');
    other.care_type = e.detail.value
    console.log(other)
    this.setData({
      care_type: other.care_type
    });
    util.setUserdata('other',other);
  },
  commTypeChange: function (e) {
    var other = util.getUserdata('other');
    other.comm_type = e.detail.value
    console.log(other)
    this.setData({
      comm_type: other.comm_type
    });
    util.setUserdata('other', other);
  },
  onLoad: function () {
    //载入缓存数据
    var other = util.getUserdata('other');
    var order_id = util.getUserArr('order_id')
    console.log(other)
    console.log(order_id)
    if (order_id > 0) {
      this.setData({
        page_count: other.page_count,
        mail_type: other.mail_type || false,
        send_type: other.send_type || false,
        care_type: other.care_type || false,
        comm_type: other.comm_type || false,

        mail_type_status: true,
        send_type_status: true,
        care_type_status: true,
        // comm_type_status: true,
        input_status: true
      })
    }else{
      console.log(other.page_count)
      this.setData({
        page_count: other.page_count || 2,
        remark: other.remark || '',
        mail_type: other.mail_type || false,
        send_type: other.send_type || false,
        care_type: other.care_type || false,
        comm_type: other.comm_type || false
      });
    }

    if (!other.page_count) other.page_count = 2

    util.setUserdata('other', other);
    

  }

})