const util = require('../../utils/util.js')
// pages/useremail/useremail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var member = util.getUserdata('member')
    console.log(member);
    var buttionnane="立即绑定";
    if (member.email){
      buttionnane = "重新绑定";
    }
    this.setData({
      member: member,
      buttionnane:buttionnane
    })
  },
  bindGetNotarizationTap:function()
  {
    var that = this;

    var member = util.getUserdata('member')

    if (member.email == '' || !member.email || util.checkMail(member.email) == false) {
      wx.showToast({
        title: '请输入邮箱地址！',
        icon: 'none',
        duration: 2000
      });
      return;
    }

    util.request('V2/updatemyinfo', { 'email': member.email },
      function (res) {
        console.log(res);
        if (res.data.code == 0) {
        //  console.log(res);
          wx.showToast({
            title: '操作成功！',
            icon: 'none',
            duration: 2000
          });
          wx.navigateTo({
            url: '../my_notarization/my_notarization',
          })
          // wx.navigateBack({ changed: true });//返回上一页  
          /*
          wx.navigateTo({
            url: '../my_notarization/my_notarization',
          }) */
        }else{
          wx.showToast({
            title: '操作失败，请重试！',
            icon: 'none',
            duration: 2000
          });
        }
      })

  },
  personal_email_input: function (e) {
    var email=e.detail.value;
    
    util.setUserdata('email', email);

    var member = util.getUserdata('member')
    member.email = email;

    util.setUserdata('member', member);

    console.log(util.getUserdata('email'));
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})