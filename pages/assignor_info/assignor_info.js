Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },

  personal: function () {
    wx.navigateTo({
      url: './personal/personal'
    })
  },
  companies: function () {
    wx.navigateTo({
      url: './companies/companies'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let pt = wx.getStorageSync('PTransfer');
    let ct = wx.getStorageSync('CTransfer');
    let data = new Array();
    if (pt != undefined) {
      for (let key in pt) {
        pt[key].transfer_type = 1;
        data.push(pt[key]);
      }
    }
    if (ct != undefined) {
      for (let key in ct) {
        ct[key].transfer_type = 2;
        data.push(ct[key]);
      }
    }
    wx.setStorageSync('Transfer', data);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  }
})