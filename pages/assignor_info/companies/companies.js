const util = require('../../../utils/util.js')

Page({
  data: {
    hidden: true,
    buthidden: false,
    companies: [],
    btn: 0
  },
  onLoad: function (options) {
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
    } catch (e) { }
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let _this = this;
    if (this.data.companies.length != 0) {
      wx.showModal({
        title: '是否保存数据',
        success: function (res) {
          if (res.confirm) {
            wx.setStorageSync('CTransfer', _this.data.companies);
          }
          else if (res.cancel) {
            wx.setStorageSync('CTransfer', []);
          }
        }
      })
    }
  },
  frontimage: function () {
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;

        let FData = {};
        FData.type = 2;
        FData.single = 1;
        FData.preInfo = JSON.stringify(wx.getStorageSync('CTransfer'));
        FData.identity_token = util.getUserdata('identity_token');

        wx.uploadFile({
          url: util.reqUrl('V1/photographed'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-1') {
              wx.showToast({
                title: '不是身份证',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            if (res.data == '-1.1') {
              wx.showToast({
                title: '请上传身份证正面',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            
            var data = JSON.parse(res.data);
            wx.setStorageSync('CTransfer', data);

            _this.setData({
              companies: data,
              btn: 1
            });
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      }
    })
  },
  reciteimage: function (e) {
    var _this = this;
    var Index = e.currentTarget.dataset.index;
    var idCardFrontSide = this.data.companies[Index].company_identity_card_front_path;
    if (idCardFrontSide == undefined) {
      wx.showToast({
        title: '请先上传身份证正面',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;

        let FData = {};
        FData.identity_token = util.getUserdata('identity_token');

        wx.uploadFile({
          url: util.reqUrl('V1/uploadIdCardBackSide'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-1') {
              wx.showToast({
                title: '不是身份证',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            if (res.data == '-1.1') {
              wx.showToast({
                title: '请上传身份证反面',
                icon: 'none',
                duration: 1000
              })
              return false;
            }

            let data = _this.data.companies[Index];
            data.company_identity_card_back_path = res.data;
            wx.setStorageSync('CTransfer', _this.data.companies);
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      }
    })
  },
  camera: function () {
    wx.showActionSheet({
      itemList: ['拍照', '历史记录'],
      success: function (res) {
        // console.log(res.tapIndex)
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: '../../upload/upload?page=2'
          })
        }
        else {
        }
      },
      fail: function (res) {
        // console.log(res.errMsg)
      }
    })
  },
  add: function () {
    this.data.companies.push({})
    this.setData({
      companies: this.data.companies
    })
  },
  del: function (e) {
    // 获取当前点击下标
    var Index = e.currentTarget.dataset.index;
    var img = new Array();
    img.push(this.data.companies[Index].business_license_path);
    img.push(this.data.companies[Index].company_identity_card_front_path);
    img.push(this.data.companies[Index].company_identity_card_back_path);
    this.data.companies.splice(Index, 1)
    // 刷新数据
    this.setData({
      companies: this.data.companies,
      btn: this.data.companies.length === 0 ? 0 : 1
    });
    wx.setStorageSync('CTransfer', this.data.companies)

    const _this = this;
    wx.request({
      url: util.reqUrl('V1/deleteImg'),
      method: 'POST',
      data: { "data": img },
      header: util.headers(),
      success(res) {
        if (res.data) {
          wx.showToast({
            title: '删除成功',
            // icon: 'success',
            duration: 1000
          })
        }
      },
      fail: function (res) {
        // console.log(res)
      }
    })
  },
  face_recognition: function () {
    wx.navigateTo({
      url: '../../face/face'
    })
  },

  
  formSubmit(e) {
    // console.log('form发生了submit事件，携带数据为：', e.detail.value)
  },

  formReset(e) {
    // console.log('form发生了reset事件，携带数据为：', e.detail.value)
    this.setData({
      chosen: ''
    })
  },





  setCompany(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_name = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setCode(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].social_credit_code = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setLicense(e) {
    var Index = e.currentTarget.dataset.index;
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        let FData = {};
        FData.type = 3;
        FData.preInfo = JSON.stringify(wx.getStorageSync('CTransfer'));
        FData.identity_token = util.getUserdata('identity_token');
        wx.uploadFile({
          url: util.reqUrl('V1/photographed'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-2') {
              wx.showToast({
                title: '不是营业执照',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            var data = JSON.parse(res.data);
            wx.setStorageSync('CTransfer', data);
            _this.setData({
              companies: data,
              btn: 1
            })
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      },
    })
  },
  setAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].license_address = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setCorporation(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_personal_name = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setCAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_address = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  },
  setPhone(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.companies;
    data[Index].company_phone = e.detail.value;
    wx.setStorageSync('CAllowed', this.data.companies)
  }
})