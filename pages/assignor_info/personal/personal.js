const util = require('../../../utils/util.js')
Page({
  data: {
    hidden: true,
    temp: 'none',
    personal: [],
    btn: 0,
    date: new Date().toLocaleDateString(),
  },
  onLoad: function (options) {
    try {
      var res = wx.getSystemInfoSync();
      var platform = res.platform
      if (platform == 'ios') {
        util.msg("警告", "IOS系统暂不支持拍照，请从相册选择照片")
        this.setData({
          sourceType: ['album']
        })
      }
    } catch (e) { }
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let _this = this;
    if (this.data.personal.length != 0) {
      wx.showModal({
        title: '提示',
        content: '是否保存数据？',
        success: function (res) {
          // console.log(wx.getStorageSync('PTransfer'))
          if (res.confirm) {
            wx.setStorageSync('PTransfer', _this.data.personal);
          }
          else if (res.cancel) {
            wx.setStorageSync('PTransfer', []);
          }
        }
      })
    }
  },
  frontimage: function () {
    var _this = this;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;
        let FData = {};
        FData.type = 1;
        FData.preInfo = JSON.stringify(wx.getStorageSync('PTransfer'));
        FData.identity_token = util.getUserdata('identity_token');
        wx.uploadFile({
          url: util.reqUrl('V1/photographed'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-1') {
              wx.showToast({
                title: '不是身份证',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            if (res.data == '-1.1') {
              wx.showToast({
                title: '请上传身份证正面',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            var data = JSON.parse(res.data);
            wx.setStorageSync('PTransfer', data);
            _this.setData({
              personal: data,
              btn: 1
            })
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      }
    })
  },
  reciteimage: function (e) {
    var _this = this;
    var Index = e.currentTarget.dataset.index;
    var idCardFrontSide = this.data.personal[Index].identity_card_front_path;
    if (idCardFrontSide == undefined) {
      wx.showToast({
        title: '请先上传身份证正面',
        icon: 'none',
        duration: 1000
      })
      return false;
    }
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (res) {
        var tempFilePaths = res.tempFilePaths;

        let FData = {};
        FData.identity_token = util.getUserdata('identity_token');

        wx.uploadFile({
          url: util.reqUrl('V1/uploadIdCardBackSide'),
          filePath: tempFilePaths[0],
          name: 'file',
          formData: FData,
          success: function (res) {
            if (res.data == '-1') {
              wx.showToast({
                title: '不是身份证',
                icon: 'none',
                duration: 1000
              })
              return false;
            }
            if (res.data == '-1.1') {
              wx.showToast({
                title: '请上传身份证反面',
                icon: 'none',
                duration: 1000
              })
              return false;
            }

            let data = _this.data.personal[Index];
            data.identity_card_back_path = res.data;
            wx.setStorageSync('PTransfer', _this.data.personal);
          },
          fail: function (res) {
            // console.log(res)
          }
        })
      }
    })
  },
  camera: function () {
    wx.showActionSheet({
      itemList: ['拍照', '历史记录'],
      success: function (res) {
        // console.log(res.tapIndex)
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: '../../upload/upload?page=1'
          })
        }
        else {
          wx.navigateTo({
            url: '../../certified_personal/certified_personal'
          })
        }
      },
      fail: function (res) {
        // console.log(res.errMsg)
      }
    })
  },
  add: function () {
    this.data.personal.push({})
    this.setData({
      personal: this.data.personal
    })
  },
  del: function (e) {
    var Index = e.currentTarget.dataset.index;
    var img = new Array();
    img.push(this.data.personal[Index].identity_card_front_path);
    img.push(this.data.personal[Index].identity_card_back_path);
    console.log(this.data.personal);
    this.data.personal.splice(Index, 1)
    // 刷新数据
    this.setData({
      personal: this.data.personal,
      btn: this.data.personal.length === 0 ? 0 : 1
    });
    // console.log(this.data.personal)
    wx.setStorageSync('PTransfer', this.data.personal)

    wx.request({
      url: util.reqUrl('V1/deleteImg'),
      method: 'POST',
      data: { "data": img },
      header: util.headers(),
      success(res) {
        if (res.data) {
          wx.showToast({
            title: '删除成功',
            // icon: 'success',
            duration: 1000
          })
        }
      },
      fail: function (res) {
        // console.log(res)
      }
    })
  },











  bindPickerChange: function (e) {
    // console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      index: e.detail.value
    })
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },



  onShareAppMessage() {
    return {
      title: 'form',
      path: 'pages/assignee_info/personal/personal'
    }
  },

  formSubmit(e) {
    // console.log('form发生了submit事件，携带数据为：', e)
  },

  formReset(e) {
    // console.log('form发生了reset事件，携带数据为：', e.detail.value)
    this.setData({
      chosen: ''
    })
  },
  setName(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.personal;
    data[Index].personal_name = e.detail.value;
    wx.setStorageSync('PTransfer', this.data.personal)
  },
  setId(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.personal;
    data[Index].identity_code = e.detail.value;
    wx.setStorageSync('PTransfer', this.data.personal)
  },
  setAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.personal;
    data[Index].identity_address = e.detail.value;
    wx.setStorageSync('PTransfer', this.data.personal)
  },
  setCAddress(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.personal;
    data[Index].personal_address = e.detail.value;
    wx.setStorageSync('PTransfer', this.data.personal)
  },
  setPhone(e) {
    var Index = e.currentTarget.dataset.index;
    var data = this.data.personal;
    data[Index].personal_phone = e.detail.value;
    wx.setStorageSync('PTransfer', this.data.personal)
  }
})