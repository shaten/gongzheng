const util = require('../../utils/util.js')
// pages/openpdf/openpdf.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  var oneorderInfo = util.getUserdata('oneorderInfo')
  var that=this;
    wx.downloadFile({
      url: oneorderInfo.statement_path,
      success: function (res) {
        console.log('下载成功')
        var filePath = res.tempFilePath;
        console.log('文件路径' + filePath)
        that.setData({
          url: filePath,
        });
        wx.openDocument({
          filePath: filePath,
          fileType: 'pdf',
          success: function (res) {
            console.log('打开文档成功')

          },
          fail:function(res){
            console.log('打开文档失败');
            console.log(res);
          }
        });
      }
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})