// pages/transfer_info_overseas/transfer_info_overseas.js
const util = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据1
   */
  data: {
    genderType: ["点击选择性别", "男", "女"],
    personal_list: [],
    genderTypeIndex: 0,
    cWidth: 0,
    cHeight: 0
  },
  go_select: function (e) {
    wx.navigateTo({
      url: '../help_transfer_info_select/help_transfer_info_select?type=3&identity_type=1',
      // url: '../transfer_info_select/transfer_info_select?type=3',
    })
  },
  buildSeal: function (e) {
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('transfer_info')
    var company_name = transfer_info[index].company_name
    console.log(company_name)
    if (company_name) {
      this.createSeal(company_name, index)
    } else {
      wx.showToast({
        title: '公司名不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }

  },
  reload: function(){
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info)
    for (var i = 0; i < transfer_info.length; i++) {
      if (transfer_info[i].transfer_type == 3) {
        var transfer_remarkp = ''
        var num = 0

        if (transfer_info[i].overseas_license_path) {
          num += 1
        }

        if (transfer_info[i].overseas_identity_card_front_path) {
          num += 1
        }
        if (transfer_info[i].choose_overseas_identity_card_back_files) {
          num += 1
        }
        // if (transfer_info[i].overseas_other_files.length > 1) {
        //   num += 1
        // }
        if (transfer_info[i].company_name) {
          transfer_remarkp += '公司名:' + transfer_info[i].company_name + ';'
          num += 1
        }
        if (transfer_info[i].social_credit_code) {
          transfer_remarkp += '公司注册编号:' + transfer_info[i].social_credit_code + ';'
          num += 1
        }
        if (transfer_info[i].company_personal_name) {
          num += 1
        }
        if (transfer_info[i].company_identity_code) {
          num += 1
        }
        if (transfer_info[i].company_gender) {
          num += 1
        }
        if (transfer_info[i].company_birth_date) {
          num += 1
        }


        if (transfer_info[i].seal_path) {
          num += 1
        }


        console.log(num)
        //后面改11
        if (num >= 11) {
          console.log('ok')
          transfer_info[i].isShow = true
          transfer_info[i].transfer_remarkp = transfer_remarkp
        }
      }
    }

    this.setData({
      personal_list: transfer_info
    });
  },
  company_name_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    transfer_info[index].company_name = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info)
    // this.createSeal(e.detail.value, index)
  },
  social_credit_code_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].social_credit_code = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  company_personal_name_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_personal_name = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  company_identity_code_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_identity_code = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  genderChange: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_gender = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info)
  },
  company_birth_date_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_birth_date = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  company_address_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].license_address = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  company_phone_input: function (e) {
    var index = e.currentTarget.dataset.index;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].company_phone = e.detail.value
    this.setData({
      personal_list: transfer_info
    });
    util.setUserdata('help_transfer_info', transfer_info);
  },
  choose_signature_files(e) {
    let that = this
    let index = e.currentTarget.dataset.index
    if (this.data.personal_list[index].signature_path) return;
    wx.navigateTo({
      url: '../help_signature/help_signature?page=help_transfer_info_overseas&type=1&index=' + index
    })
  },
  identify_check(e) {
    console.log("identify_check");
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('transfer_info')
    var name = transfer_info[index].company_personal_name
    var identity_code = transfer_info[index].company_identity_code

    if (e.currentTarget.dataset.text == '已通过') {
      return false
    }

    var msg = "";
    if (!name || name == "") {
      wx.showToast({
        title: '姓名不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }

    if (!identity_code || identity_code == "") {
      wx.showToast({
        title: '身份证号码不能为空!',
        icon: 'none',
        duration: 1000
      });
      return;
    }
    console.log("第" + index + "个转让人");
    util.setUserdata('transfer_info_GetToken_index', index + 1);
    //return wx.showToast("身份证号码不能为空!");
    //   util.request('V3/identifyCheck',
    wx.showLoading({
      title: '程序加载中',
      icon: 'loading',
      mask: true
    })

    var apply_no = "";
    if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
      apply_no = util.getUserArr('transfer_info_apply_nos');
    }
    console.log('transfer_info_apply_nos==')
    console.log(util.getUserArr('transfer_info_apply_nos'))
    util.request('V3/GetToken',
      {
        'name': name,
        'identity_code': identity_code,
        'apply_no': apply_no
      },
      function (res) {
        console.log(res)
        if (res.data.code == 0) {

          var transfer_info = util.getUserArr('transfer_info')

          if (Object.keys(util.getUserArr('transfer_info_apply_nos')).length !== 0) {
            console.log("transfer_info_apply_nos已存在值")
            console.log(util.getUserArr('transfer_info_apply_nos'))
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = util.getUserArr('transfer_info_apply_nos');//apply_no赋值
          } else {
            console.log("给transfer_info_apply_nos赋值")
            util.setUserdata('transfer_info_apply_nos', res.data.apply_no);
            transfer_info[util.getUserArr('transfer_info_GetToken_index') - 1].apply_no = res.data.apply_no; //apply_no赋值
          }

          util.setUserdata('transfer_info', transfer_info)

          /*
         //旧的认证
          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: 'pages/idverification/idverification?certoken=' + res.data.token,//pages/idverification/idverification?certoken=
            envVersion: 'trial',
            success(res) {
              // 打开成功
              console.log("open other app ok!")
            }
          })
          */


          wx.navigateToMiniProgram({
            appId: 'wxe13612bab6414ea4',
            path: '/pages/idverification/idverification',
            envVersion: 'release',
            extraData: {
              'token': res.data.access_token, //身份凭证accessToken
              'applyNo': res.data.apply_no,//订单号applyNo
              'name': name, //当事人姓名
              'idno': identity_code //当事人身份证号码
            },
            success(res) {
              console.log(res);
            }
          });
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 1000
          });
        }
      })
  },
  preview_seal_files: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    console.log(this.data.personal_list);
    if (this.data.personal_list[index].seal_path) return;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('overseas_seal_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'overseas_seal_files',
                destWidth: canvasWidth,
                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadSeal'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token') },
                    filePath: result.tempFilePath,
                    name: 'company_seal_' + index,
                    success(res) {
                      wx.hideLoading()
                      var rtn = JSON.parse(res.data)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      console.log(rtn);
                      // console.log(rtn.media_unid);
                      var transfer_info = util.getUserArr('help_transfer_info')
                      // console.log('https://gongzheng.ma.cn/Files/Media/fget.html/media_id/' + rtn.FILES.media_unid);
                      // transfer_info[index].seal_path = 'https://gongzheng.ma.cn/Files/Media/fget.html/media_id/'+rtn.media_unid

                      transfer_info[index].seal_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]
                      transfer_info[index].seal_vc = rtn.verify_code
                      console.log(result.tempFilePath)
                      transfer_info[index].seal_path_name = result.tempFilePath

                      that.setData({
                        personal_list: transfer_info
                      })

                      util.setUserdata('help_transfer_info', transfer_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })

      }
    })

  },
  choose_overseas_license_files: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    if (that.data.personal_list[index].business_license_path) return;
    wx.chooseImage({
      count: 　1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('overseas_license_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'overseas_license_files',
                destWidth: canvasWidth,
                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadBusinessLicense'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token') },
                    filePath: result.tempFilePath,
                    name: 'overseas_license_' + index,
                    success(res) {
                      wx.hideLoading();
                      console.log(res);
                      var rtn = JSON.parse(res.data)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      console.log(rtn)
                      var transfer_info = util.getUserArr('help_transfer_info')
                      transfer_info[index].business_license_vc = rtn.verify_code


                      transfer_info[index].business_license_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                      that.setData({
                        personal_list: transfer_info
                      })

                      console.log(that.data.personal_list)

                      util.setUserdata('help_transfer_info', transfer_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })

      }
    })
  },
  choose_overseas_identity_card_front_files: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    if (that.data.personal_list[index].company_identity_card_front_path) return;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        console.log(photo.tempFilePaths[0])
        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('overseas_identity_card_front_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'overseas_identity_card_front_files',
                destWidth: canvasWidth,

                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadIdentityFront'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token') },
                    filePath: result.tempFilePath,
                    name: 'identity_card_front_' + index,
                    success(res) {
                      wx.hideLoading()
                      var rtn = JSON.parse(res.data)
                      console.log(rtn)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      var transfer_info = util.getUserArr('help_transfer_info')
                      transfer_info[index].company_identity_card_front_vc = rtn.verify_code

                      transfer_info[index].company_identity_card_front_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                      if (rtn.personal_name) {
                        transfer_info[index].company_personal_name = rtn.personal_name
                      }

                      if (rtn.identity_code) {
                        transfer_info[index].company_identity_code = rtn.identity_code
                      }

                      if (rtn.personal_gender) {
                        transfer_info[index].company_gender = rtn.personal_gender
                      }

                      if (rtn.personal_birth_date) {
                        transfer_info[index].company_birth_date = rtn.personal_birth_date
                      }

                      that.setData({
                        personal_list: transfer_info
                      })

                      util.setUserdata('help_transfer_info', transfer_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })

      }
    })
  },
  choose_overseas_identity_card_back_files: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index
    if (this.data.personal_list[index].company_identity_card_back_path) return;
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('overseas_identity_card_back_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'overseas_identity_card_back_files',
                destWidth: canvasWidth,
                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadIdentityBack'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token') },
                    filePath: result.tempFilePath,
                    name: 'identity_card_back_' + index,
                    success(res) {
                      wx.hideLoading()
                      var rtn = JSON.parse(res.data)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      var transfer_info = util.getUserArr('help_transfer_info')
                      transfer_info[index].company_identity_card_back_vc = rtn.verify_code

                      transfer_info[index].company_identity_card_back_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]

                      that.setData({
                        personal_list: transfer_info
                      })

                      util.setUserdata('help_transfer_info', transfer_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })

      }
    })
  },
  touchStart: function (e) {
    console.log(e);
    this.touchTimeStamp = e.timeStamp
  },
  touchEnd: function (e) {
    var diff = e.timeStamp - this.touchTimeStamp
    var tag = e.currentTarget.dataset.tag
    var id_img = e.currentTarget.dataset.id
    if (tag == 'overseas_license_block') {
      diff >= 500 ? this.delete_overseas_license_files(e) : this.preview_overseas_license_files(e)
    }
    if (tag == 'overseas_identity_card_front_block') {
      diff >= 500 ? this.delete_overseas_identity_card_front_files(e) : this.preview_overseas_identity_card_front_files(e)
    }
    if (tag == 'overseas_identity_card_back_block') {
      diff >= 500 ? this.delete_overseas_identity_card_back_files(e) : this.preview_overseas_identity_card_back_files(e)
    }
    if (tag == 'signature_block') {
      diff >= 500 ? this.delete_signature_files(e) : this.preview_signature_files(e)
    }
    if (tag == 'seal_files_test') {
      console.log(diff)
      diff >= 500 ? this.delete_seal_files(e) : this.preview_seal_files_img(e)
    }
    console.log(e)
    if (tag == 'overseas_other_block') {
      diff >= 500 ? this.delete_overseas_files(e, id_img) : this.preview_overseas_files_img(e, id_img)
    }
  },
  delete_signature_files: function (e) {
    var index = e.currentTarget.dataset.index
    var that = this
    if (this.data.personal_list[index].signature_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].signature_path = '';
            transfer_info[index].signature_vc = '';
            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  delete_overseas_files: function (e, id) {
    let index = e.currentTarget.dataset.index
    let that = this
    if (that.data.personal_list[index].overseas_other_files) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            let temp = transfer_info[index].overseas_other_files
            let temp_layimg = transfer_info[index].overseas_lawyer_img

            console.log(temp)
            console.log(id)
            console.log(transfer_info)

            // let temp_vc = transfer_info[index].overseas_other_vc
            temp.splice(id, 1)
            // temp_vc.splice(id, 1)
            temp_layimg.splice(id, 1)
            transfer_info[index].overseas_other_files = temp
            // transfer_info[index].overseas_other_vc = temp_vc
            transfer_info[index].lawyer_img = temp_layimg.join(',')
            console.log(transfer_info)
            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  preview_overseas_files_img: function (e, id) {
    var index = e.currentTarget.dataset.index;
    console.log(this.data.personal_list[index].overseas_other_files)
    if (this.data.personal_list[index].overseas_other_files) {
      wx.previewImage({
        current: this.data.personal_list[index].overseas_other_files[id],
        urls: this.data.personal_list[index].overseas_other_files
      })
    }
  },
  preview_signature_files: function (e) {
    var index = e.currentTarget.dataset.index;
    if (this.data.personal_list[index].signature_path) {
      wx.previewImage({
        urls: [this.data.personal_list[index].signature_path]
      })
    }
  },
  preview_seal_files_img: function (e) {
    var index = e.currentTarget.dataset.index;
    if (this.data.personal_list[index].seal_path) {
      wx.previewImage({
        urls: this.data.personal_list[index].seal_path
      })
    }
  },
  delete_seal_files: function (e) {
    // console.log('删除公司印章')
    let index = e.currentTarget.dataset.index
    let that = this
    console.log(this.data.personal_list)
    if (that.data.personal_list[index].seal_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].seal_path = '';
            transfer_info[index].seal_vc = '';
            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  preview_overseas_identity_card_back_files: function (e) {
    var index = e.currentTarget.dataset.index;
    if (this.data.personal_list[index].company_identity_card_back_path) {
      wx.previewImage({
        urls: this.data.personal_list[index].company_identity_card_back_path
      })
    }
  },
  delete_overseas_identity_card_back_files: function (e) {
    let index = e.currentTarget.dataset.index
    let that = this
    if (that.data.personal_list[index].company_identity_card_back_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].company_identity_card_back_path = '';
            transfer_info[index].company_identity_card_back_vc = '';
            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  preview_overseas_identity_card_front_files: function (e) {
    let index = e.currentTarget.dataset.index
    let that = this

    if (this.data.personal_list[index].company_identity_card_front_path) {
      wx.previewImage({
        urls: that.data.personal_list[index].company_identity_card_front_path
      })
    }
  },
  delete_overseas_identity_card_front_files: function (e) {
    var index = e.currentTarget.dataset.index
    var that = this
    if (this.data.personal_list[index].company_identity_card_front_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            transfer_info[index].company_identity_card_front_path = '';
            transfer_info[index].company_identity_card_front_vc = '';

            util.setUserdata('help_transfer_info', transfer_info)
            that.reload()
          }
        }
      })
    }
  },
  preview_overseas_license_files: function (e) {
    let that = this
    let index = e.currentTarget.dataset.index

    if (that.data.personal_list[index].business_license_path) {
      wx.previewImage({
        urls: that.data.personal_list[index].business_license_path
      })
    }
  },
  delete_overseas_license_files: function (e) {
    let index = e.currentTarget.dataset.index
    let that = this
    if (that.data.personal_list[index].business_license_path) {
      wx.showModal({
        title: '删除图片',
        content: '是否确认要删除该图片?',
        confirmText: "确认",
        cancelText: "取消",
        success: function (res) {
          if (res.confirm) {
            var transfer_info = util.getUserArr('help_transfer_info')
            console.log(transfer_info);
            transfer_info[index].business_license_path = '';
            transfer_info[index].business_license_vc = '';
            util.setUserdata('help_transfer_info', transfer_info)

            that.reload()
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reload()
    let ctx = wx.createCanvasContext('sealCreate')
    this.setData({
      ctx: ctx
    })
  },
  save: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''

    console.log(pageData);
    var transfer_info = util.getUserArr('help_transfer_info')
    console.log(transfer_info);
    console.log(index);
    // console.log(pageData[index].personal_birth_date)

    // for(var i = 0; i < pageData.length; i++) {
    if (pageData[index].transfer_type == 3) {
      if (!pageData[index].business_license_path) {
        wx.showToast({
          title: '请上商业登记证',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (!pageData[index].company_identity_card_front_path) {
        wx.showToast({
          title: '请上传身份证正面照片',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (!pageData[index].company_identity_card_back_path) {
        wx.showToast({
          title: '请上传身份证背面照片',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (pageData[index].overseas_other_files.length < 1) {
        wx.showToast({
          title: '请上传律师证明及其他文件',
          icon: 'none',
          duration: 2000
        })
        return false
      }
      
      if (pageData[index].company_name == '' || !pageData[index].company_name) {
        wx.showToast({
          title: '请输入公司名',
          icon: 'none',
          duration: 2000
        })
        return false
      }
      transfer_remarkp += '转让人:' + pageData[index].company_name + ';'

      if (pageData[index].social_credit_code == '' || !pageData[index].social_credit_code) {
        wx.showToast({
          title: '请输入公司注册编号',
          icon: 'none',
          duration: 2000
        })
        return false
      }
      transfer_remarkp += '公司注册编号:' + pageData[index].social_credit_code + ';'

      // if (pageData[index].license_address == '') {
      //   wx.showToast({
      //     title: '请输入公司地址',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }
      // if (pageData[index].license_address) {
      //   transfer_remarkp += '地址:' + pageData[index].license_address + ';'
      // }

      if (pageData[index].company_personal_name == '') {
        wx.showToast({
          title: '请输入法定代表人',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (pageData[index].company_identity_code == '') {
        wx.showToast({
          title: '请输入法人身份证号',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (pageData[index].company_gender == 0) {
        wx.showToast({
          title: '请选择法人性别',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      if (pageData[index].company_birth_date == '') {
        wx.showToast({
          title: '请选择法人出生日期',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      // if (!pageData[index].verifycode) {
      //   wx.showToast({
      //     title: '请通过身份认证',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      // if (!pageData[index].signature_path) {
      //   wx.showToast({
      //     title: '请输入法人授权姓名',
      //     icon: 'none',
      //     duration: 2000
      //   })
      //   return false
      // }

      if (!pageData[index].seal_path) {
        wx.showToast({
          title: '请上传公司电子印章',
          icon: 'none',
          duration: 2000
        })
        return false
      }

      pageData[index].isShow = true
      pageData[index].transfer_remarkp = transfer_remarkp
    }
    // }

    // return false;
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info[index].business_license_path = pageData[index].business_license_path
    transfer_info[index].business_license_vc = pageData[index].business_license_vc
    transfer_info[index].company_birth_date = pageData[index].company_birth_date
    transfer_info[index].company_gender = pageData[index].company_gender
    transfer_info[index].company_identity_card_back_path = pageData[index].company_identity_card_back_path
    transfer_info[index].company_identity_card_back_vc = pageData[index].company_identity_card_back_vc
    transfer_info[index].company_identity_card_front_path = pageData[index].company_identity_card_front_path
    transfer_info[index].company_identity_card_front_vc = pageData[index].company_identity_card_front_vc
    transfer_info[index].company_identity_code = pageData[index].company_identity_code
    transfer_info[index].overseas_other_files = pageData[index].overseas_other_files
    // transfer_info[index].overseas_other_vc = pageData[index].overseas_other_vc
    transfer_info[index].company_name = pageData[index].company_name
    transfer_info[index].company_personal_name = pageData[index].company_personal_name
    transfer_info[index].license_address = pageData[index].license_address
    transfer_info[index].seal_path = pageData[index].seal_path
    // transfer_info[index].signature_path = pageData[index].signature_path
    transfer_info[index].signature_vc = pageData[index].signature_vc
    transfer_info[index].company_address = pageData[index].company_address
    transfer_info[index].company_phone = pageData[index].company_phone
    transfer_info[index].social_credit_code = pageData[index].social_credit_code
    transfer_info[index].lawyer_img = pageData[index].lawyer_img
    transfer_info[index].transfer_type = 3
    transfer_info[index].isShow = pageData[index].isShow

    that.setData({
      personal_list: pageData
    })

    console.log(transfer_info);
    wx.showToast({
      title: '保存成功',
      icon: 'none',
      duration: 2000
    })
    wx.reLaunch({
      url: '../help_get_notarization/help_get_notarization',
    })
    return false

    wx.showModal({
      title: '提示',
      content: '是否确认保存',
      cancelText: "返回",
      confirmText: "继续添加",
      success: function (sm) {
        util.setUserdata('help_transfer_info', transfer_info)
        if (sm.cancel) {
          wx.navigateTo({
            url: '../help_transfer_info/help_transfer_info',
          })
        }
      }
    })
  },
  add: function() {
    var transfer_info = util.getUserArr('help_transfer_info')
    transfer_info.push({ 'transfer_type': 3, 'ot': false, isShow: false, transfer_remarkp: '', overseas_other_files: [], overseas_lawyer_img:[]})
    this.setData({
      personal_list: transfer_info
    })
    console.log(this.data.personal_list)
    util.setUserdata('help_transfer_info', transfer_info)
    
  },
  choose_brand_other_files: function (e) {
    var that = this;
    let index = e.currentTarget.dataset.index
    wx.chooseImage({
      count: 1,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function (photo) {
        var image_size = 5 * 1024 * 1024
        if (photo.tempFiles[0].size > image_size) {
          wx.showToast({
            title: '上传图片不能超过5M',
            icon: 'none',
            duration: 2000
          })
          return false
        }

        wx.getImageInfo({
          src: photo.tempFilePaths[0],
          success: function (res) {
            console.log(res);
            //---------利用canvas压缩图片--------------
            var ratio = 2;
            var canvasWidth = res.width //图片原始长宽
            var canvasHeight = res.height
            while (canvasWidth > 1024 || canvasHeight > 1024) {// 保证宽高在400以内
              canvasWidth = Math.trunc(res.width / ratio)
              canvasHeight = Math.trunc(res.height / ratio)
              ratio++;
            }
            that.setData({
              cWidth: canvasWidth,
              cHeight: canvasHeight
            })

            //----------绘制图形并取出图片路径--------------
            var ctx = wx.createCanvasContext('overseas_other_files')
            ctx.drawImage(res.path, 0, 0, canvasWidth, canvasHeight)
            ctx.draw(false, setTimeout(function () {
              wx.canvasToTempFilePath({
                canvasId: 'overseas_other_files',
                destWidth: canvasWidth,
                destHeight: canvasHeight,
                success: function (result) {
                  console.log(result.tempFilePath)//最终图片路径

                  //上传图片
                  wx.showLoading({
                    title: '图片上传中',
                    icon: 'loading',
                    mask: true
                  })
                  wx.uploadFile({
                    url: util.reqUrl('V2/uploadSeal'),
                    header: util.headers(),
                    formData: { 'identity_token': util.getUserdata('identity_token'), 'image_type':8 },
                    filePath: result.tempFilePath,
                    name: 'overseas_other' + index,
                    success(res) {
                      wx.hideLoading()
                      console.log(res);
                      var rtn = JSON.parse(res.data)
                      if (rtn.code < 0) {
                        wx.showToast({
                          title: rtn.err,
                          icon: 'none',
                          duration: 2000
                        })
                        return false
                      }
                      var transfer_info = util.getUserArr('help_transfer_info')
                      var overseas_other = transfer_info[index].overseas_other_files
                      console.log(index)
                      console.log(transfer_info)
                      console.log(overseas_other)
                      var overseas_other_lawer = transfer_info[index].overseas_lawyer_img
                      // var overseas_other_vc = transfer_info[index].overseas_other_vc
                      transfer_info[index].overseas_other_files = overseas_other.concat('https://gongzheng.ma.cn/MYPublic/' + rtn.image_name)
                      transfer_info[index].overseas_lawyer_img = overseas_other_lawer.concat(rtn.image_name)
                      // transfer_info[index].overseas_other_vc = overseas_other_vc.concat(rtn.verify_code)
                      transfer_info[index].lawyer_img = transfer_info[index].overseas_lawyer_img.join(',')
                      console.log(transfer_info[index].lawyer_img)
                      that.setData({
                        personal_list: transfer_info
                      })

                      util.setUserdata('help_transfer_info', transfer_info)

                      console.log(transfer_info)
                    }
                  })

                },
                fail: function (res) {
                  console.log(res.errMsg)
                }
              })
            }, 500))
          }
        })

      }
    })
  },
  change_isShow: function (e) {
    var that = this
    var index = e.currentTarget.dataset.index
    var pageData = that.data.personal_list
    var transfer_remarkp = ''
    var transfer_info = util.getUserArr('help_transfer_info')

    console.log(transfer_info[index].isShow);
    console.log(pageData[index].company_name);
    transfer_info[index].isShow = !transfer_info[index].isShow
    pageData[index].company_name ? transfer_remarkp += '转让人:' + pageData[index].company_name + ';' : ''
    pageData[index].social_credit_code ? transfer_remarkp += '公司注册编号:' + pageData[index].social_credit_code + ';' : ''
    pageData[index].license_address ? transfer_remarkp += '地址:' + pageData[index].license_address + ';' : ''

    transfer_info[index].transfer_remarkp = transfer_remarkp
    that.setData({
      personal_list: transfer_info
    })
    util.setUserdata('help_transfer_info', transfer_info)
  },
  clear_input: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    var pageList = that.data.personal_list

    console.log(transfer_info);
    console.log(that.data.personal_list);

    wx.showModal({
      title: '提示',
      content: '确定要清空吗？',
      success: function (sm) {
        if (sm.confirm) {
          for (var i = 0; i < pageList.length; i++) {
            if (i == index && pageList[i].transfer_type == 3) {
              pageList[i].company_identity_card_back_path = '';
              pageList[i].company_identity_card_back_vc = '';
              pageList[i].company_identity_card_front_path = '';
              pageList[i].company_identity_card_front_vc = '';
              pageList[i].verifycode = '';
              pageList[i].company_address = '';
              pageList[i].company_phone = '';
              pageList[i].signature_path = '';
              pageList[i].company_identity_code = '';
              pageList[i].business_license_path = '';
              pageList[i].business_license_vc = '';
              pageList[i].company_name = '';
              pageList[i].company_personal_name = '';
              pageList[i].license_address = '';
              pageList[i].seal_path = '';
              pageList[i].social_credit_code = '';
              pageList[i].company_gender = 0;
              pageList[i].company_birth_date = '';
              pageList[i].seal_path_name = '';
              pageList[i].seal_vc = '';
              pageList[i].transfer_remarkp = '';
              pageList[i].overseas_other_files = [];
              pageList[i].overseas_lawyer_img = [];
              pageList[i].lawyer_img = '';
            }
          }
          that.setData({
            personal_list: pageList
          });


          for (var i = 0; i < transfer_info.length; i++) {
            if (i == index && transfer_info[i].transfer_type == 3) {
              transfer_info[i].company_identity_card_back_path = '';
              transfer_info[i].company_identity_card_back_vc = '';
              transfer_info[i].company_identity_card_front_path = '';
              transfer_info[i].company_identity_card_front_vc = '';
              transfer_info[i].verifycode = '';
              transfer_info[i].company_address = '';
              transfer_info[i].company_phone = '';
              transfer_info[i].signature_path = '';
              transfer_info[i].company_identity_code = '';
              transfer_info[i].business_license_path = '';
              transfer_info[i].business_license_vc = '';
              transfer_info[i].company_name = '';
              transfer_info[i].company_personal_name = '';
              transfer_info[i].license_address = '';
              transfer_info[i].seal_path = '';
              transfer_info[i].social_credit_code = '';
              transfer_info[i].company_gender = 0;
              transfer_info[i].company_birth_date = '';
              transfer_info[i].seal_path_name = '';
              transfer_info[i].seal_vc = '';
              transfer_info[i].transfer_remarkp = '';
              transfer_info[i].overseas_other_files = [];
              transfer_info[i].overseas_lawyer_img = [];
              transfer_info[i].lawyer_img = '';
            }
          }
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })
  },
  del: function(e){
    var that = this;
    var index = e.currentTarget.dataset.index
    var transfer_info = util.getUserArr('help_transfer_info')
    wx.showModal({
      title: '提示',
      content: '确定要删除吗？',
      success: function (sm) {
        if (sm.confirm) {
          // if (transfer_info[index].transfer_type != 2 && transfer_info[index].transfer_type != 1) {
          //   console.log(transfer_info[index])
          // }
          transfer_info.splice(index, 1)
          that.setData({
            personal_list: transfer_info
          })
          util.setUserdata('help_transfer_info', transfer_info)
        }
      }
    })
  },
  createSeal(company_name, index) {
    var that = this
    let context = this.data.ctx;
    console.log(context)

    let w = 150
    let h = 150
    let color = '#E69696'
    let sx = w / 2
    let sy = h / 2
    let rotato = 0
    let radius = 15

    let fontSize = 16
    let max = 40

    //context.save();  
    context.setFillStyle(color);
    // context.moveTo(10, 10)
    // context.rect(10, 10, 100, 50)
    // context.lineTo(110, 60)
    // context.stroke()
    // context.draw()
    context.translate(0, 0)
    context.beginPath();
    context.arc(sx, sy, 65, 0, Math.PI * 2)
    context.setStrokeStyle(color)
    context.stroke()
    context.save()

    context.translate(sx, sy);//移动坐标原点  
    context.rotate(Math.PI + rotato);//旋转  
    context.beginPath();//创建路径  
    var x = Math.sin(0);
    var y = Math.cos(0);
    var dig = Math.PI / 5 * 4;
    console.log(dig)
    for (var i = 0; i < 5; i++) {//画五角星的五条边  
      var x = Math.sin(i * dig);
      var y = Math.cos(i * dig);
      context.lineTo(x * radius, y * radius);
    }
    context.closePath();
    context.stroke();
    context.fill();
    context.restore();

    context.translate(sx, sy);
    context.font = '16px STFANGSO'
    context.textBaseline = 'middle';
    context.textAlign = 'center';
    context.lineWidth = 1


    var count = company_name.length;
    if (count > 16) fontSize = 12;
    if (count > 22) fontSize = 10;
    if (count > 28) fontSize = 8;
    // if(count>max)count=max;

    context.setFontSize(fontSize)

    var angle = 4 * Math.PI / (3 * (count - 1));
    var chars = company_name.split("");
    var c;
    for (var i = 0; i < count; i++) {
      c = chars[i];

      if (i == 0)
        context.rotate(5 * Math.PI / 6);
      else
        context.rotate(angle);
      context.save();
      context.translate(75 - fontSize, 0);
      context.rotate(Math.PI / 2);
      //context.font = 'bolder 16px SimSun '
      //context.setFontSize(fontSize)
      console.log(c)
      context.fillText(c, 0, 5);
      context.restore();
    }

    context.draw()
    wx.canvasToTempFilePath({
      x: 0,
      y: 0,
      width: 150,
      height: 150,
      destWidth: 150,
      destHeight: 150,
      canvasId: 'sealCreate',
      success(res) {
        console.log(res)
        console.log(res.tempFilePath)
        var transfer_info = util.getUserArr('transfer_info')
        // transfer_info[index].seal_path_name = res.tempFilePath
        wx.uploadFile({
          url: util.reqUrl('V2/uploadSeal'),
          header: util.headers(),
          formData: { 'identity_token': util.getUserdata('identity_token') },
          filePath: res.tempFilePath,
          name: 'company_seal_' + index,
          success(result) {
            var rtn = JSON.parse(result.data)
            console.log(rtn);
            console.log(rtn.media_unid);
            var transfer_info = util.getUserArr('transfer_info')
            // console.log('https://gongzheng.ma.cn/Files/Media/fget.html/media_id/' + rtn.FILES.media_unid);
            // transfer_info[index].seal_path = 'https://gongzheng.ma.cn/Files/Media/fget.html/media_id/' + rtn.media_unid

            transfer_info[index].seal_path = ['https://gongzheng.ma.cn/MYPublic/' + rtn.image_name]
            transfer_info[index].seal_vc = rtn.verify_code
            transfer_info[index].seal_path_name = res.tempFilePath

            console.log(transfer_info)
            that.setData({
              personal_list: transfer_info
            })

            util.setUserdata('transfer_info', transfer_info)

            // var transfer_info = util.getUserArr('transfer_info')
            // transfer_info[index].company_identity_card_back_vc = rtn.verify_code

            // that.setData({
            //   personal_list: transfer_info
            // })

            // util.setUserdata('transfer_info', transfer_info)
          }
        })

        // transfer_info[index].seal_path_name = res.tempFilePath
        // that.setData({
        //   personal_list : transfer_info
        // });
        // util.setUserdata('transfer_info',transfer_info)
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    console.log("监听页面显示");
    this.reload()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})