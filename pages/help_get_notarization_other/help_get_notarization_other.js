const util = require('../../utils/util.js')
Page({
  data: {
    page_count: '2',
    remark: '',
    mail_type: false,
    care_type: false,
    send_type: false,
    comm_type: false,
    nopaper_type:false,
    mail_type_status: false,
    care_type_status: false,
    send_type_status: false,
    comm_type_status: false,
    nopaper_type_status: false,
    input_status: false,
    vip_mail_status: false,
    vip_care_status: false
  },  
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    setTimeout(() => {
      wx.hideNavigationBarLoading()
      wx.stopPullDownRefresh()
    }, 2000);
  },
  bindBackTap: function () {
    wx.navigateTo({
      url: '../get_notarization/get_notarization',
    })
  },
  bindOtherTap: function () {
    wx.navigateTo({
      url: '../get_notarization_other/get_notarization_other',
    })
  },
  page_countadd:function()
  {
    //若选中不邮寄 阻止以下操作
    if (this.data.nopaper_type) {
      return false
    }
    var order_id = util.getUserArr('help_order_id')
    console.log(order_id)
    if (order_id > 0) {
      return false;
    }
    var other = util.getUserdata('help_other');
    console.log(other);
    other.page_count = parseInt(other.page_count)
    var page_count = other.page_count+1;
    
    console.log(page_count);
    this.setData({
      page_count: page_count
    });
    other.page_count = page_count;
    util.setUserdata('help_other', other);
  },
  page_countlost: function () {
    //若选中不邮寄 阻止以下操作
    if (this.data.nopaper_type) {
      return false
    }
    var order_id = util.getUserArr('help_order_id')
    console.log(order_id)
    if(order_id > 0) {
      return false;
    }
    var other = util.getUserdata('help_other');
    console.log(other.page_count);
    var page_count =2;
    if (other.page_count>2){
      page_count = other.page_count-1;
    } 
    console.log(page_count);
    this.setData({
      page_count: page_count
    });

    other.page_count = page_count;
    util.setUserdata('help_other', other);

  },
  countInput: function (e){
    // var order_id = util.getUserArr('order_id')
    // console.log(order_id)
    // if (order_id > 0) {
    //   return false;
    // }
    console.log(e.detail.value)
    var other = util.getUserdata('help_other');
    other.page_count = e.detail.value
    console.log(other)
    console.log(isNaN(parseInt(other.page_count)))
    if (isNaN(parseInt(other.page_count)) == true || parseInt(other.page_count) < 2) {
      other.page_count = 2
    }
    
    // return false
    this.setData({
      page_count: other.page_count
    });
    util.setUserdata('help_other',other);
  },
  remarkInput: function (e){
    var other = util.getUserdata('help_other');
    other.remark = e.detail.value
    console.log(other)
    this.setData({
      remark: other.remark
    });
    util.setUserdata('help_other',other);
  },
  mailTypeChange: function(e){
    var other = util.getUserdata('help_other');
    console.log(other)
    other.mail_type = e.detail.value
    console.log(other)
    this.setData({
      mail_type: other.mail_type
    });
    util.setUserdata('help_other',other);
  },
  sendTypeChange: function(e){
    var other = util.getUserdata('help_other');
    other.send_type = e.detail.value
    console.log(other)
    this.setData({
      send_type: other.send_type
    });
    util.setUserdata('help_other',other);
  },
  careTypeChange: function(e){
    var other = util.getUserdata('help_other');
    other.care_type = e.detail.value
    console.log(other)
    this.setData({
      care_type: other.care_type
    });
    util.setUserdata('help_other',other);
  },
  commTypeChange: function (e) {
    var other = util.getUserdata('help_other');
    other.comm_type = e.detail.value
    console.log(other)
    this.setData({
      comm_type: other.comm_type
    });
    util.setUserdata('help_other', other);
  },
  noSendPaper: function (e) {
    var other = util.getUserdata('help_other');
    other.nopaper_type = e.detail.value
    if(e.detail.value) {
      other.page_count = 0
      other.mail_type = false
    }else{
      other.page_count = 2
      other.mail_type = true
    }
    console.log('nopaper_type', e.detail.value)
    this.setData({
      page_count: other.page_count,
      mail_type: other.mail_type,
      nopaper_type: e.detail.value
    });
    util.setUserdata('help_other', other);
  },
  onLoad: function () {
    let that = this
    util.request(
      'V2/myAllInfo',
      { 'type': 1 },
      function (res) {
        if (res.data.code == 0) {
          let other = util.getUserdata('help_other');
          let order_id = util.getUserArr('help_order_id')
          console.log(other)
          console.log(order_id)
          let is_vip = res.data.member.is_vip

          if(is_vip == 1) {
            if (order_id > 0) {
              //修改订单
              that.setData({
                nopaper_type: other.nopaper_type,
                page_count: other.page_count,
                mail_type: other.mail_type || false,
                send_type: other.send_type || false,
                care_type: other.care_type || false,
                comm_type: other.comm_type || false,

                mail_type_status: true,
                send_type_status: true,
                care_type_status: true,
                // comm_type_status: true,
                input_status: true,
                is_vip: is_vip
              })
            } else {
              //vip会员,新增订单
              console.log(other.page_count)
              console.log('comm_type', other.comm_type)

              // //选中不邮寄
              // if (that.nopaper_type) {

              // }else{
              //   that.nopaper_type?2:0
              // }

              that.setData({
                // page_count: other.page_count || 2,
                nopaper_type: other.nopaper_type,
                page_count: other.nopaper_type ? 0 : 2,
                remark: other.remark || '',
                mail_type: other.nopaper_type ? false : true,
                mail_type_status: true,
                send_type: other.send_type || false,
                care_type: false,
                care_type_status: false,
                comm_type: other.comm_type || false,
                is_vip: is_vip
              });
            }
            // if (!other.page_count) other.page_count = 2
            console.log('other22', other)

            util.setUserdata('help_other', other);
            
          }else{

            if (order_id > 0) {
              //修改订单
              that.setData({
                nopaper_type: other.nopaper_type,
                page_count: other.page_count,
                mail_type: other.mail_type || false,
                send_type: other.send_type || false,
                care_type: other.care_type || false,
                comm_type: other.comm_type || false,

                mail_type_status: true,
                send_type_status: true,
                care_type_status: true,
                // comm_type_status: true,
                input_status: true
              })
            } else {
              //非vip会员,新增订单
              console.log('非vip会员,新增订单'+other.page_count)
              that.setData({
                nopaper_type: other.nopaper_type,
                page_count: other.nopaper_type ? 0 : 2,
                // page_count: other.page_count || 2,
                remark: other.remark || '',
                // mail_type: other.mail_type || false,
                mail_type: other.nopaper_type ? false : true,
                // mail_type_status: true,1
                send_type: other.send_type || false,
                care_type: other.care_type || false,
                // care_type_status: true,
                comm_type: other.comm_type || false
              });
            }
            // if (!other.page_count) other.page_count = 2

            util.setUserdata('help_other', other);
          }

        }
      },
      function (res) {
        //尝试重启
        wx.reLaunch({
          url: 'pages/index/index'
        })
      },
      function (res) {
        // wx.hideToast();
      }
    )
    

  }

})